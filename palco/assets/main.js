import p5 from 'p5';
import setFormation from './src/formation';
import { Manager, DoubleArcManager } from './src/manager';
import {
  ArcReference,
  RectangleReference,
  CircleReference,
  DoubleArcReference,
} from './src/reference';
import {
  GHOST_MODE,
  WIDTH,
  HEIGHT,
  FRAME_RATE,
} from './src/defs';
import UI from './src/ui';

let isMobile;
const onMediaQueryDo = [];
const mediaQuery = window.matchMedia('(max-width: 350px)');
const handleMediaChange = (e) => {
  isMobile = e.matches;
  onMediaQueryDo.forEach((f) => f(e.matches));
};
mediaQuery.addListener(handleMediaChange);
handleMediaChange(mediaQuery);

function init(p5Object, options) {
  const p = p5Object;
  const palco = {
    n_singers: options.n_singers,
    debug: options.debug,
    lazy: options.lazy,
    repeat: options.repeat,
    arc_reference: new ArcReference(p),
    circle_reference: new CircleReference(p),
    rectangle_reference: new RectangleReference(p),
    double_arc_reference: new DoubleArcReference(p),
    manager: new Manager(p),
    double_arc_manager: new DoubleArcManager(p),
  };

  setFormation(palco, options.formation);
  p.palco = palco;
}

// eslint-disable-next-line new-cap
const sketch = (opt) => new p5((p5Object) => {
  const p = p5Object;
  const options = opt;
  init(p, options);

  const bg = () => {
    p.background(255, 255, 255);
  };

  p.setup = () => {
    p.frameRate(FRAME_RATE);
    p.pixelDensity(2);

    const canvas = p.createCanvas(WIDTH, HEIGHT);
    const setAspectRatio = (isMob) => {
      if (isMob) {
        p.resizeCanvas(HEIGHT, WIDTH, false);
        canvas.canvas.style.setProperty(
          'aspect-ratio',
          `${HEIGHT} / ${WIDTH}`,
        );
        canvas.canvas.style.setProperty('width', '60%');
        canvas.canvas.style.setProperty('margin', '0 auto');
        canvas.canvas.style.setProperty('height', 'auto');
      } else {
        p.resizeCanvas(WIDTH, HEIGHT);
        canvas.canvas.style.setProperty(
          'aspect-ratio',
          `${WIDTH} / ${HEIGHT}`,
        );
        canvas.canvas.style.setProperty('width', '100%');
        canvas.canvas.style.setProperty('height', 'auto');
      }
    };
    setAspectRatio(isMobile);
    onMediaQueryDo.push(setAspectRatio);

    if (options.ui) {
      const ui = new UI(
        p,
        canvas,
        options,
      );
      ui.init();
    }
    p.palco.curr_manager.update();

    bg();
  };

  p.draw = () => {
    if (!GHOST_MODE) {
      bg();
    }

    if (isMobile) {
      const x = WIDTH / 2;
      const y = HEIGHT / 2 + (WIDTH - HEIGHT) / 2;
      p.translate(x, y);
      p.rotate(-Math.PI / 2);
      p.translate(-x, -y);
    }
    p.palco.curr_manager.draw();
  };
}, opt.id);

export default sketch;
