const path = require('path');

module.exports = {
  entry: './main.js',
  output: {
    library: 'Palco',
    libraryExport: 'default',
    filename: 'bundle.js',
    path: path.resolve(__dirname, '../static/palco'),

  },
  optimization: {
    usedExports: true,
  },
};
