function vecSum(p, vectors) {
  const ret = p.createVector(0, 0);
  vectors.forEach((v) => ret.add(v));
  return ret;
}

function normalizeAngle(angle) {
  const a = angle % (2 * Math.PI);
  if (a > Math.PI) {
    return -2 * Math.PI + a;
  }
  if (a < -Math.PI) {
    return 2 * Math.PI + a;
  }
  return a;
}

function indexChoice(arr) {
  return Math.floor(Math.random() * arr.length);
}

export { vecSum, normalizeAngle, indexChoice };
