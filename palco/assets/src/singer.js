import p5 from 'p5';
import {
  GHOST_MODE,
  WIDTH,
  HEIGHT,
  EPS,
  DEBUG,
  FRAME_RATE,
  WAIT_AFTER_SETTLED,
  MAX_SECS_BEFORE_RESTART,
} from './defs';

import { vecSum, normalizeAngle } from './util';

class Singer {
  constructor(p, reference, eps = EPS) {
    this.p = p;
    this.r = GHOST_MODE ? 1 : 4;
    this.max_speed = 1;
    this.acceleration = 0.05;
    this.eps = eps;
    this.reset();
    if (reference) {
      this.setReference(reference);
    }
  }

  reset() {
    this.pos = this.p.createVector(this.p.random(WIDTH), this.p.random(HEIGHT));
    this.color = [this.p.random(150), this.p.random(150), this.p.random(150)];

    // Timing
    this.speed = 0.005;
    this.frames = WAIT_AFTER_SETTLED * FRAME_RATE;
    this.max_frames = MAX_SECS_BEFORE_RESTART * FRAME_RATE;
    this.ready = false;
    this.initial_pause = 1 * FRAME_RATE;
    this.lazy_ready = false;
    this.lazy_pause = 2 * FRAME_RATE;
  }

  setReference(reference) {
    this.reference = reference;
    this.quadrants = this.reference.quadrants;
    this.speed = 0.005;
  }

  draw() {
    this.p.push();
    this.p.stroke(...this.color, GHOST_MODE ? 5 : 255);
    this.p.fill(...this.color, GHOST_MODE ? 5 : 255);
    const h = this.r * 2;
    this.p.ellipse(this.pos.x, this.pos.y, h, h);
    this.p.pop();
  }

  findNearestNeighbors(singers) {
    return singers
      .filter((s) => s !== this)
      .sort((a, b) => (
        this.p.dist(this.pos.x, this.pos.y, a.pos.x, a.pos.y)
        - this.p.dist(this.pos.x, this.pos.y, b.pos.x, b.pos.y)
      ));
  }

  neighborsByAngle(neighbors) {
    const e0 = this.p.createVector(1, 0);
    const ret = [];
    const angles = [];
    const minAngularDist = (2 * Math.PI) / this.quadrants;

    neighbors.forEach((n) => {
      const angle = p5.Vector.angleBetween(
        e0,
        p5.Vector.sub(this.pos, n.pos),
      );

      const use = angles.every((refAngle) => {
        const nref = normalizeAngle(refAngle);
        const nang = normalizeAngle(angle);
        if (Math.min(
          Math.abs(nref - nang),
          Math.PI - Math.abs(nref) + Math.PI - Math.abs(nang),
        ) < minAngularDist) {
          return false;
        }
        return true;
      });

      if (use) {
        ret.push(n);
        angles.push(angle);
      }
    });

    return ret;
  }

  checkReady(oldPos) {
    if (this.pos.dist(oldPos) < 0.1) {
      this.frames = Math.max(this.frames - 1, 0);
      if (this.frames <= 0) {
        this.ready = true;
      }
    }
  }

  gaveUpLazy(mag) {
    if (this.p.palco.lazy) {
      if (mag < 3) {
        this.lazy_ready = true;
      }
      if (this.lazy_ready) {
        this.lazy_pause = Math.max(0, this.lazy_pause - 1);
      }
      if (this.lazy_pause <= 0) {
        return true;
      }
    }
    return false;
  }

  accelerate() {
    if (this.initial_pause > 0) {
      this.initial_pause -= 1;
      return;
    }
    this.speed = Math.min(this.max_speed, this.speed + this.acceleration);
  }

  walkStraight(v) {
    // Walk along vector v, regarding it's magnitude and our speed
    const mag = v.mag();

    if (mag > this.eps) {
      if (mag > 1) {
        v.normalize();
      }
      this.pos.x += v.x * this.speed;
      this.pos.y += v.y * this.speed;
    } else if (GHOST_MODE) {
      this.r = Math.min(this.r + 0.05, 4);
    }
  }

  walkTo(p) {
    return this.walkStraight(p5.Vector.sub(p, this.pos));
  }

  walk(singers) {
    this.accelerate();

    let neighbors = this.findNearestNeighbors(singers);
    neighbors = this.neighborsByAngle(neighbors);

    const refDirection = this.reference.direction(this.pos);
    const mag = Math.max(refDirection.mag(), 0.00001);

    if (this.gaveUpLazy(mag)) {
      this.ready = true;
      return;
    }

    const list = neighbors.map((n) => {
      const dist = Math.max(n.pos.dist(this.pos), 0.00001);

      return p5.Vector.sub(n.pos, this.pos)
        .normalize()
        .mult((
          2 * this.reference.expected_distance
            * -1 * Math.max(1, 0.5 / mag)
        ) / dist);
    });

    list.push(refDirection);
    const sum = vecSum(this.p, list);

    // console.log(this.p.palco.debug);
    if (DEBUG || this.p.palco.debug || this.debug) {
      this.p.push();
      this.p.stroke(180);
      neighbors.forEach((n) => (
        this.p.line(n.pos.x, n.pos.y, this.pos.x, this.pos.y)
      ));

      this.p.stroke(180, 180, 30);
      this.p.line(
        this.pos.x,
        this.pos.y,
        this.pos.x + refDirection.x,
        this.pos.y + refDirection.y,
      );

      this.p.pop();
    }

    const oldPos = this.p.createVector(this.pos.x, this.pos.y);
    this.walkStraight(sum);
    this.checkReady(oldPos);
  }
}

export default Singer;
