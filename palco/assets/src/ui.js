import setFormation from './formation';

const ptTranslations = {
  arc: 'arco',
  'arc 2x': 'arco 2x',
  circle: 'círculo',
  random: 'aleatório',
  lines: 'linhas',
  lazy: 'não insiste',
  repeat: 'repetir',
  reset: 'resetar',
};

class UI {
  constructor(p, canvas, options) {
    this.p = p;
    this.width = canvas.canvas.offsetWidth;
    this.height = canvas.canvas.offsetHeight;
    this.options = options;
    this.selectedFormation = null;
  }

  _(text) {
    if (this.options.language === 'pt') {
      return ptTranslations[text];
    }
    return text;
  }

  init() {
    const form = this.options.formation;

    let div = this.p.createDiv();
    div.elt.classList.add('formation');

    this.createFormationButton(div, this._('arc'), 'arc', form === 'arc');
    this.createFormationButton(div, this._('arc 2x'), 'double_arc', form === 'double_arc');
    this.createFormationButton(div, this._('circle'), 'circle', form === 'circle');
    this.createFormationButton(div, this._('random'), 'rectangle', form === 'rectangle');

    div = this.p.createDiv();
    div.elt.classList.add('toggle');

    this.createToggleButton(div, this._('lines'), 'debug', this.options.debug);
    this.createToggleButton(div, this._('lazy'), 'lazy', this.options.lazy);
    this.createToggleButton(div, this._('repeat'), 'repeat', this.options.repeat);

    div = this.p.createDiv();
    div.elt.classList.add('singers');

    this.createSingersButton(div, '+', 'addSinger');
    this.createSingersButton(div, '-', 'rmSinger');

    div = this.p.createDiv();
    div.elt.classList.add('reset');

    this.createResetButton(div, this._('reset'), 'reset');
  }

  createButton(div, text, value) {
    const button = this.p.createButton(text, value);
    button.parent(div);
    return button;
  }

  setSelected(newSelected) {
    if (this.selectedFormation !== null) {
      this.selectedFormation.elt.classList.remove('selected');
    }
    this.selectedFormation = newSelected;
    newSelected.elt.classList.add('selected');
  }

  handleClickHof(value, button) {
    return () => {
      this.setSelected(button);
      setFormation(this.p.palco, value);
      this.p.palco.curr_manager.update();
    };
  }

  createFormationButton(div, text, value, selected) {
    const button = this.createButton(div, text, value);
    if (selected) {
      this.setSelected(button);
    }
    button.mousePressed(this.handleClickHof(value, button));
  }

  createToggleButton(div, text, value, on = false) {
    const button = this.createButton(div, text, value);
    if (on) {
      button.elt.classList.add('selected');
    }
    button.mousePressed(() => {
      button.elt.classList.toggle('selected');
      this.p.palco[value] = !this.p.palco[value];
    });
  }

  createSingersButton(div, text, value) {
    const button = this.createButton(div, text, value);
    button.mousePressed(() => this.p.palco.curr_manager[value]());
  }

  createResetButton(div, text, value) {
    const button = this.createButton(div, text, value);
    button.mousePressed(() => this.p.palco.manager.resetSingers());
  }
}

export default UI;
