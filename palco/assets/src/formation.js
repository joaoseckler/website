function setFormation(palcoVars, formation) {
  const palco = palcoVars;

  switch (formation) {
    default: // eslint-disable-line default-case-last
      console.error(
        `Formation not recognized: ${formation}. `
        + 'defaulting to "arc"...',
      );
    case 'arc': // eslint-disable-line no-fallthrough
      palco.curr_reference = palco.arc_reference;
      palco.curr_manager = palco.manager;
      break;
    case 'circle':
      palco.curr_reference = palco.circle_reference;
      palco.curr_manager = palco.manager;
      break;
    case 'random':
    case 'rectangle':
      palco.curr_reference = palco.rectangle_reference;
      palco.curr_manager = palco.manager;
      break;
    case 'double_arc':
      palco.curr_reference = palco.arc_reference;
      palco.curr_manager = palco.double_arc_manager;
      break;
    case 'double_arc_reference': // Not very good, don't use this
      palco.curr_reference = palco.double_arc_reference;
      palco.curr_manager = palco.manager;
      break;
  }
}

export default setFormation;
