/* eslint-disable max-classes-per-file */

import p5 from 'p5';
import {
  WIDTH,
  HEIGHT,
} from './defs';

import { normalizeAngle } from './util';

/*
class Reference {
  constructor(p) {}

  draw() {}

  // Get the vector that points a position to the nearest point in this
  // reference
  direction() {
    return this;
  }
}
*/

class ArcReference {
  constructor(
    p,
    radius = 200,
    center = null,
    angles = [-Math.PI + Math.PI / 6, -Math.PI / 6],
  ) {
    this.angles = angles;

    let c = center;
    if (c === null) {
      const offset = (
        Math.abs(Math.sin(this.angles[1])) * radius
        + ((1 - Math.abs(Math.sin(this.angles[1]))) * radius) / 2
      );

      c = [
        WIDTH / 2,
        HEIGHT / 2 + offset,
      ];
    }

    this.p = p;
    this.pos = p.createVector(...c);
    this.r = radius;
    this.color = [0, 0, 0];
    this.quadrants = 3;
    this.gone = [0, 0];
    this.calcEdges();
  }

  edgesFromRadius(radius) {
    return [
      this.p.createVector(
        this.pos.x + radius * Math.cos(this.angles[0]),
        this.pos.y + radius * Math.sin(this.angles[0]),
      ),
      this.p.createVector(
        this.pos.x + radius * Math.cos(this.angles[1]),
        this.pos.y + radius * Math.sin(this.angles[1]),
      ),
    ];
  }

  update(n) {
    this.calcExpectedDistance(n);
  }

  calcEdges() {
    this.edges = this.edgesFromRadius(this.r);
  }

  calcExpectedDistance(n) {
    this.expected_distance = Math.abs(
      ((this.angles[1] - this.angles[0]) * this.r) / (n),
    );
  }

  drawRadius(radius) {
    const h = radius * 2;

    this.p.push();
    this.p.noFill();
    this.p.strokeWeight(1);
    this.p.stroke(150);
    this.p.arc(this.pos.x, this.pos.y, h, h, ...this.angles);
    this.p.pop();
  }

  draw() {
    return this.drawRadius(this.r);
  }

  directionRadius(pos, radius) {
    const angle = this.p.createVector(1, 0).angleBetween(
      p5.Vector.sub(pos, this.pos),
    );

    const middle = (this.angles[0] + this.angles[1]) / 2;

    const [a, b] = [...this.angles].sort((x, y) => x - y);
    const [na, nb] = [a, b].map((x) => normalizeAngle(x - middle));
    const nangle = normalizeAngle(angle - middle);

    if (this.edges.length > 1) {
      if (nangle > nb) {
        return p5.Vector.sub(this.edges[1], pos);
      }
      if (nangle < na) {
        return p5.Vector.sub(this.edges[0], pos);
      }
    }

    const d = p5.Vector.dist(pos, this.pos);
    const v = this.p.createVector(
      (radius - d) * Math.cos(angle),
      (radius - d) * Math.sin(angle),
    );

    return v;
  }

  direction(p) {
    return this.directionRadius(p, this.r);
  }
}

class RectangleReference {
  constructor(
    p,
    nSingers,
    center = [WIDTH / 2, HEIGHT / 2],
    width = 250,
    height = 100,
  ) {
    this.p = p;
    const [x, y] = center;
    this.edges = [
      // Order matters for direction
      p.createVector(x - width / 2, y - height / 2),
      p.createVector(x - width / 2, y + height / 2),
      p.createVector(x + width / 2, y + height / 2),
      p.createVector(x + width / 2, y - height / 2),
    ];

    this.width = width;
    this.height = height;
    [this.tl_corner] = this.edges;
    this.quadrants = 3;
  }

  calcExpectedDistance() {
    this.expected_distance = (
      Math.sqrt(this.width * this.height) / (Math.sqrt(this.p.palco.n_singers))
    );
  }

  update() {
    this.calcExpectedDistance();
  }

  addSinger() {
    this.calcExpectedDistance();
  }

  rmSinger() {
    this.calcExpectedDistance();
  }

  draw() {
    this.p.push();
    this.p.stroke(0);
    this.p.fill(255, 234, 233);
    this.p.rect(this.tl_corner.x, this.tl_corner.y, this.width, this.height);
    this.p.pop();
  }

  direction(pos) {
    let x; let y;
    const [x1, x2] = [this.edges[0].x, this.edges[2].x];
    const [y1, y2] = [this.edges[3].y, this.edges[1].y];

    if (pos.x > x1 && pos.x < x2) x = 0;
    else if (pos.x < x1) x = x1 - pos.x;
    else x = x2 - pos.x;

    if (pos.y > y1 && pos.y < y2) y = 0;
    else if (pos.y < y1) y = y1 - pos.y;
    else y = y2 - pos.y;

    return this.p.createVector(x, y);
  }
}

class CircleReference extends ArcReference {
  constructor(p) {
    const radius = (Math.min(WIDTH, HEIGHT) * 0.8) / 2;
    const center = [WIDTH / 2, HEIGHT / 2];
    super(p, radius, center, [0, 2 * Math.PI]);
    this.edges = [];
  }
}

class DoubleArcReference extends ArcReference {
  constructor(
    p,
    radius = 200,
    center = null,
    angles = [-Math.PI + Math.PI / 6, -Math.PI / 6],
  ) {
    super(p, radius, center, angles);
    this.r = [radius - 20, radius];
    this.calcExpectedDistance();
    this.edges = [
      this.edgesFromRadius(this.r[0]),
      this.edgesFromRadius(this.r[1]),
    ];
    this.quadrants = 2;
  }

  calcExpectedDistance() {
    this.expected_distance = Math.abs(
      ((this.angles[1] - this.angles[0]) * (this.r[0] + this.r[1]))
      / (this.nSingers),
    );
  }

  draw() {
    this.drawRadius(this.r[0]);
    this.drawRadius(this.r[1]);
  }

  direction(pos) {
    const angle = this.p.createVector(1, 0).angleBetween(
      p5.Vector.sub(pos, this.pos),
    );

    const middle = (this.angles[0] + this.angles[1]) / 2;

    const [a, b] = [...this.angles].sort((x, y) => x - y);
    const [na, nb] = [a, b].map((x) => normalizeAngle(x - middle));
    const nangle = normalizeAngle(angle - middle);

    const distToCenter = this.pos.dist(pos);
    let edgeIndex;
    let radiusIndex;

    if (this.edges.length > 1 && (nangle > nb || nangle < na)) {
      edgeIndex = nangle > nb ? 1 : 0;

      if (distToCenter < this.r[0]) {
        radiusIndex = 0;
      } else if (distToCenter > this.r[1]) {
        radiusIndex = 1;
      } else {
        const [e1, e2] = [
          this.edges[0][edgeIndex],
          this.edges[0][edgeIndex],
        ];
        const target = p5.Vector
          .sub(e2, e1)
          .mult((distToCenter - this.r[0]) / (this.r[1] - this.r[0]));

        return p5.Vector.sub(target, pos);
      }

      return p5.Vector.sub(this.edges[radiusIndex][edgeIndex], pos);
    }

    let radius = distToCenter < this.r[0] ? this.r[0] : this.r[1];
    if (distToCenter > this.r[0] && distToCenter < this.r[1]) {
      radius = this.r[0] + (this.r[1] - this.r[0]) / 2;
      // return this.p.createVector(0, 0);
    }

    const d = p5.Vector.dist(pos, this.pos);
    const v = this.p.createVector(
      (radius - d) * Math.cos(angle),
      (radius - d) * Math.sin(angle),
    );

    return v;
  }
}

export {
  ArcReference,
  RectangleReference,
  CircleReference,
  DoubleArcReference,
};
