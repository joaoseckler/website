/* eslint-disable max-classes-per-file */

import p5 from 'p5';
import Singer from './singer';
import { ArcReference } from './reference';
import { MAX_SECS_BEFORE_RESTART, FRAME_RATE } from './defs';
import { indexChoice } from './util';

class Manager {
  constructor(p) {
    this.p = p;
  }

  update() {
    this.reference = this.p.palco.curr_reference;

    if (this.p.palco.singers === undefined) {
      this.p.palco.singers = Array.from(
        { length: this.p.palco.n_singers },
        () => new Singer(this.p, this.reference),
      );
    }

    this.frames = MAX_SECS_BEFORE_RESTART * FRAME_RATE;
    this.n_singers = this.p.palco.n_singers;

    this.singers = this.p.palco.singers;
    this.reference.update(this.singers.length);
    this.singers.forEach((s) => s.setReference(this.reference));
  }

  resetSingers() {
    this.p.palco.singers.forEach((s) => {
      s.reset();
    });
  }

  addSinger() {
    this.singers.push(new Singer(this.p, this.reference));
    this.reference.update(this.singers.length);
  }

  rmSinger() {
    const index = indexChoice(this.singers);
    this.singers.splice(index, 1);
    this.reference.update(this.singers.length);
  }

  static closestToEdge(edge, singers, exclude = []) {
    let mind = Number.MAX_SAFE_INTEGER;
    let mins = null;

    singers
      .filter((s) => !exclude.includes(s))
      .forEach((singer) => {
        const d = p5.Vector.dist(singer.pos, edge);
        if (d < mind) {
          mind = d;
          mins = singer;
        }
      });

    return mins;
  }

  drawAndReady(singers, reference) {
    reference.draw();

    singers.forEach((s) => {
      s.walk(singers);
      s.draw();
    });

    if (this.p.palco.repeat) {
      this.frames = Math.max(0, this.frames - 1);
    }
    if (this.frames <= 0) {
      return true;
    }

    return this.singers.every((s) => {
      if (!s.ready) {
        return false;
      }
      return this.p.palco.repeat;
    });
  }

  draw() {
    const ready = this.drawAndReady(this.singers, this.reference);
    if (ready) {
      this.resetSingers();
      this.update();
    }
  }
}

class DoubleArcManager extends Manager {
  constructor(p) {
    super(p);
    this.p = p;
  }

  update() {
    if (this.p.palco.singers === undefined) {
      this.p.palco.singers = Array.from(
        { length: this.p.palco.n_singers },
        () => new Singer(this.p),
      );
    }

    this.frames = MAX_SECS_BEFORE_RESTART * FRAME_RATE;
    this.n_singers = this.p.palco.n_singers;

    this.singers = this.p.palco.singers;
    this.n_singers = this.p.palco.n_singers;

    const [n1, n2] = [
      Math.floor(this.n_singers / 2),
      Math.ceil(this.n_singers / 2),
    ];
    const r2 = this.p.palco.curr_reference;
    const r1 = new ArcReference(this.p, 180, [r2.pos.x, r2.pos.y]);

    r2.update(n2);
    r1.update(n1);
    this.reference = [r1, r2];

    this.singers_split = [
      this.singers.slice(0, n1),
      this.singers.slice(n1),
    ];

    this.singers_split[0].forEach((s) => s.setReference(r1));
    this.singers_split[1].forEach((s) => s.setReference(r2));
  }

  addSinger() {
    const splitIndex = this.singers_split[0].length >= this.singers_split[1].length
      ? 1 : 0;

    this.singers_split[splitIndex].push(new Singer(this.p, this.reference[splitIndex]));
    this.reference[splitIndex].update(this.singers_split[splitIndex].length);
    this.singers = this.singers_split[0].concat(this.singers_split[1]);
  }

  rmSinger() {
    const splitIndex = this.singers_split[0].length > this.singers_split[1].length
      ? 0 : 1;
    const singerIndex = indexChoice(this.singers_split[splitIndex]);

    this.singers_split[splitIndex].splice(singerIndex, 1);
    this.reference[splitIndex].update(this.singers_split[splitIndex].length);
    this.singers = this.singers_split[0].concat(this.singers_split[1]);
  }

  draw() {
    const r1 = this.drawAndReady(this.singers_split[0], this.reference[0]);
    const r2 = this.drawAndReady(this.singers_split[1], this.reference[1]);

    if (r1 && r2) {
      this.resetSingers();
      this.update();
    }
  }
}

export { Manager, DoubleArcManager };
