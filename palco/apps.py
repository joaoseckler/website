from django.apps import AppConfig


class PalcoConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "palco"
