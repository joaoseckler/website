from django import template
from django.utils.safestring import mark_safe
from uuid import uuid4

register = template.Library()

@register.inclusion_tag("palco/sketch.html")
def palco(
    n_singers=15,
    formation="arc",
    lazy=False,
    debug=False,
    ui=False,
    repeat=True,
):
    divid = "palcop5_" + str(uuid4())[:8]
    return {
        "n_singers": n_singers,
        "formation": formation,
        "debug": debug,
        "id": divid,
        "lazy": lazy,
        "ui": ui,
        "repeat": repeat,
    }
