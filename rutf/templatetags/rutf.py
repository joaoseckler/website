from pathlib import Path
from django import template
from ..rutf import get_value
from django.utils.safestring import mark_safe
from django.urls import reverse
from django.templatetags.static import static

register = template.Library()


@register.simple_tag()
def utf():
    return mark_safe("&#" + get_value())


@register.simple_tag()
def utf_value():
    return get_value()

@register.simple_tag()
def generic_link(url, label, new_tab=False):
    return mark_safe(
        '<a '
        + ('target="_blank" ' if new_tab else "")
        + 'class="icon-anchor" '
        + 'href="'
        + url
        + '">'
        + ('<span class="label">' + label + "</span>" if label else "")
        + '<span class="utf">&#'
        + get_value()
        + "</span></a>"
    )


@register.simple_tag()
def il(link, label='', new_tab=False):
    """Internal link"""
    return generic_link(reverse("pages:page", args=(link,)), label, new_tab)


@register.simple_tag()
def el(link, label='', new_tab=True):
    """External link"""
    return generic_link(link, label, new_tab)


@register.simple_tag()
def sl(link, label='', new_tab=True):
    """static link"""
    return generic_link(static(link), label, new_tab)

@register.simple_tag(takes_context=True)
def lf(context, link, label='', new_tab=False):
    """Local folder"""
    link = lstatic(context, link, label)
    return generic_link(link, label, new_tab)

@register.simple_tag(takes_context=True)
def lstatic(context, link, label='', new_tab=False):
    """Local static"""
    return static(Path(context["name"]).parent / link)
