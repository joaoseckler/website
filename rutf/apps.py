from django.apps import AppConfig


class RutfConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "rutf"
