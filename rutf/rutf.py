import re, random, os

# TODO: build uv in __init and only access it here

with open(os.path.dirname(__file__) + '/utf_values') as f:
    uv = f.read().split('\n')
if uv[-1] == '':
    uv.pop()

def repl(matchobj):
    global uv
    return matchobj.group(1) + 'class="utf" ' + matchobj.group(2) +\
           '&#' + random.choice(uv) + matchobj.group(3)


def replace_link(string):
    '''
    Replace every <a href... tag in string (html) with a randomly selected utf
    value from the utf-values file. Assign this tag to the "utf" class
    '''
    global uv
    with open(os.path.dirname(__file__) + '/utf_values') as f:
        uv = f.read().split('\n')

    return re.subn(r'(\<a )(.*href.*\>)\s?-\s?(\</a>)', repl, string)[0]

def get_value():
    '''
    Get one random number from the list utf_values
    '''
    return random.choice(uv)
