#!/usr/bin/env python3

import subprocess


# This script creates a text file with values that will serve the other
# script, random-utf.py. You need to execute this only once, and the
# reference file is created. We keep this here for backup and curiosity
# purposes. For each value, it also outputs a corresponding image


GEN_IMAGES = True

in_file = open("utf_values", "w")

starts = [
    9632,
    9698,
    9737,
    9776,
    9898,
    9920,
    10009,
    10018,
    10041,
    10063,
    9600,
    9472,
    9596,
]

stops = [
    9691,
    9728,
    9742,
    9784,
    9907,
    9924,
    10013,
    10024,
    10046,
    10067,
    9632,
    9581,
    9600,
]

exclude = [
    9899,
]

n = sum(map(lambda a: a[1] - a[0], zip(starts, stops)))
i = 0
print()

for start, stop in zip(starts, stops):
    for j in range(start, stop):
        if j not in exclude:
            if GEN_IMAGES:
                print(f"\033[A\r\033[Kprocessing {i}/{n}")
                i += 1
                subprocess.run(
                    [
                        "convert",
                        "-font",
                        "DejaVu-Sans",
                        "-gravity",
                        "center",
                        "-density",
                        "256x256",
                        "-background",
                        "transparent",
                        "-define",
                        "icon:auto-resize",
                        "-colors",
                        "256",
                        "label:@-",
                        "static/" + str(j) + ".ico",
                    ],
                    input=chr(j),
                    text=True,
                )
                subprocess.run(
                    [
                        "convert",
                        "-size",
                        "1200x630",
                        "-font",
                        "DejaVu-Sans",
                        "-gravity",
                        "center",
                        "-pointsize",
                        "400",
                        "label:@-",
                        "static/" + str(j) + "ogp.jpg",
                    ],
                    input=chr(j),
                    text=True,
                )
            in_file.write(str(j) + "\n")

in_file.close()
