cmd=(
  "cd jseckler.xyz &&"
  " git pull origin master --rebase &&"
  " systemctl restart gunicorn &&"
  " systemctl restart nginx"
)

ssh jseckler.xyz "${cmd[*]}"
