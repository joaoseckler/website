import re
from django import forms
from django.utils.translation import gettext_lazy as _
from django.forms.renderers import DjangoTemplates
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe
from django.core.validators import FileExtensionValidator
from pages.widgets import ClearableFileInput

class MarginsWidget(forms.MultiWidget):
    template_name="booklet/marginswidget.html"

    def __init__(self, widgets, labels=None, **kwargs):
        for widget, label in zip(widgets, labels):
            widget.label = label
        super().__init__(widgets, **kwargs)

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)
        for c, w in zip(context["widget"]["subwidgets"], self.widgets):
            c["label"] = w.label

        return context

    def decompress(self, data_list):
        if not data_list:
            return [None] * 5
        return data_list

class MarginsField(forms.MultiValueField):
    valid = re.compile(r"^\s*-?((\d+)?\.)?\d+\s*(pt|mm|cm|in|ex|em|mu|sp)?\s*$")
    has_unit = re.compile(r"pt|mm|cm|in|ex|em|mu|sp")

    attrs={"class": "grid-2-cols-left lp-1 ai-c ji-r"}

    def __init__(self, **kwargs):
        fields = tuple((
            forms.CharField(
                label=label,
                required=False,
                max_length="100",
            )
            for label in (
                _("inner"),
                _("left"),
                _("top"),
                _("right"),
                _("bottom")
            )
        ))

        super().__init__(
            fields=fields,
            require_all_fields=False,
            widget=MarginsWidget(
                labels=[field.label for field in fields],
                widgets=[
                    forms.TextInput(attrs={"class": "mono"})
                    for field in fields
                ],
                attrs={"placeholder": "…"},
            ),
            initial=None,
            **kwargs,
        )

    def _add_units(self, val):
        if not val:
            return "0cm"

        if not self.has_unit.search(val):
            return f"{val.strip()}cm"

        return val.strip()

    def compress(self, data_list):
        if not all(map(
            lambda v: not v or self.valid.match(v),
            data_list
        )):
            raise ValidationError(
                "please use a numbers, maybe followed by units"
            )

        if not any(data_list):
            return None

        return list(map(self._add_units, data_list))

    def get_bound_field(self, *args, **kwargs):
        bf = super().get_bound_field(*args, **kwargs)
        bf.attrs = self.attrs
        return bf

class BookletForm(forms.Form):

    file = forms.FileField(
        label=_("file"),
        required=False,
        widget=ClearableFileInput,
        validators=[
            FileExtensionValidator(["pdf"], _("please provide a pdf file")),
        ]
    )

    paper = forms.ChoiceField(
        label=_("paper type"),
        choices=(
            ("letter", _("letter")),
            ("a2paper", _("a2")),
            ("a3paper", _("a3")),
            ("a4paper", _("a4")),
            ("a5paper", _("a5")),
            ("a6paper", _("a6")),
        ),
        initial="a4paper",
        widget=forms.RadioSelect(),
    )
    orientation = forms.ChoiceField(
        label=_("orientation"),
        choices=(
            ("portrait", _("portrait")),
            ("landscape", _("landscape")),
        ),
        initial="landscape",
        widget=forms.RadioSelect(),
    )
    signature = forms.IntegerField(
        label=_("signature"),
        help_text=_("Number of pages in each signature"),
        initial=16,
        required=False,
    )
    turning_edge = forms.ChoiceField(
        label=_("turning edge"),
        choices=(
            ("long", _("long edge")),
            ("short", _("short edge")),
        ),
        widget=forms.RadioSelect(),
        initial="long",
        help_text=_(
            "Turn pages by the short or long side when printing. If "
            "long edge is chosen, odd pages of resulting file will "
            "appear upside down"
        )
    )
    pages = forms.CharField(
        label=_("pages"),
        help_text=mark_safe(_(
            "Select pages, separated by commas. Hyphens denote "
            "ranges: <code>'5-'</code> means from fifth page until the "
            "end. <code>{}</code> denotes a blank page"
        )),
        widget=forms.TextInput(attrs={
            "placeholder": "{},4,5,10-",
            "class": "mono",
        }),
        max_length=500,
        required=False,
    )
    margins = MarginsField(
        label=_("margins"),
        help_text=_(
            "available units: pt, mm, cm, in, ex, em, mu, sp. "
            "Defaults to cm. Use negative values for trimming"
        ),
        required=False,
    )
    other_options = forms.CharField(
        label=_("other pdfjam options"),
        help_text=mark_safe(_(
            'See available options <a target="_blank" href='
            '"https://github.com/rrthomas/pdfjam/blob/master/pdfjam-help.txt"'
            '>here</a>'
        )),
        widget=forms.Textarea(attrs={
            "placeholder": "--scale 0.7",
            "class": "mono textinput-size",
            "style": False,
            "rows": False,
            "cols": False,
        }),
        required=False,
        max_length=1000,
    )
    has_file = True

    def clean_file(self):
        file = self.cleaned_data['file']
        if not file:
            raise ValidationError(_("Please provide a file"))
        return file

    def clean_pages(self):
        pages = self.cleaned_data['pages']
        if not pages:
            return pages
        if not re.match(r"(({}|\d+|\d*-\d*),)*({}|\d+|\d*-\d*)$", pages):
            raise ValidationError(_("Invalid format"))

        return pages

    def clean_signature(self):
        signature = self.cleaned_data['signature']
        if not signature:
            raise ValidationError(_("Please choose a nonzero signature size"))
        return signature

    def __init__(self, data=None, files=None, *args, **kwargs):
        super().__init__(data, files, *args, **kwargs)

        if files:
            for field, file in files.items():
                ff = self.fields[field]
                ff.widget.filename = file.name
