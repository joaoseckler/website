from django import template
from tagform import tagform_register
from ..forms import BookletForm
from ..booklet import make_booklet

register = template.Library()

tagform_register(
    library=register,
    tag_name="booklet_form",
    Form=BookletForm,
    process=make_booklet,
    template="booklet/form.html"
)
