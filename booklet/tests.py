from django.test import SimpleTestCase
from booklet.booklet import margins_to_trim, process_options

class ProcessOptionsTestCase(SimpleTestCase):
    def test_single_value_no_opt(self):
        self.assertEqual(
            process_options("--xpto"),
            ["--xpto"],
        )

    def test_single_value_with_opt(self):
        self.assertEqual(
            process_options("--xpto aham"),
            ["--xpto", "aham"],
        )

    def test_multiple_values(self):
        self.assertEqual(
            process_options("--xpto aham --xptu ahim --zpto --zpti"),
            ["--xpto", "aham", "--xptu", "ahim", "--zpto", "--zpti"],
        )

    def test_no_values(self):
        self.assertEqual(
            process_options(""),
            [],
        )

    def test_single_quotes(self):
        self.assertEqual(
            process_options("--xpto '1 2 3 4'"),
            ["--xpto", "1 2 3 4"],
        )

    def test_double_quotes(self):
        self.assertEqual(
            process_options('--xpto "1 2 3 4"'),
            ["--xpto", "1 2 3 4"],
        )

    def test_multiple_double_quotes(self):
        self.assertEqual(
            process_options('--xpto "1 2 3 4" --xpti "4 3 2 1"'),
            ["--xpto", "1 2 3 4", "--xpti", "4 3 2 1"],
        )

    def test_multiple_sinlge_quotes(self):
        self.assertEqual(
            process_options("--xpto '1 2 3 4' --xpti '4 3 2 1'"),
            ["--xpto", "1 2 3 4", "--xpti", "4 3 2 1"],
        )

    def test_all_mixed(self):
        self.assertEqual(
            process_options(
                "--uau --xpto \"1 2 3 4\" --zpto hmmm --xptu 'uau ueu uiu' "
                "--zptu \"4 3 2 uau\""
            ),
            [
                "--uau", "--xpto", "1 2 3 4", "--zpto", "hmmm", "--xptu",
                "uau ueu uiu", "--zptu", "4 3 2 uau",
            ]
        )

class BookletMarginParsingTestCase(SimpleTestCase):
    def test_no_inner(self):
        self.assertEqual(
            margins_to_trim(["0px", "1cm", "-.5ex", "0cm", "0cm"]),
            ["--clip", "false", "--trim", "-1cm .5ex -0cm -0cm"]
        )

    def test_only_inner_float(self):
        self.assertEqual(
            margins_to_trim(["2.7sp", "0cm", "0ex", "0sp", "0cm"]),
            ["--clip", "false", "--delta", "5.4sp 0cm"]
        )

    def test_only_inner_int(self):
        self.assertEqual(
            margins_to_trim(["2sp", "0cm", "0ex", "0sp", "0cm"]),
            ["--clip", "false", "--delta", "4sp 0cm"]
        )

    def test_nothing(self):
        self.assertEqual(
            margins_to_trim(["0cm", "0em", "0", "0sp", "0cm"]),
            []
        )

    def test_all(self):
        self.assertEqual(
            margins_to_trim([".5cm", "3em", "-4sp", "3pt", "0cm"]),
            [
                "--clip", "false", "--trim", "-3em 4sp -3pt -0cm",
                "--delta", "1cm 0cm"
            ]
        )


