import subprocess as sp
import re
from pathlib import Path
from django.conf import settings
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _


options_re = re.compile(
    r"""\s*(--[-\w]+)\s*('[^']+'|"[^"]+"|[^-\s"']{2}[^\s"']*|[^-\s"']*)\s*"""
)
def process_options(options):
    if not options:
        return []

    acc = []
    for key, val in re.findall(options_re, options):
        if val:
            acc += [key, val.strip("'\"")]
        else:
            acc += [key]

    return acc

def measure_value(meas):
    return float(re.search(r"((\d+)?\.?)\d+", meas).group(0))

def calc_delta(inner_margin):
    unit = re.search(r"[a-zA-Z]+", inner_margin).group(0)
    val = measure_value(inner_margin) * 2

    if val == 0:
        return []

    return ["--delta", f"{val:g}{unit} 0cm"]

def calc_trim(margins):
    trim = list(map(lambda m: m[1:] if m[0] == "-" else f"-{m}", margins))

    if all(map(lambda m: measure_value(m) == 0, trim)):
        return []

    return ["--trim", " ".join(trim)]

def margins_to_trim(margins):
    if not margins:
        return []

    delta = calc_delta(margins[0])
    trim = calc_trim(margins[1:])

    if not trim and not delta:
        return []

    return ["--clip", "false"] + trim + delta

def get_error(completed):
    folder_gm = (
        re.search(
            (
                "Temporary directory for this job is\n"
                r"\s*(\S*/pdfjam-[^\s/]+)"
            ),
            completed.stderr.decode()
        )
        or re.search(
            (
                "you[\n ]can[\n ]examine[\n ]the[\n ]log[\n ]file[\n ]at\n"
                r"\s*(\S*/pdfjam-[^\s/]+)"
            ),
            completed.stderr.decode()

        )
    )
    print(folder_gm)

    if not folder_gm:
        return {
            "error": _("pdfjam error"),
            "verbose_error": completed.stderr.decode(),
        }
    print(folder_gm.group(1))

    folder = folder_gm.group(1)

    logfile = Path(folder) / "a.log"
    with open(logfile, "r") as f:
        vv = f.read()

    v = "\n".join(set(re.findall(r"^.*[Ee]rror.*$", vv, re.MULTILINE)))

    return {
        "error": _("pdfjam error"),
        "verbose_error": v,
        "verbose_verbose_error": vv,
    }

def make_booklet(options):
    o = options

    key = str(o["session"].session_key)[:6]
    outname = f"booklet_{o['file'].name}"
    outfolder = settings.MEDIA_ROOT / "booklet" / key
    outfile = outfolder / outname
    outurl = Path(settings.MEDIA_URL) / "booklet" / key / outname
    infile = f"/tmp/{o['file'].name}"

    if not o["session"].get("tagform_redirect"):
        if outfile.is_file():
            return {
                "url": outurl,
                "filename": outname,
            }
        return {}

    preamble = ""
    if o["turning_edge"] == "short":
        preamble = (
            r"  \makeatletter"
            r"  \AddToHook{shipout/before}"
            r"{\ifodd\c@page\pdfpageattr{/Rotate 180}\fi}"
            r"  \makeatother"
        )

    cmd = list(map(str, (
        ["pdfjam"]
        + ["--signature", o["signature"]]
        + ["--paper", o["paper"]]
        + [
            "--landscape"
            if o["orientation"] == "landscape"
            else "--no-landscape"
        ]
        + process_options(o["other_options"])
        + ["--outfile", outfile]
        + (["--preamble", preamble] if preamble else [])
        + margins_to_trim(o["margins"])
        + ["--no-tidy"]
        + ["--", infile]
        + ([o["pages"]] if o["pages"] else [])
    )))

    outfolder.mkdir(parents=True, exist_ok=True)
    try:
        with open(infile, "wb") as f:
            for chunck in o['file'].chunks():
                f.write(chunck)
    except OSError as e:
        return {
            "error": _("file error"),
            "verbose_error": e,
        }

    completed = sp.run(cmd, capture_output=True)
    print(completed.stdout.decode())
    print(completed.stderr.decode())

    if completed.returncode != 0:
        return get_error(completed)

    return {
        "url": outurl,
        "filename": outname,
    }

