TurndownService.prototype.escape = (x) => x;
let turndownService = new TurndownService();

turndownService.addRule("h1", {
  filter: "h1",
  replacement: function (content, node) {
    var underline = Array(content.length + 1).join("=");
    return "\n\n" + content + "\n" + underline + "\n\n";
  },
});

turndownService.addRule("h2", {
  filter: "h2",
  replacement: function (content, node) {
    var underline = Array(content.length + 1).join("-");
    return "\n\n" + content + "\n" + underline + "\n\n";
  },
});

turndownService.addRule("bold", {
  filter: function (node, options) {
    const weight = node?.style?.fontWeight;
    return weight && (weight.toLowerCase() === "bold" || weight >= 650);
  },
  replacement: (content) => `**${content}**`,
});

turndownService.addRule("br", {
  filter: "br",
  replacement: () => "\n",
});

// Returns 0 if is not list, 1 if is top level, etc
function findListDepth(node) {
  if (!node.nodeName.match(/LI/i)) {
    return 0;
  }

  if (node.parentNode.nodeName.match(/OL|UL/i)) {
    for (const cl of node.parentNode.classList) {
      const m = cl.match(/list-(number|bullet)(\d+)/);
      if (m) return parseInt(m[2]);
    }
  }

  let count = 0;
  let n = node.parentNode;

  while (n.nodeName.match(/LI|OL|UL/i)) {
    if (n.nodeName.match(/OL|UL/i)) {
      count += 1;
    }
    n = n.parentNode;
  }

  return count;
}

turndownService.addRule("li", {
  filter: "li",
  replacement: (c, node) => {
    const count = findListDepth(node);

    return (
      (node.parentNode.nodeName === "OL" ? "#" : "*").repeat(count) +
      " " +
      c.replace(/^\n+/m, "").replace(/\n*$/m, "\n")
    );
  },
});

turndownService.addRule("a", {
  filter: "a",
  replacement: (content, node, options) => {
    const href = node.href || "";

    return `[${content} -> ${href}]`;
  },
});

function postProcess(text) {
  let ret = text;
  ret = ret.replaceAll(/\n\\((=|-)+\n)/gm, "$1");

  // First run fixes every odd occurrence, second the even ones
  ret = ret.replaceAll(/(\*+ [^\n]+)\n\n(\*+ )/gm, "$1\n$2");
  ret = ret.replaceAll(/(\*+ [^\n]+)\n\n(\*+ )/gm, "$1\n$2");

  ret = ret.replaceAll(/(\#+ [^\n]+)\n\n(\#+ )/gm, "$1\n$2");
  ret = ret.replaceAll(/(\#+ [^\n]+)\n\n(\#+ )/gm, "$1\n$2");

  ret = ret.replaceAll(/([\*\#])\s+\\-/g, "$1 ");

  return ret;
}

function convert(text) {
  const markdown = turndownService.turndown(text);

  return postProcess(markdown);
}

document.addEventListener("DOMContentLoaded", function () {
  const container = document.querySelector(".html2gc");

  document.addEventListener("keydown", function (event) {
    if (event.ctrlKey || event.metaKey) {
      if (String.fromCharCode(event.which).toLowerCase() === "v") {
        container.innerHTML = "";
        container.focus();
      }
    }
  });

  container.addEventListener("paste", function () {
    setTimeout(() => {
      const html = container.innerHTML;
      const markdown = convert(html);

      container.innerHTML = markdown;
    });
  });
});
