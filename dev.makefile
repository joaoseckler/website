MAKEFLAGS:= --jobs=8

.PHONY: md server palco all laialaia_txt laialaia_page

all: md server palco laialaia_txt laialaia_page

md:
	+@. ./venv/bin/activate \
		&& watchmake --quiet --ext md md | sed 's/^/[  md  ]  /'

server:
	@. ./venv/bin/activate \
		&& ./manage.py runserver 0.0.0.0:8080 \
		2>&1 | sed 's/^/[server]  /'

palco:
	@if [ -n "$(PALCO)" ]; then \
		cd palco/assets/; FORCE_COLOR=1 npm run dev | sed 's/^/[  js  ]  /'; \
	fi

laialaia_txt:
	+@. ./venv/bin/activate \
	&& watchmake -q --make-cmd='make laialaia_txt DEV=--development TARGET={}' --ext txt laialaia/raw/txt \
	| sed 's/^/[cifra ]  /'

laialaia_page:
	+@. ./venv/bin/activate \
	&& watchmake -q --make-cmd='make laialaia_md TARGET={}' --ext md laialaia/raw/md \
	| sed 's/^/[cifra ]  /'
