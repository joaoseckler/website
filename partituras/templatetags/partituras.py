import os
from django import template
from django.conf import settings
from django.template.defaultfilters import stringfilter
from django.templatetags.static import static

register = template.Library()

PARTITURAS_ROOT = settings.BASE_DIR / "partituras/static/partituras/"


def parse_line(line):
    ret = ""
    words = (
        line[line.index("=") + 1 :]
        .replace("{", " ")
        .replace("}", " ")
        .replace('"', " ")
        .split()
    )

    for w in words:
        if w[0] != "\\":
            if ret != "":
                ret += " "
            ret += w

    return ret


def parse_file(file):
    active = 0
    ret = {}

    for line in file.readlines():
        if r"\header {" in line:
            active = 1
        if active:
            active += line.count("{")
            active -= line.count("}")

            if "title" in line and "subtitle" not in line:
                ret["title"] = parse_line(line)
            elif "composer" in line:
                ret["composer"] = parse_line(line)

        if "title" in ret and "composer" in ret:
            break

    return ret


def parse_all():
    ret = []

    for file in os.listdir(PARTITURAS_ROOT):
        if file.endswith(".ly"):
            with open(PARTITURAS_ROOT / file, "r") as f:
                new = parse_file(f)
                if "title" in new and "composer" in new:
                    new["url"] = "partituras/" + file[:-3] + ".pdf"
                    new["source_url"] = "partituras/" + file

                    ret.append(new)
    ret.sort(key=lambda x: x.get('composer'))
    return ret


@register.simple_tag()
def partituras():
    return parse_all()
