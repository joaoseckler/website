from django import forms
from django_comments.forms import CommentForm, CommentDetailsForm
from django.utils.translation import pgettext_lazy, gettext_lazy as _
from django.conf import settings
from django.core.exceptions import ValidationError

COMMENT_MAX_LENGTH = getattr(settings, "COMMENT_MAX_LENGTH", 3000)

class CustomComment(CommentForm):
    name = forms.CharField(
        label=pgettext_lazy("Person name", "Name"),
        max_length=50,
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("name")}),
    )
    email = forms.EmailField(
        label=_("Email address"),
        required=False,
        widget=forms.TextInput(attrs={"placeholder": _("email")}),
    )
    url = forms.URLField(label=_("URL"), required=False, disabled=True)

    comment = forms.CharField(
        label=_("Comment"),
        widget=forms.Textarea(attrs={"placeholder": "…"}),
        max_length=COMMENT_MAX_LENGTH,
        required=False,
    )

    def clean_comment(self):
        comment = self.cleaned_data['comment']
        if not comment:
            raise ValidationError(_("Please share something non-empty with us"))
        return comment

