from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _


class User(AbstractUser):
    repetition_can_upload = models.BooleanField(
        default=False, verbose_name=_("Can upload to repetition app?")
    )

    class Meta:
        db_table = "auth_user"
