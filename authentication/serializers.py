from dj_rest_auth.serializers import UserDetailsSerializer as BaseUserDetailsSerializer


class UserDetailsSerializer(BaseUserDetailsSerializer):
    class Meta(BaseUserDetailsSerializer.Meta):
        fields = [*BaseUserDetailsSerializer.Meta.fields, "repetition_can_upload"]
