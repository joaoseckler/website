from django.apps import AppConfig


class AutodeleteConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "autodelete"
