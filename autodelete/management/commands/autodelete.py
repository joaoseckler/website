from django.core.management.base import BaseCommand, CommandError
import os
from datetime import datetime, timedelta
from pathlib import Path
from django.conf import settings

class Command(BaseCommand):
    help = "Autodelete media files"

    def handle(self, *args, **options):
        cutoff = datetime.now() - timedelta(days=1)
        done_something = False

        folders = getattr(settings, "AUTODELETE_FOLDERS", [])
        for folder in folders:
            for file in Path(folder).rglob("*"):
                if file.is_file():
                    mtime = datetime.fromtimestamp(file.stat().st_mtime)
                    if mtime < cutoff:
                        f = str(file).replace(str(settings.MEDIA_ROOT), '')
                        self.stdout.write(f"deleting file {f}...")
                        done_something = True
                        file.unlink()
            for file in sorted(Path(folder).rglob("*")):
                if file.is_dir() and not any(file.iterdir()):
                    self.stdout.write(f"removing dir {file}...")
                    file.rmdir()
        if not done_something:
            self.stdout.write("nothing to delete.")


