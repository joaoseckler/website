from datetime import datetime, timedelta
from os import utime
from io import StringIO
from pathlib import Path
from itertools import product
from django.core.management import call_command
from django.test import SimpleTestCase, override_settings
from tempfile import TemporaryDirectory

class AutodeleteTestCase(SimpleTestCase):
    folders = "aaaa", "bbbb"
    filenames = ["aaaa.pdf", "bbbb.jpg", "cccc.txt"]

    def setUp(self):
        self._td = TemporaryDirectory()
        self.td = Path(self._td.name)

    def tearDown(self):
        self._td.cleanup()

    def all_filenames(self):
        return map(
            lambda a: self.td / a[0] / a[1] / a[2],
            product(self.folders, self.folders, self.filenames)
        )

    def populate(self, d, time=None):
        for f in self.all_filenames():
            f.parent.mkdir(parents=True, exist_ok=True)
            f.touch()
            if time:
                utime(f, (time.timestamp(), time.timestamp()))

    def test_rm_new_files(self):
        with self.settings(AUTODELETE_FOLDERS=[self.td]):
            self.populate(self.td)

            out = StringIO()
            call_command("autodelete", stdout=out)

            output = out.getvalue()
            self.assertNotIn("deleting", output)
            self.assertNotIn("removing", output)
            self.assertIn("othing to delete", output)

            for f in self.all_filenames():
                self.assertTrue(f.is_file())

    def test_rm_old_files(self):
        cutoff = datetime.now() - timedelta(days=2)

        with self.settings(AUTODELETE_FOLDERS=[self.td]):
            self.populate(self.td, time=cutoff)

            out = StringIO()
            call_command("autodelete", stdout=out)
            output = out.getvalue()

            self.assertIn("deleting", output)
            self.assertIn("removing", output)
            self.assertNotIn("othing to delete", output)

            for f in self.all_filenames():
                self.assertFalse(f.is_file())
