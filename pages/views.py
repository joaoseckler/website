from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404
from django.template import Template, RequestContext
from django.template.loader import render_to_string
from django.utils import translation
from django.utils.safestring import mark_safe
from .models import Page
from rutf import rutf
from django_comments import get_form

from bs4 import BeautifulSoup

TEMPLATE_HEAD = '{% load rutf page_tags static partituras %}\n'

def set_resources(page, context):
    rel = page.pageresource_set
    context["resources"] = {
        "css": rel.filter(type_location="css"),
        "js_body": rel.filter(type_location="js_body"),
        "js_head": rel.filter(type_location="js_head"),
    }

def index(request):
    if request.LANGUAGE_CODE == 'pt':
        return view(request, 'index')
    return view(request, 'index_en')

def view(request, name):
    o = get_object_or_404(Page, name=name)

    context = {}
    context['o'] = o
    context['footerlink'] = o.footerlink
    set_resources(o, context)

    translation.activate(o.language)
    md_context = RequestContext(request, {"name": o.name})
    md_template = Template(TEMPLATE_HEAD + o.text)

    context['text'] = mark_safe(
        html_tweaks(md_template.render(md_context))
    )
    o.views += 1
    o.save()

    return base(request, 'pages/page.html', context)

def comments(request, name):
    o = get_object_or_404(Page, name=name)
    if not o.hascomments:
        raise Http404('Page has no comment section')

    context = {}
    context['form'] = get_form()(o.comment)
    context['o'] = o
    context['footerlink'] = name
    context['style'] = 'pages/comments.css'

    return base(request, 'pages/comments.html', context)

def meta(request, name):
    o = get_object_or_404(Page, name=name)

    context = {}
    context['o'] = o
    context['footerlink'] = name
    return base(request, 'pages/meta.html', context)

def base(request, template, context):
    if not context['o'].visible:
        raise Http404()
    translation.activate(context['o'].language)
    if "text" in context:
        context["math"] = 'class="arithmatex"' in context["text"]

    return HttpResponse(render(request, template, context))


def html_tweaks(html):
    """Use random utf symbols for permanent links to titles"""
    bs = BeautifulSoup(html, "html.parser")
    first = True
    for p in bs.find_all(class_="headerlink"):
        if first:
            first = False
            p.decompose()
            continue
        p.string.replace_with(chr(int(rutf.get_value())))
        p["class"] = p.get("class", []) + ["utf"]

    return str(bs)
