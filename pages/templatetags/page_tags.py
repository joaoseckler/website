from pathlib import Path
from django import template
from django.templatetags.static import static
from django.urls import reverse

# Thanks to https://stackoverflow.com/questions/844746/performing-a-getattr-style-lookup-in-a-django-template


register = template.Library()

@register.filter()
def getattrfilter(o, attr):
    try:
        return getattr(o, attr)
    except:
        return ''
@register.filter(name="to_classname")
def forloopcounter2classname(counter):
    return "comment1" if counter % 2 else "comment0"

@register.simple_tag()
def absolute_url(request, view, name):
    return 'https://' + request.get_host() + reverse(view, args=(name,))

@register.simple_tag()
def absolute_static(request, name):
    return 'https://' + request.get_host() + static(name)

# TODO: allow something other than local file
@register.inclusion_tag("pages/video.html", takes_context=True)
def video(context, url, *formats, name):
    return {
        "url": static(Path(context["name"]).parent / url),
        "formats": list(formats),
        "name": name,
    }
