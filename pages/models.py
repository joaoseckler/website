from django.db import models

# this is a dummy class for the comments to attach to. This way english
# and portuguese comments are together
class PageComment(models.Model):
    name_pt = models.CharField('pt version', max_length=200, null=True)
    name_en = models.CharField('en version', max_length=200, null=True)

class Page(models.Model):
    # meta data
    name = models.CharField('Page id', max_length=200)
    pub_date = models.DateTimeField('Date of publication')
    last_mod_date = models.DateTimeField('Date of last modification')
    views = models.IntegerField('Number of accesses', default = 0)
    visible = models.BooleanField('Visibe?', default=True)

    # html
    title = models.CharField('Page title', max_length=50)
    keywords = models.CharField('Keywords', max_length=400)
    shortdescription = models.CharField('Keywords', max_length=400)

    # meta
    footerlink = models.CharField('Where to go back to', max_length=200)
    hascomments = models.BooleanField('Has comments?', default=False)
    nofooter = models.BooleanField('No footer?', default=False)
    language = models.CharField('Language', max_length=5)
    en_version = models.CharField('English version',
            max_length=200,
            null=True)
    pt_version = models.CharField('Portuguese version',
            max_length=200,
            null=True)
    comment = models.ForeignKey(PageComment, on_delete=models.CASCADE, null=True)

    # Actual text
    text = models.TextField('html text', null=True)

    def __str__(self):
        return self.name

class PageResource(models.Model):
    resource = models.CharField('url or static path', max_length=500)
    type_location = models.CharField(
        'type and location',
        max_length=7,
        choices=(
            ('css', 'CSS'),
            ('js_head', 'javascript in head'),
            ('js_body', 'javascript in body'),
        )
    )
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    attrs = models.CharField(
        'Additional attributes',
        max_length=500,
        null=True
    )
    is_static = models.BooleanField("Is static?", default=False)

    def __str__(self):
        return f"{self.page}: {self.type_location}"
