from django.forms import ClearableFileInput as CFI

class ClearableFileInput(CFI):
    template_name = "pages/file_input.html"

    def get_context(self, *args, **kwargs):
        context = super().get_context(*args, **kwargs)
        filename = getattr(self, "filename", None)
        if filename:
            context["filename"] = filename
        return context
