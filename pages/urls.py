from django.urls import path, re_path, include
from . import views
from custom_comments.views import post_comment
import os, re

app_name = 'pages'
urlpatterns = [
    re_path(r'^$', views.index, name='index'),
    re_path(
        r'^(?P<name>.+)/(comments|discussão)/post$',
        post_comment,
        name='post'
    ),
    re_path(
        r'^(?P<name>.+)/(comments|discussão)$',
        views.comments,
        name='comments'
    ),
    re_path(r'^(?P<name>.+)/meta$', views.meta, name='meta'),
    re_path(r'^(?P<name>.+)$', views.view, name='page'),
]
