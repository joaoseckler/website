function clipboard() {
  function fallbackCopyTextToClipboard(text) {
    var textArea = document.createElement("textarea");
    textArea.value = text;

    // Avoid scrolling to bottom
    textArea.style.top = "0";
    textArea.style.left = "0";
    textArea.style.position = "fixed";
    textArea.style.opacity = "0";

    document.body.appendChild(textArea);
    textArea.focus();
    textArea.select();

    var successful = document.execCommand('copy');
    document.body.removeChild(textArea);
  }

  function copyTextToClipboard(text) {
    if (!navigator.clipboard) {
      fallbackCopyTextToClipboard(text);
      return;
    }
    navigator.clipboard.writeText(text)
  }

  for (const el of document.querySelectorAll(".clipboard-copy, .codehilite")) {
    el.addEventListener("click", e => copyTextToClipboard(el.innerText))
  }
}

function fileSelectors() {
  for (const el of document.querySelectorAll(".file-label")) {
    const input = el.querySelector("input");
    const sel = el.querySelector("span");

    input.addEventListener("change", e => {
      if (e.target.files)
        sel.innerText = "(" + e.target.files[0].name + ")"
    })
  }
}

function video() {
  const onPause = (e) => {
    e.target.dataset.playing = "false"
  }

  const onPlay = (e) => {
    e.target.dataset.playing = "true";
    const interval = setInterval(() => {

      e.target.scrollIntoView();
      console.log("aqui")
    }, 100)

    setTimeout(() => {
      clearInterval(interval);
    }, 550)
  }


  for (const video of document.querySelectorAll("video")) {
    video.addEventListener("pause", onPause);
    video.addEventListener("play", onPlay);
    video.style.setProperty("--_old-width", video.offsetWidth + "px");
  }
}


clipboard()
video()
fileSelectors()
