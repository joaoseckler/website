#!/usr/bin/env python3

import markdown, os, re, yaml, sys, warnings
from os.path import join as j, dirname, isfile
from django.utils import timezone
from bs4 import BeautifulSoup
from bs4 import MarkupResemblesLocatorWarning

warnings.filterwarnings("ignore", category=MarkupResemblesLocatorWarning)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'joaoseckler.settings')

import django
django.setup()

from pages.models import Page, PageComment


if len(sys.argv) == 3 and sys.argv[1] == '--delete-post':
    try:
        p = Page.objects.get(name=sys.argv[2])
    except Page.DoesNotExist:
        print('Page', sys.argv[2], 'not found')
        exit()

    print('Making', sys.argv[2], 'invisible...')
    p.visible = False
    p.save()
    exit()

if len(sys.argv) > 2:
    print("Use --delete-post to make posts invisible.")
    print("Or pass no arguments to update and create posts inside the md folder")
    exit()

MD_FOLDER = 'md/'
TIMESTAMPS_FILE = '.makepages.yml'

##### List files for changing ########

def readfiles(d, folder = ''):
    ''' Puts all the files inside MD_FOLDER/folder
        and their timestamps in a dict
    '''
    for f in os.listdir(j(MD_FOLDER, folder)):
        if os.path.isdir(j(j(MD_FOLDER, folder), f)):
            readfiles(d, os.path.join(folder, f))
        elif f[-3:] == '.md':
            d[os.path.join(folder, f)] =\
                    os.path.getmtime(j(j(MD_FOLDER, folder), f))

    d[__file__] = os.path.getmtime(__file__)

new = {}
readfiles(new)

open(TIMESTAMPS_FILE, 'a').close()
with open(TIMESTAMPS_FILE, 'r', encoding='utf-8') as f:
    old = yaml.load(f.read(), Loader=yaml.FullLoader)
with open(TIMESTAMPS_FILE, 'w', encoding='utf-8') as f:
    f.write(yaml.dump(new))

if (old and __file__ in old and old[__file__] != new[__file__]) or not old:
    changed = list(filter(lambda x: x != __file__, new))
else:
    changed = list(filter(lambda x: x != __file__ and (x not in old or new[x] != old[x]), new))

##### Markdown configuration #########

def md_conf():
    ret = {}
    ret["extensions"] = [
        "meta",
        "codehilite",
        "fenced_code",
        "toc",
        "attr_list",
        "pymdownx.arithmatex",
    ]
    ret["extension_configs"] = {
        "toc": {
            "permalink": " ",
            "permalink_title": "permanent link"
        },
        "pymdownx.arithmatex": {
            "generic": True,
        }
    }

    return ret

def replace_quotes(text):
    """Replace quotes by fancy quotes ignoring django templating"""

    text = list(text)
    opening = True
    stack = []

    for i in range(len(text)):
        pair = "".join(text[i:i+2])
        if pair in ("{%", "{#", "{{"):
            stack.append(pair[1] + "}")
            continue
        if text[i] == "<":
            stack.append(">")
            continue
        if pair in ("%}", "#}", "}}"):
            if len(stack) > 1 and stack[-2:] == pair:
                stack.pop()
            continue
        if text[i] == ">":
            if len(stack) > 0 and stack[-1] == text[i]:
                stack.pop()
            continue

        if not stack and text[i] == '"':
            if opening:
                text[i] = '“'
            else:
                text[i] = '”'
            opening = not opening

    return "".join(text)


def html_tweaks(html):
    """Use fancy quotation marks"""
    bs = BeautifulSoup(html, "html.parser")

    for e in (*bs.find_all("p"), *bs.find_all("li")):
        ih = replace_quotes(e.decode_contents())

        e.clear()
        e.append(BeautifulSoup(ih, "html.parser"))

    return str(bs)

######### Register changes in db ##########

def parse_resources(l):
    acc = []
    attrs = []
    resource = ""
    is_static = True

    first_line = True
    for line in l:
        line = line.strip()

        if (
            line[:4] == "url "
            or line[:7] == "static "
            or line[:4] == "http"
            and resource
        ):
            acc.append((resource, " ".join(attrs), is_static))
            resource = ""
            attrs = []
            is_static = True

        if line[:4] == "url ":
            resource = line[4:]
            is_static = False
        elif line[:7] == "static ":
            resource = line[7:]
            is_static = True
        elif line[:4] == "http":
            is_static = False
            resource = line
        elif first_line:
            is_static = True
            resource = line
        else:
            attrs.append(line)

        first_line = False

    if resource:
        acc.append((resource, " ".join(attrs), is_static))
        resource = ""
        attrs = []
        is_static = True

    return acc

def create_resources(p, context):
    p.save()
    for pr in p.pageresource_set.all():
        pr.delete()
    for key, value in context.items():
        if key in ("js_head", "js_body", "css"):
            for res, attrs, is_static in parse_resources(value):
                p.pageresource_set.create(
                    resource=res,
                    type_location=key,
                    attrs=attrs,
                    is_static=is_static,
                )

if not changed:
    print("Nothing to be changed.")
    exit

for filename in changed:
    print('Changing', filename, '...')
    name = filename[:-3]

    md = markdown.Markdown(**md_conf())
    with open(os.path.join(MD_FOLDER, filename), encoding='utf-8') as f:
        html = html_tweaks(md.convert(f.read()))

    context = {
        c: (
            '\n'.join(md.Meta[c])
            if c not in ("js_head", "js_body", "css")
            else md.Meta[c]
        )
        for c in md.Meta
    }

    # If title is not set, get it from the body
    if 'title' not in context:
        bs = BeautifulSoup(html, 'html.parser')
        for b in bs.h1, bs.h2, bs.h3, bs.h4:
            if b:
                context['title'] = b.text
                break

    if 'footerlink' not in context:
        t = dirname(filename[:])
        while (t != ''):
            if isfile(j(MD_FOLDER, t + '.md')):
                context['footerlink'] = t
                break
            t = dirname(t)

    # Support both hascomments and nocommments for md files
    if 'nocomments' in context and 'hascomments' not in context:
        context['hascomments'] = False
        del context['nocomments']

    try:
        p = Page.objects.get(name=name)
    except Page.DoesNotExist:
        p = Page(name = name, pub_date=timezone.now(), views = 0)


    p.last_mod_date = timezone.now()

    for i in 'title', 'keywords', 'shortdescription', 'footerlink':
        if i not in context or context[i] == None:
            setattr(p, i, '')
        else:
            setattr(p, i, context[i])

    for i in 'nofooter',:
        if i not in context:
            setattr(p, i, False)
        else:
            setattr(p, i, context[i])

    for i in 'hascomments', 'visible':
        if i not in context:
            setattr(p, i, True)
        else:
            setattr(p, i, context[i])

    if 'en_version' in context:
        p.en_version = context['en_version']
        p.language = 'pt'
    elif 'pt_version' in context:
        p.pt_version = context['pt_version']
        p.language = 'en'

    if 'lang' in context:
        p.language = context['lang']

    create_resources(p, context)
    p.text = html

    p.save()

######### Mutualize language version references ############

for p in Page.objects.all():
    if p.pt_version != None:
        try:
            q = Page.objects.get(name=p.pt_version)
            if q.en_version != None:
                if q.en_version != p.name:
                    print("Warning: misreferenced pages:")
                    print("\n%s is pt_version of %s" % (p.pt_version, p))
                    print("\n%s is en_version of %s" % (q.en_version, q))
            else:
                q.en_version = p.name
                q.save()

        except Page.DoesNotExist:
            print("Warning: non existent pt_version: %s" % p.pt_version)

    # The same as above but en switched with pt
    elif p.en_version != None:
        try:
            q = Page.objects.get(name=p.en_version)
            if q.pt_version != None:
                if q.pt_version != p.name:
                    print("Warning: misreferenced pages:")
                    print("\t%s is en_version of %s" % (p.en_version, p))
                    print("\t%s is pt_version of %s" % (q.pt_version, q))
            else:
                q.pt_version = p.name
                q.save()

        except Page.DoesNotExist:
            print("Warning: non existent en_version: %s" % p.en_version)

######### Create comment object references ##############

for p in Page.objects.filter(language='pt'):
    if p.en_version:
        q = Page.objects.get(name=p.en_version)
        if q:
            if q.comment:
                p.comment = q.comment
            elif p.comment:
                q.comment = p.comment
            else:
                c = PageComment(name_pt=p, name_en=q)
                c.save()
                q.comment = c
                p.comment = c
            p.save()
            q.save()

for p in Page.objects.filter(comment=None):
    c = PageComment()
    if p.language == 'pt':
        c.pt_version = p.name
    elif p.language == 'en':
        c.en_version = p.name
    c.save()
    p.comment = c
    p.save()
