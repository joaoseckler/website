from django.db import models
from django.dispatch import receiver

from repetition.models import Asset

# thanks to Anton Strogonoff
# https://stackoverflow.com/a/16041527/22174384


@receiver(models.signals.post_delete, sender=Asset)
def auto_delete_file_on_delete(sender, instance: Asset, **kwargs):
    """
    Deletes file from filesystem
    when corresponding `MediaFile` object is deleted.
    """
    if instance.file and not instance.file_is_copy:
        instance.file.delete(save=False)


@receiver(models.signals.pre_save, sender=Asset)
def auto_delete_file_on_change(sender, instance: Asset, **kwargs):
    """
    Deletes old file from filesystem
    when corresponding `MediaFile` object is updated
    with new file.
    """
    if not instance.pk:
        return False

    try:
        old_file = Asset.objects.get(pk=instance.pk).file
    except Asset.DoesNotExist:
        return False

    new_file = instance.file
    if not old_file == new_file and not instance.file_is_copy:
        old_file.delete(save=False)
