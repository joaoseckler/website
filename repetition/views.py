from django.contrib.auth import get_user_model
from django.db.models import Q
from django.utils.text import slugify
from fsrs import Card, Rating
from rest_framework import generics, permissions
from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import NotFound, ValidationError
from rest_framework.parsers import FormParser, JSONParser, MultiPartParser
from rest_framework.response import Response

from .models import Asset, Repertoire, Song, fsrf_scheduler
from .permissions import HasOwnedFieldOrReadonly, IsOwnerOrReadOnly
from .serializers import AssetSerializer, RepertoireSerializer, SongSerializer

User = get_user_model()


class PublicRepertoireList(generics.ListAPIView):
    serializer_class = RepertoireSerializer

    def get_queryset(self):
        qs = Repertoire.objects.filter(public=True)
        if self.request.user.is_anonymous:
            return qs
        return qs.filter(~Q(owner=self.request.user))


class RepertoireList(generics.ListCreateAPIView):
    serializer_class = RepertoireSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,
    ]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        username = self.kwargs["username"]
        if self.request.user.username != username:
            raise NotFound
        return Repertoire.objects.filter(owner=self.request.user)


class RepertoireDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = RepertoireSerializer

    lookup_field = "slug"
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
        IsOwnerOrReadOnly,
    ]

    def get_queryset(self):
        username = self.kwargs["username"]
        user = self.request.user
        if user.is_anonymous or user.username != username:
            return Repertoire.objects.filter(owner__username=username, public=True)
        return Repertoire.objects.filter(owner=user)


class SongList(generics.ListCreateAPIView):
    serializer_class = SongSerializer
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
        HasOwnedFieldOrReadonly("repertoire"),
    ]

    def perform_create(self, serializer):
        repertoire = Repertoire.objects.get(
            owner=self.request.user, slug=self.kwargs["slug"]
        )
        serializer.save(repertoire=repertoire)

    def get_queryset(self):
        username = self.kwargs["username"]
        rep_slug = self.kwargs["slug"]

        user = self.request.user

        if user.is_anonymous or user.username != username:
            return Song.objects.filter(
                repertoire__slug=rep_slug, repertoire__public=True
            )

        return Song.objects.filter(repertoire__owner=user, repertoire__slug=rep_slug)


class SongDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = SongSerializer
    lookup_field = "slug"
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
        HasOwnedFieldOrReadonly("repertoire"),
    ]

    def get_queryset(self):
        username = self.kwargs["username"]
        rep_slug = self.kwargs["rep_slug"]

        user = self.request.user
        qs = Song.objects.filter(
            repertoire__owner__username=username,
            repertoire__slug=rep_slug,
        )

        if user.is_anonymous or user.username != username:
            return qs.filter(public=True)

        return qs.filter(repertoire__owner=user)


class AssetList(generics.ListCreateAPIView):
    serializer_class = AssetSerializer
    parser_classes = [MultiPartParser, FormParser, JSONParser]
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
        HasOwnedFieldOrReadonly("song__repertoire"),
    ]

    def perform_create(self, serializer):
        repertoire = Repertoire.objects.get(
            owner=self.request.user, slug=self.kwargs["rep_slug"]
        )
        song = repertoire.songs.get(slug=self.kwargs["slug"])
        name = serializer.validated_data.get("name")

        if name and song.assets.filter(slug=slugify(name)):
            raise ValidationError({"name": ["Name already exists!"]})

        serializer.save(song=song)

    def get_queryset(self):
        username = self.kwargs["username"]
        rep_slug = self.kwargs["rep_slug"]
        song_slug = self.kwargs["slug"]

        user = self.request.user
        qs = Asset.objects.filter(
            song__slug=song_slug,
            song__repertoire__slug=rep_slug,
            song__repertoire__owner__username=username,
        )

        if user.is_anonymous or user.username != username:
            return qs.filter(public=True)

        return qs.filter(song__repertoire__owner=user)


class AssetDetail(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = AssetSerializer
    parser_classes = [MultiPartParser, FormParser, JSONParser]
    lookup_field = "slug"
    permission_classes = [
        permissions.IsAuthenticatedOrReadOnly,
        HasOwnedFieldOrReadonly("song__repertoire"),
    ]

    def get_queryset(self):
        username = self.kwargs["username"]
        rep_slug = self.kwargs["rep_slug"]
        song_slug = self.kwargs["song_slug"]

        user = self.request.user
        qs = Asset.objects.filter(
            song__slug=song_slug,
            song__repertoire__slug=rep_slug,
            song__repertoire__owner=user,
        )

        if user.is_anonymous or user.username != username:
            return qs.filter(public=True)

        return qs


@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def updateCard(request, username, rep_slug, slug):
    try:
        song = Song.objects.get(
            repertoire__owner__username=username,
            repertoire__slug=rep_slug,
            slug=slug,
        )
    except Song.DoesNotExist:
        raise NotFound

    if not request.data or not request.data.get("rating"):
        raise ValidationError({"rating": ["rating field is required"]})

    rating = request.data["rating"]

    if rating not in Rating:
        raise ValidationError({"rating": ["rating value not valid"]})

    card = Card.from_dict(song.card)
    card, _ = fsrf_scheduler.review_card(card, rating)

    song.card = card.to_dict()
    song.save()

    return Response({"message": f"Card updated with rating {rating} sucessfuly"})


@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def reorderAssets(request, username, rep_slug, slug):
    try:
        song = Song.objects.get(
            repertoire__owner__username=username,
            repertoire__slug=rep_slug,
            slug=slug,
        )
    except Song.DoesNotExist:
        raise NotFound

    if not request.data or not request.data.get("order"):
        raise ValidationError({"order": ["order field is required"]})

    slugs = request.data["order"]

    pks = {obj.slug: obj.pk for obj in song.assets.all()}
    song.set_asset_order([pks[slug] for slug in slugs])

    return Response({"message": "Assets reordered successfully"})


@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def copySong(request, username, rep_slug, slug):
    try:
        source = Song.objects.get(
            repertoire__owner__username=username,
            repertoire__slug=rep_slug,
            slug=slug,
        )
    except Song.DoesNotExist:
        raise NotFound

    if not (dest_slug := request.data.get("repertoire_slug")):
        raise ValidationError(
            {"repertoire_slug": ["repertoire_slug field is required"]}
        )

    dest = Repertoire.objects.get(owner=request.user, slug=dest_slug)
    if dest.songs.filter(slug=source.slug).exists():
        raise ValidationError(
            {
                "non_field_errors": [
                    f'Song "{source.name}" already exists in repertoire "{dest.name}"'
                ]
            }
        )

    new = source.copy_to(dest)
    return Response(SongSerializer(new, context={"request": request}).data)


@api_view(["POST"])
@permission_classes([permissions.IsAuthenticated])
def copyRepertoire(request, username, slug):
    try:
        source = Repertoire.objects.get(
            owner__username=username,
            slug=slug,
        )
    except Song.DoesNotExist:
        raise NotFound

    songs = source.songs.all()

    new_name = request.data.get("new_name")
    if not new_name:
        raise ValidationError({"new_name": ["Can't be empty"]})

    song_slugs = request.data.get("song_slugs")
    if song_slugs:
        songs = songs.filter(slug__in=song_slugs)

    if Repertoire.objects.filter(owner=request.user, name=new_name).exists():
        raise ValidationError(
            {"non_field_errors": [f'Repertoire "{source.name}" already exists']}
        )

    new: Repertoire = source.copy_to(request.user, new_name)
    for song in songs:
        if song.public:
            song.copy_to(new)

    return Response(RepertoireSerializer(new, context={"request": request}).data)
