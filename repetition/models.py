from __future__ import annotations

from datetime import datetime
from random import random
from typing import TYPE_CHECKING

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.crypto import get_random_string
from django.utils.text import slugify
from fsrs import Card, Scheduler

if TYPE_CHECKING:
    from authentication.models import User as UserType

User = get_user_model()


def get_sentinel_user():
    return User.objects.get_or_create(username="deleted", first_name="(deleted)")[0]


class Repertoire(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, blank=True)
    owner = models.ForeignKey(
        User, on_delete=models.SET(get_sentinel_user), related_name="repertoires"
    )
    public = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=["slug", "owner"], name="unique_slug_owner")
        ]

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super().save(*args, **kwargs)

    def reorder(self):
        data = sorted(
            (obj.due().date(), obj.card["state"], random(), obj.pk)
            for obj in self.songs.all()
        )
        self.set_song_order([tup[-1] for tup in data])

    def copy_to(self, user: UserType, name: str):
        return user.repertoires.create(name=name)

    def __str__(self):
        return self.name


def create_card():
    return Card().to_dict()


class Song(models.Model):
    repertoire = models.ForeignKey(
        Repertoire, on_delete=models.CASCADE, related_name="songs"
    )
    name = models.CharField(max_length=200)
    slug = models.SlugField(max_length=200, blank=True)
    artist = models.TextField(blank=True, max_length=200)
    public = models.BooleanField(default=True)
    card = models.JSONField(default=create_card)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)
        self.repertoire.reorder()

    def __str__(self):
        return f"{self.name} ({self.artist})"

    def due(self):
        return datetime.fromisoformat(self.card["due"])

    def copy_to(self, repertoire: Repertoire):
        new = repertoire.songs.create(name=self.name, artist=self.artist)

        for asset in self.assets.all():
            new.assets.create(
                name=asset.name,
                type=asset.type,
                value=asset.value,
                file=asset.file,
                file_is_copy=bool(asset.file),
            )
        return new

    @property
    def prev_slug(self):
        try:
            return self.get_previous_in_order().slug
        except self.__class__.DoesNotExist:
            return None

    @property
    def next_slug(self):
        try:
            return self.get_next_in_order().slug
        except self.__class__.DoesNotExist:
            return None

    def _public_get_next_or_previous_slug_in_order(self, *, is_next=True):
        cachename = f"__{is_next}_public_slug_order_cache"
        if not hasattr(self, cachename):
            op = "gt" if is_next else "lt"
            order = "_order" if is_next else "-_order"
            order_field = self._meta.order_with_respect_to
            filter_args = order_field.get_filter_kwargs_for_object(self)
            my_order = self.__class__._default_manager.values("_order").filter(
                pk=self.pk
            )
            slug = (
                self.__class__._default_manager.filter(
                    **filter_args, public=True, **{f"_order__{op}": my_order}
                )
                .order_by(order)[:1]
                .get()
            ).slug

            setattr(self, cachename, slug)
            return slug
        return getattr(self, cachename)

    @property
    def prev_public_slug(self):
        return self._public_get_next_or_previous_slug_in_order(is_next=False)

    @property
    def next_public_slug(self):
        return self._public_get_next_or_previous_slug_in_order(is_next=True)

    class Meta:
        order_with_respect_to = "repertoire"
        constraints = [
            models.UniqueConstraint(
                fields=["slug", "repertoire"], name="unique_slug_per_repertoire"
            )
        ]


def upload_to(instance: Asset, filename: str):
    username = instance.song.repertoire.owner.username
    rep_slug = instance.song.repertoire.slug
    song_slug = instance.song.slug
    return f"repetition/{username}/{rep_slug}/{song_slug}/{filename}"


class Asset(models.Model):
    song = models.ForeignKey(Song, on_delete=models.CASCADE, related_name="assets")
    slug = models.SlugField(max_length=200, blank=True)
    name = models.CharField(max_length=200, blank=True, null=True)
    type = models.CharField(max_length=20)
    value = models.TextField(blank=True)
    public = models.BooleanField(default=True)
    file = models.FileField(null=True, upload_to=upload_to)
    file_is_copy = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.name:
            self.slug = slugify(self.name)
        elif not self.slug:
            self.slug = f"asset-{get_random_string(4)}"

        if self.file and not self.song.repertoire.owner.repetition_can_upload:
            self.file = None

        old_url = self.file.url if self.file else None

        super().save(*args, **kwargs)

        new_url = self.file.url if self.file else None

        if old_url != new_url:
            self.value = new_url
            self.save()

    def __str__(self):
        return self.name

    class Meta:
        order_with_respect_to = "song"
        constraints = [
            models.UniqueConstraint(
                fields=["slug", "song"], name="unique_slug_per_song"
            )
        ]


fsrf_scheduler = Scheduler()
