from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from . import views

urlpatterns = format_suffix_patterns(
    [
        path("repertoire/", views.PublicRepertoireList.as_view()),
        path("<username>/repertoire/", views.RepertoireList.as_view()),
        path("<username>/repertoire/<slug>/", views.RepertoireDetail.as_view()),
        path("<username>/repertoire/<slug>/song/", views.SongList.as_view()),
        path(
            "<username>/repertoire/<rep_slug>/song/<slug>/",
            views.SongDetail.as_view(),
        ),
        path(
            "<username>/repertoire/<rep_slug>/song/<slug>/asset/",
            views.AssetList.as_view(),
        ),
        path(
            "<username>/repertoire/<rep_slug>/song/<song_slug>/asset/<slug>/",
            views.AssetDetail.as_view(),
        ),
        path(
            "<username>/repertoire/<rep_slug>/song/<slug>/reorder/",
            views.reorderAssets,
        ),
        path(
            "<username>/repertoire/<rep_slug>/song/<slug>/rate/",
            views.updateCard,
        ),
        path(
            "<username>/repertoire/<slug>/copy/",
            views.copyRepertoire,
        ),
        path(
            "<username>/repertoire/<rep_slug>/song/<slug>/copy/",
            views.copySong,
        ),
    ]
)
