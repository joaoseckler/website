from typing import Generic, Protocol, TypeVar

from django.db import models
from rest_framework import permissions


class HasOwner(Protocol):
    owner: models.Model


T = TypeVar("T", bound=HasOwner)


def HasOwnedFieldOrReadonly(owned_field=None):
    class InnerIsOwnerOrReadOnly(permissions.BasePermission, Generic[T]):
        """
        Custom permission to only allow owners of an object to edit it.
        """

        def has_object_permission(self, request, view, obj: T):
            if request.method in permissions.SAFE_METHODS:
                return True

            if owned_field is None:
                return obj.owner == request.user

            queue = owned_field.split("__")
            o = obj
            for fieldname in queue:
                o = getattr(o, fieldname)

            return o.owner == request.user

    return InnerIsOwnerOrReadOnly


IsOwnerOrReadOnly = HasOwnedFieldOrReadonly()
