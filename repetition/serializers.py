from django.contrib.auth import get_user_model
from django.utils.text import slugify
from rest_framework import serializers

from .models import Asset, Repertoire, Song

User = get_user_model()


class RepertoireSerializer(serializers.ModelSerializer):
    owner_username = serializers.SerializerMethodField()

    def get_owner_username(self, obj):
        return obj.owner.username

    class Meta:
        model = Repertoire
        fields = ["id", "slug", "name", "public", "owner_username", "public"]
        read_only_fields = ["slug", "owner", "owner_username"]

    def validate(self, data):
        owner = self.context["request"].user
        name = data.get("name")

        if (
            owner
            and name
            and Repertoire.objects.filter(owner=owner, slug=slugify(name)).first()
        ):
            raise serializers.ValidationError(f"Name {name} already exists!")

        return data


class SongSerializer(serializers.ModelSerializer):
    prev_slug = serializers.ReadOnlyField()
    next_slug = serializers.ReadOnlyField()
    prev_public_slug = serializers.ReadOnlyField()
    next_public_slug = serializers.ReadOnlyField()
    prev_is_owner = serializers.SerializerMethodField()
    next_is_owner = serializers.SerializerMethodField()
    owner_username = serializers.SerializerMethodField()

    def get_prev_is_owner(self, obj):
        user = self.context.get("request").user
        try:
            return obj.get_next_in_order().repertoire.owner == user
        except obj.__class__.DoesNotExist:
            return False

    def get_next_is_owner(self, obj):
        user = self.context.get("request").user
        try:
            return obj.get_previous_in_order().repertoire.owner == user
        except obj.__class__.DoesNotExist:
            return False

    def get_owner_username(self, obj):
        return obj.repertoire.owner.username

    class Meta:
        model = Song
        fields = "__all__"
        read_only_fields = ["repertoire", "slug"]


class AssetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Asset
        fields = "__all__"
        read_only_fields = ["slug", "song"]
