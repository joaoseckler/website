import operator
import re
from functools import reduce

from django.db import models
from rest_framework import filters
from unidecode import unidecode

# stackoverflow.com/questions/63377291
# (real nice)


class PrioritizedSearchFilter(filters.SearchFilter):
    def filter_queryset(self, request, queryset, view):
        search_fields = self.get_search_fields(view, request)
        search_terms = self.get_search_terms(request)

        if not search_fields or not search_terms:
            return queryset

        orm_lookups = [
            self.construct_search(str(search_field)) for search_field in search_fields
        ]

        base = queryset
        querysets = []

        for search_term in search_terms:
            queries = [
                models.Q(**{orm_lookup: search_term}) for orm_lookup in orm_lookups
            ]

            when_conditions = [
                models.When(queries[i], then=models.Value(len(queries) - i - 1))
                for i in range(len(queries))
            ]

            querysets.append(
                queryset.filter(reduce(operator.or_, queries)).alias(
                    priority=models.Case(
                        *when_conditions,
                        output_field=models.IntegerField(),
                        default=models.Value(-1),  # Lowest priority possible
                    )
                )
            )

        queryset = reduce(operator.and_, querysets).order_by("-priority")

        if self.must_call_distinct(queryset, search_fields):
            # inspired by django.contrib.admin
            # this is more accurate than .distinct form M2M relationship
            # also is cross-database
            queryset = queryset.filter(pk=models.OuterRef("pk"))
            queryset = base.filter(models.Exists(queryset))
        return queryset


class NormalizedSearchFilter(filters.SearchFilter):
    def normalize(self, string):
        return re.sub(r"[()\[\]\{\}\|]", "", unidecode(string))

    def get_search_terms(self, request):
        params_list = super().get_search_terms(request)
        return list(map(self.normalize, params_list))
