from pathlib import Path
from unittest import TestCase

from laialaia import __file__ as laialaia_file
from laialaia.music.analysis import KeyDetecter
from laialaia.music.common import Pitch
from laialaia.music.harmony import Chord, Key
from laialaia.music.hmm import HiddenMarkovModel


class TestDetection(TestCase):
    txt_dir = Path(laialaia_file).parent / "raw" / "txt"

    def test_id_representation(self):
        for cls in Chord, Key, Pitch:
            for i in range(cls.max_id()):
                self.assertEqual(i, cls.from_id(i).id(), cls.__name__)

    def test_hmm(self):
        n_states = 2
        init = [0.5, 0.5]
        max_obs = 6
        trans = [[0.5, 0.5], [0.5, 0.5]]
        emit = [
            [1 / 3, 1 / 3, 1 / 3, 0, 0, 0],
            [0, 0, 0, 1 / 3, 1 / 3, 1 / 3],
        ]

        model = HiddenMarkovModel(n_states, max_obs, init, trans, emit)

        obs = [0, 1, 2, 3, 4, 5]
        self.assertEqual(list(model.predict(obs)), [0, 0, 0, 1, 1, 1])

    def test_possible_keys_by_chords(self):
        kd = KeyDetecter([Chord(c) for c in ["C7", "F", "E7", "A"]])
        for got, expected in zip(
            kd.possible_keys_by_chord,
            [
                {Key("Dm"), Key("F")},
                {Key("F"), Key("C"), Key("Dm"), Key("Am")},
                {Key("F#m"), Key("A")},
                {Key("A"), Key("E"), Key("F#m"), Key("C#m")},
            ],
        ):
            self.assertLess(expected, got)
