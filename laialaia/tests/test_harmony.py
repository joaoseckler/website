import pickle
from pathlib import Path
from unittest import TestCase

from laialaia.music.analysis import (
    SingleKeyAnalysis,
)
from laialaia.music.common import Interval, Pitch
from laialaia.music.harmony import (
    Chord,
    ChordFunctions,
    Function,
    Key,
)
from laialaia.parse.chords import transpose


class TestHarmony(TestCase):
    def test_parsing_a_lot(self):
        pickle_file = Path(__file__).parent / "chords.pkl"

        with open(pickle_file, "rb") as f:
            expected = pickle.load(f)

        for chord, ret in expected.items():
            if ret is None:
                self.assertIs(transpose(chord), None)
            else:
                self.assertEqual(transpose(chord)[1], ret)

    def test_parsing_manual(self):
        for chord, ret in {
            "A": (("A", None), "%root%"),
            "Cm": (("C", None), "%root%m"),
            "Hb": (("Hb", None), "%root%"),
            "B7": (("B", None), "%root%7"),
            "Amaj7": (("A", None), "%root%maj7"),
            "Ammaj7": (("A", None), "%root%mmaj7"),
            "G7M": (("G", None), "%root%7M"),
            "Dmaj7(9)": (("D", None), "%root%maj7(9)"),
            "Eb6(9)": (("Eb", None), "%root%6(9)"),
            "Daug5": (("D", None), "%root%aug5"),
            "F#m7(b5)": (("F#", None), "%root%m7(b5)"),
            "C#(#5)": (("C#", None), "%root%(#5)"),
            "Daug5/Db": (("D", "Db"), "%root%aug5/%slash%"),
            "F#m7(b5)/E": (("F#", "E"), "%root%m7(b5)/%slash%"),
            "C#(#5)/A": (("C#", "A"), "%root%(#5)/%slash%"),
            "A/5+": (("A", None), "%root%/5+"),
            "Em7/5-": (("E", None), "%root%m7/5-"),
            "F7/9/13/Eb": (("F", "Eb"), "%root%7/9/13/%slash%"),
            "G(4/7)": (("G", None), "%root%(4/7)"),
            "D°7M": (("D", None), "%root%°7M"),
            "Dº7M": (("D", None), "%root%º7M"),
        }.items():
            self.assertEqual(transpose(chord), ret)

    def test_interval_inversion(self):
        for note in range(0, 7):
            for acc in range(-2, 3):
                intr = Interval(note, acc)
                inv = intr.inverse()
                self.assertEqual(
                    (intr.n + inv.n) % 12,
                    0,
                    msg=(
                        f"{intr} ({intr.n} st) and it's inverse "
                        f"{inv} ({inv.n} st) don't add up to an octave"
                    ),
                )

    def test_pitch2n(self):
        for pitch, n in {
            "C": 0,
            "C#": 1,
            "Db": 1,
            "D": 2,
            "D#": 3,
            "Eb": 3,
            "E": 4,
            "E#": 5,
            "Fb": 4,
            "F": 5,
            "F#": 6,
            "Gb": 6,
            "G": 7,
            "G#": 8,
            "Ab": 8,
            "A": 9,
            "A#": 10,
            "Bb": 10,
            "B": 11,
            "B#": 0,
            "H": 11,
        }.items():
            self.assertEqual(Pitch(pitch).n, n, msg=pitch)

    def test_key(self):
        for k, (n, m) in {
            "A": (9, True),
            "Am": (9, False),
            "Bm": (11, False),
            "Hm": (11, False),
            "Bbm": (10, False),
            "Cbm": (11, False),
        }.items():
            key = Key(k)
            self.assertEqual(n, key.pitch.n, msg=k)
            self.assertEqual(m, key.major, msg=k)

    def test_degree(self):
        self.assertEqual(str(Key("Bbm").roman_numeral(Pitch("C"))), "ii")
        self.assertEqual(str(Key("Bbm").roman_numeral(Pitch("E"))), "IV#")
        self.assertEqual(str(Key("Cb").roman_numeral(Pitch("F"))), "IV#")
        self.assertEqual(str(Key("Cb").roman_numeral(Pitch("Gb"))), "V")
        self.assertEqual(str(Key("Cb").roman_numeral(Pitch("Db"))), "ii")
        self.assertEqual(str(Key("Cb").roman_numeral(Pitch("Bb"))), "vii")

    def test_roman_numeral_from_semitones(self):
        self.assertEqual(str(Key("Bbm").roman_numeral_from_semitones(2)), "III#")
        self.assertEqual(str(Key("Bbm").roman_numeral_from_semitones(6)), "VI")
        self.assertEqual(str(Key("Cb").roman_numeral_from_semitones(6)), "V")
        self.assertEqual(str(Key("Cb").roman_numeral_from_semitones(7)), "V#")
        self.assertEqual(str(Key("Cb").roman_numeral_from_semitones(2)), "II#")
        self.assertEqual(str(Key("Cb").roman_numeral_from_semitones(11)), "I")

    @staticmethod
    def is_hf(key: Key, chord: Chord):
        return ChordFunctions(key, [chord])[0] & Function.HF

    def test_find_function(self):
        self.assertTrue(self.is_hf(Key("Bbm"), Chord("Cm7(b5)")))
        self.assertTrue(self.is_hf(Key("Bbm"), Chord("Db6")))
        self.assertTrue(self.is_hf(Key("Bbm"), Chord("F7")))
        self.assertTrue(self.is_hf(Key("Bbm"), Chord("Ab7(13b)")))
        self.assertFalse(self.is_hf(Key("Bbm"), Chord("Am7")))
        self.assertFalse(self.is_hf(Key("Bbm"), Chord("Dm")))
        self.assertFalse(self.is_hf(Key("Bbm"), Chord("G7")))
        self.assertFalse(self.is_hf(Key("Bbm"), Chord("Cm")))

        self.assertTrue(self.is_hf(Key("Cb"), Chord("Dbm")))
        self.assertTrue(self.is_hf(Key("Cb"), Chord("Ebm")))
        self.assertTrue(self.is_hf(Key("Cb"), Chord("Gb7")))
        self.assertTrue(self.is_hf(Key("Cb"), Chord("Abm")))
        self.assertFalse(self.is_hf(Key("Cb"), Chord("C")))
        self.assertFalse(self.is_hf(Key("Cb"), Chord("G")))
        self.assertFalse(self.is_hf(Key("Cb"), Chord("Fm")))
        self.assertFalse(self.is_hf(Key("Cb"), Chord("Gm")))

        self.assertTrue(self.is_hf(Key("E"), Chord("A")))
        self.assertTrue(self.is_hf(Key("E"), Chord("B7")))
        self.assertTrue(self.is_hf(Key("E"), Chord("C#m7")))
        self.assertTrue(self.is_hf(Key("E"), Chord("D#m7(b5)")))
        self.assertFalse(self.is_hf(Key("E"), Chord("D#m")))
        self.assertFalse(self.is_hf(Key("E"), Chord("Gm")))
        self.assertFalse(self.is_hf(Key("E"), Chord("F")))
        self.assertFalse(self.is_hf(Key("E"), Chord("C")))

        self.assertTrue(self.is_hf(Key("C"), Chord("Em")))
        self.assertFalse(self.is_hf(Key("C"), Chord("C7")))

        function = ChordFunctions(Key("C"), [Chord("Em")])[0]
        self.assertTrue(function & Function.HF, msg="Em on C should be HF")

        function = ChordFunctions(Key("C"), [Chord("C7")])[0]
        self.assertTrue(function is Function.U, msg="C7 on C should be U")

        function = ChordFunctions(Key("C"), [Chord("F"), Chord("Fm")])[1]
        self.assertTrue(function & Function.M4, msg="(F) Fm on C should be m4")

        function = ChordFunctions(Key("C"), [Chord("Dm"), Chord("Fm")])[1]
        self.assertTrue(function & Function.M4, msg="(Dm) Fm on C should be m4")

        function = ChordFunctions(Key("C"), [Chord("Am"), Chord("Fm")])[1]
        self.assertTrue(function & Function.M4, msg="(Am) Fm on C should be m4")

        function = ChordFunctions(Key("C"), [Chord("F#m7(b5)"), Chord("Fm")])[1]
        self.assertTrue(function & Function.M4, msg="(F#m7(b5)) Fm on C should be m4")

        function = ChordFunctions(Key("C"), [Chord("Gbm7(b5)"), Chord("Fm")])[1]
        self.assertTrue(function & Function.M4, msg="(Gbm7(b5)) Fm on C should be m4")

    def test_pitch_transposition(self):
        self.assertEqual(Pitch("C").transpose(Interval(4, 0)), Pitch("G"))
        self.assertEqual(Pitch("D").transpose(Interval(4, 0)), Pitch("A"))
        self.assertEqual(Pitch("A").transpose(Interval(2, 0)), Pitch("C#"))
        self.assertEqual(Pitch("A").transpose(Interval(2, -1)), Pitch("C"))
        self.assertEqual(Pitch("Ab").transpose(Interval(2, 0)), Pitch("C"))
        self.assertEqual(Pitch("Ab").transpose(Interval(2, 1)), Pitch("C#"))
        self.assertEqual(Pitch("Ab").transpose(Interval(2, -1)), Pitch("Cb"))
        self.assertEqual(Pitch("E").transpose(Interval(6, -1)), Pitch("D"))
        self.assertEqual(Pitch("B").transpose(Interval(5, 1)), Pitch("G##"))
        self.assertEqual(Pitch("B").transpose(Interval(5, 0)), Pitch("G#"))
        self.assertEqual(Pitch("B").transpose(Interval(5, -1)), Pitch("G"))
        self.assertEqual(Pitch("F#").transpose(Interval(6, 0)), Pitch("E#"))

        with self.assertRaises(AssertionError):
            Interval(7, 0)

    def test_interval_transposition(self):
        self.assertEqual(
            Interval("P1").transpose(Interval("P5")),
            Interval("P5"),
            msg="P1 plus P5",
        )
        self.assertEqual(
            Interval("P5").transpose(Interval("P1")),
            Interval("P5"),
            msg="P5 plus P1",
        )
        self.assertEqual(
            Interval("m3").transpose(Interval("M3")),
            Interval("P5"),
            msg="m3 plus M3",
        )
        self.assertEqual(
            Interval("M3").transpose(Interval("m3")),
            Interval("P5"),
            msg="M3 plus m3",
        )
        self.assertEqual(
            Interval("M3").transpose(Interval("M3")),
            Interval("a5"),
            msg="M3 plus M3",
        )
        self.assertEqual(
            Interval("M6").transpose(Interval("m2")),
            Interval("m7"),
            msg="M6 plus m2",
        )
        self.assertEqual(
            Interval("M6").transpose(Interval("m2")),
            Interval("m7"),
            msg="M6 plus m2",
        )
        self.assertEqual(
            Interval("P4").transpose(Interval("M2")),
            Interval("P5"),
            msg="P4 plus M2",
        )
        self.assertEqual(
            Interval("P4").transpose(Interval("m2")),
            Interval("d5"),
            msg="P4 plus m2",
        )
        self.assertEqual(
            Interval("M3").transpose(Interval("P4")),
            Interval("M6"),
            msg="M3 plus P4",
        )
        self.assertEqual(
            Interval("m3").transpose(Interval("P4")),
            Interval("m6"),
            msg="m3 plus P4",
        )
        self.assertEqual(
            Interval("m7").transpose(Interval("m7")),
            Interval("m6"),
            msg="m7 plus m7",
        )
        self.assertEqual(
            Interval("M7").transpose(Interval("M7")),
            Interval("a6"),
            msg="M7 plus M7",
        )
        self.assertEqual(
            Interval("M3").transpose(Interval("M3")),
            Interval("a5"),
            msg="M3 plus M3",
        )
        self.assertEqual(
            Interval("d2").transpose(Interval("M3")),
            Interval("d4"),
            msg="d2 plus M3",
        )
        self.assertEqual(
            Interval("d3").transpose(Interval("d3")),
            Interval("ddd5"),
            msg="d2 plus M3",
        )

    def test_interval_str_repr(self):
        self.assertEqual(str(Interval(0, 0)), "P1")
        self.assertEqual(str(Interval(1, 0)), "M2")
        self.assertEqual(str(Interval(1, -1)), "m2")
        self.assertEqual(str(Interval(1, -2)), "d2")
        self.assertEqual(str(Interval(1, -3)), "dd2")
        self.assertEqual(str(Interval(1, 1)), "a2")
        self.assertEqual(str(Interval(1, 2)), "aa2")
        self.assertEqual(str(Interval(4, 0)), "P5")
        self.assertEqual(str(Interval(4, 1)), "a5")
        self.assertEqual(str(Interval(4, -1)), "d5")

    def test_interval_from_pitches(self):
        self.assertEqual(Interval.from_pitches(Pitch("C"), Pitch("C")), Interval(0, 0))
        self.assertEqual(Interval.from_pitches(Pitch("C"), Pitch("D")), Interval(1, 0))
        self.assertEqual(Interval.from_pitches(Pitch("C"), Pitch("E")), Interval(2, 0))
        self.assertEqual(Interval.from_pitches(Pitch("C"), Pitch("F")), Interval(3, 0))
        self.assertEqual(Interval.from_pitches(Pitch("C"), Pitch("G")), Interval(4, 0))
        self.assertEqual(Interval.from_pitches(Pitch("C"), Pitch("G#")), Interval(4, 1))
        self.assertEqual(
            Interval.from_pitches(Pitch("C"), Pitch("Gb")), Interval(4, -1)
        )
        self.assertEqual(
            Interval.from_pitches(Pitch("C"), Pitch("Eb")), Interval(2, -1)
        )

    def test_analysis(self):
        self.assertEqual(
            SingleKeyAnalysis(["C", "F", "G7", "C"], "C").generic,
            ["I", "IV", "V7", "I"],
        )
        self.assertEqual(
            SingleKeyAnalysis(["Bb", "Eb", "F7", "Bb"], "Bb").generic,
            ["I", "IV", "V7", "I"],
        )
        self.assertEqual(
            SingleKeyAnalysis(["Bbm", "Ebm", "F7", "Bbm"], "Bbm").generic,
            ["i", "iv", "V7", "i"],
        )
        self.assertEqual(
            SingleKeyAnalysis(["C", "F#º", "G7", "C"], "C").generic,
            ["I", "iv#º", "V7", "I"],
        )
        self.assertEqual(
            SingleKeyAnalysis(["C", "Dm7", "G7", "C"], "C").generic,
            ["I", "ii7", "V7", "I"],
        )
        self.assertEqual(
            SingleKeyAnalysis(["C", "D7", "G7", "C"], "C").generic,
            ["I", "V7/V", "V7", "I"],
        )
        self.assertEqual(
            SingleKeyAnalysis(["C", "F", "A7", "D7", "G7", "C"], "C").generic,
            ["I", "IV", "V7/ii", "V7/V", "V7", "I"],
        )
        self.assertEqual(
            SingleKeyAnalysis(["C", "F", "A7", "D7(9)", "G7", "C"], "C").generic,
            ["I", "IV", "V7/ii", "V7(9)/V", "V7", "I"],
        )
        self.assertEqual(
            SingleKeyAnalysis(["C", "F/C", "A7/E", "D7(9)/F#", "G7", "C"], "C").generic,
            ["I", "IV/5", "V7/5/ii", "V7(9)/3/V", "V7", "I"],
        )
        self.assertEqual(
            SingleKeyAnalysis(["G", "Am7", "B7", "Em", "D7", "C", "G"], "G").generic,
            ["I", "ii7", "V7/vi", "vi", "V7", "IV", "I"],
        )
        self.assertEqual(
            SingleKeyAnalysis(
                ["G/B", "Am7/E", "B7/F#", "Em/G", "D7/C", "C/G", "G/D"], "G"
            ).generic,
            ["I/3", "ii7/5", "V7/5/vi", "vi/3", "V7/7", "IV/5", "I/5"],
        )
        self.assertEqual(
            SingleKeyAnalysis(
                [
                    "G/B",
                    "Am7/E",
                    "F#m7(b5)",
                    "B7/F#",
                    "Em/G",
                    "D7/C",
                    "C/G",
                    "G/D",
                ],
                "G",
            ).generic,
            [
                "I/3",
                "ii7/5",
                "ii7(b5)/vi",
                "V7/5/vi",
                "vi/3",
                "V7/7",
                "IV/5",
                "I/5",
            ],
        )
        self.assertEqual(
            SingleKeyAnalysis(
                ["Bb", "Em7(b5)", "A7(9-)", "Dm7", "G7(9)", "Cm7", "F7(4/9)"],
                "Bb",
            ).generic,
            [
                "I",
                "ii7(b5)/iii",
                "V7(9-)/iii",
                "ii7/ii",
                "V7(9)/ii",
                "ii7",
                "V7(4/9)",
            ],
        )
        self.assertEqual(
            SingleKeyAnalysis(["G", "A7", "Am7", "D7", "G"], "G").generic,
            ["I", "V7/V", "ii7", "V7", "I"],
        )
        self.assertEqual(
            SingleKeyAnalysis(["C", "Gb7", "F", "C"], "C").generic,
            ["I", "IIb7/IV", "IV", "I"],
        )
        self.assertEqual(
            SingleKeyAnalysis(["D7", "Db7", "C7", "F"], "C").generic,
            ["IIb7/I#", "IIb7/I", "V7/IV", "IV"],
        )
        self.assertEqual(
            SingleKeyAnalysis(  # From "https://jseckler.xyz/cifras/ta-vendo-aquela-lua"
                [
                    "G7M",
                    "C#7(11#)",
                    "C7M",
                    "D7(4/9)",
                    "G7M",
                ],
                "G",
            ).generic,
            ["I7M", "IIb7(11#)/IV", "IV7M", "V7(4/9)", "I7M"],
        )
