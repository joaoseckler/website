import html
import re
from dataclasses import astuple, dataclass
from uuid import uuid4

from bs4 import BeautifulSoup
from laialaia.parse.chords import transpose
from laialaia.parse.common import CssClass, key_pattern, lysincopation
from laialaia.svgly import svgly
from laialaia.svgly.transposition import parse_key


@dataclass
class Staff:
    key = r"c \major"
    time_signature = "2/4"
    clef = "treble"
    size = 20


@dataclass
class Options:
    hideall = False
    noanchor = False
    hideall = False
    nobreak = False
    use_sinc = False

    def __iter__(self):
        return iter(astuple(self))

    def __getitem__(self, keys):
        return iter(getattr(self, k) for k in keys)


class SheetMusicProcessor:
    patterns = {
        "clef": re.compile(r"\\clef (\w+)"),
        "hideall": re.compile(r"\\hideall"),
        "id": re.compile(r'\s+id="(\S*)"'),
        "key": re.compile(r"\\key (\w \\(major|minor))"),
        "noanchor": re.compile(r"\\noanchor"),
        "nobreak": re.compile(r"\\nobreak"),
        "size": re.compile(r"\\staffsize ([\d\.]+)"),
        "time_signature": re.compile(r"\\time (\d{1,2}/\d{1,2})"),
        "sinc": re.compile(r"\\sinc"),
        "hshortest": re.compile(r"\\hshortest (\d+/\d+)"),
        "hinc": re.compile(r"\\hinc ([\d.-]+)"),
        "hincrev": re.compile(r"\\hincrev"),
    }

    def __init__(self, transposition_dict, dev):
        self.transposition_dict = transposition_dict
        self.staff = Staff()
        self.options = Options()
        self.used_ids = set()
        self.positions = []
        self.ledger_lines = 0
        self.development = dev

    @staticmethod
    def key2ly(key):
        ret = key[0].lower()

        if not key:
            return r"c \major"

        if not re.match(key_pattern, key):
            raise ValueError(f"Invalid key: {key}. Please match {key_pattern.pattern}")

        if len(key) > 1:
            if key[1] == "b":
                if key[0] == "e":
                    ret += "s"
                else:
                    ret += "es"
            elif key[1] == "#":
                ret += "is"

            if key[-1] == "m":
                return rf"{ret} \minor"

        return rf"{ret} \major"

    @staticmethod
    def ly2key(ly):
        note_name, accidental, quality = parse_key(ly)

        if accidental:
            accidental = "#" if accidental == "is" else "b"
        else:
            accidental = ""

        if quality:
            quality = "m" if quality == "\\minor" else ""

        return note_name.upper() + accidental + quality

    def save_and_replace(self, where, what, fn=lambda mo: mo.group(1)):
        def _(mo):
            setattr(where, what, fn(mo))
            return ""

        return _

    def strip_options(self, sm):
        for obj_key_fn in (
            (self.options, "hideall", lambda mo: True),
            (self.options, "noanchor", lambda mo: True),
            (self.options, "nobreak", lambda mo: True),
            (self.staff, "time_signature"),
            (self.staff, "key"),
            (self.staff, "clef"),
            (self.staff, "size", lambda mo: int(mo.group(1))),
        ):
            sm = re.sub(
                self.patterns[obj_key_fn[1]],
                self.save_and_replace(*obj_key_fn),
                sm,
                1,
            )

        return sm

    def replace_options(self, sm):
        for pat, repl in (
            (
                self.patterns["hincrev"],
                r"\\newSpacingSection \\revert Score.SpacingSpanner.spacing-increment",
            ),
            (
                self.patterns["hinc"],
                (
                    r"\\newSpacingSection \\override "
                    + r"Score.SpacingSpanner.spacing-increment = #\g<1>"
                ),
            ),
            (
                self.patterns["hshortest"],
                (
                    r"\\newSpacingSection \\override "
                    + "Score.SpacingSpanner.base-shortest-duration = "
                    + r"#(ly:make-moment \g<1>)"
                ),
            ),
        ):
            sm = re.sub(pat, repl, sm)

        return sm

    def detect_options(self, sm):
        if re.search(self.patterns["sinc"], sm):
            self.options.use_sinc = True

    def store_used_ids(self, svg):
        for mo in re.finditer(self.patterns["id"], svg):
            self.used_ids.add(mo.group(1))

    def div_join(self, svgs):
        acc = ""

        for key, (svg, _) in svgs.items():
            uuid = str(uuid4())[:8]
            attrs = f'data-key-signature="{self.ly2key(key)}" data-variant-id="{uuid}"'

            if key == self.staff.key:
                attrs += ' data-original="1"'
            else:
                attrs += ' style="display:none;"'
            acc += f"<div {attrs}>"
            acc += svg
            acc += "</div>"

        return acc

    def process(self, sm, key):
        self.staff.key = self.key2ly(key) if key else None

        sm = self.replace_options(sm)
        sm = self.strip_options(sm)
        self.detect_options(sm)
        svgs = svgly(
            sm,
            one_svg_per_measure=True,
            time_signature=self.staff.time_signature,
            clef=self.staff.clef,
            key=self.staff.key,
            staff_size=self.staff.size,
            id_list=" ".join(self.used_ids),
            html_attributes={"class": "sheet-music-svg"},
            hideall=self.options.hideall,
            verbose=True,
            ly_block=lysincopation if self.options.use_sinc else None,
            measure_positions=True,
            all_keys=not self.development,
        )

        self.positions = {
            self.ly2key(key): data[1]["positions"] for key, data in svgs.items()
        }

        map(lambda svg_info: self.store_used_ids(svg_info[0]), svgs.values())

        return self.div_join(svgs)

    def chordlines2chord(self, chordlines):
        chords = list(
            map(
                lambda x: x.replace(".", " "),
                " <br/> ".join(chordlines).split(),
            )
        )

        ret = []
        brk = []

        i = 0
        while i < len(chords):
            chord = chords[i]
            if chord == ",":
                ret[-1].append(chords[i + 1])
                i += 1
            elif chord == "<br/>":
                brk.append(i)
            elif chord == "(":
                try:
                    ret.append([chord + "\u00a0", chords[i + 1]])
                    i += 1
                except IndexError:
                    ret.append([chord])
            elif chord == ")":
                ret[-1].append("\u00a0" + ")")
            else:
                ret.append([chord])

            i += 1

        return ret, brk

    def set_transposition_info(self, span, chord):
        span["data-root"], span["data-fstring"] = transpose(chord)

    def anchor(self, svgs, chordlines):
        soup = BeautifulSoup(f"{svgs}", "lxml")

        for svg_div in soup.find_all("div", attrs={"data-key-signature": True}):
            key = svg_div["data-key-signature"]
            spans = self.anchor_key(
                svg_div,
                chordlines,
                self.positions[key],
                new_tag=soup.new_tag,
            )
            svg_div.clear()
            svg_div.extend(spans)

        return str(soup.contents[0].contents[0])

    def anchor_key(self, soup, chordlines, positions, new_tag):
        chords, brk = self.chordlines2chord(list(map(html.escape, chordlines)))

        is_original = bool(soup.get("data-original", 0))
        uuid = soup["data-variant-id"]
        ret = []
        all_svgs = soup.find_all("svg")

        chord_count = 0
        for i, svg in enumerate(all_svgs):
            span = new_tag("span", attrs={"class": CssClass.POSITIONED.value})

            if len(chords) > i:
                start = positions[i]
                end = positions[i + 1]
                n = len(
                    list(
                        filter(
                            lambda x: x not in ("(\u00a0", "\u00a0)"),
                            chords[i],
                        )
                    )
                )

                position_j = 0
                last_pos = ""
                last_len = 0

                for chord in chords[i]:
                    self.transposition_dict.update(chord)

                    pos = ((end - start) / n) * position_j
                    if i == 0:
                        pos += positions[i]

                    # Match aprox. firt note, not barline
                    pos += 7

                    chordspan = new_tag("span", attrs={"class": CssClass.CHORD.value})
                    if is_original:
                        chordspan["data-variant-id"] = f"{uuid}-{chord_count}"
                    else:
                        chordspan["data-variant-of"] = f"{uuid}-{chord_count}"
                    chord_count += 1

                    if chord == "(\u00a0":
                        chordspan["style"] = f"left:calc({pos}px - 1rem);"
                        position_j -= 1
                    elif chord == "\u00a0)":
                        left = f"calc({last_pos}px + {last_len * .7:.4f}rem)"
                        chordspan["style"] = f"left:{left};"
                        position_j -= 1
                    else:
                        chordspan["style"] = f"left:{pos}px;"

                    chordspan.string = chord
                    span.append(chordspan)

                    last_pos = pos
                    last_len = len(chord)
                    position_j += 1

                if i + 1 == len(all_svgs) and len(chords) > i + 1:
                    pos = positions[i + 1] - positions[i]
                    chordspan = new_tag(
                        "span", attrs={"class": CssClass.CHORDGROUP.value}
                    )

                    for i, chord in enumerate(chords[i + 1 :]):
                        self.transposition_dict.update(chord[0])
                        new = new_tag("span", attrs={"class": CssClass.CHORD.value})
                        if is_original:
                            new["data-variant-id"] = f"{uuid}-{chord_count}"
                        else:
                            new["data-variant-of"] = f"{uuid}-{chord_count}"
                        chord_count += 1
                        new.string = chord[0]
                        chordspan.append(new)
                        if i != len(chords[i + 1 :]) - 1:
                            chordspan.append("\u00a0\u00a0")

                    chordspan["style"] = f"left:{pos}px;"
                    span.append(chordspan)

            span.append(svg)

            if i in brk and not self.options.nobreak:
                ret.append(new_tag("br"))
            ret.append(span)

        return ret
