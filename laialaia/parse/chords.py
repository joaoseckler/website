from dataclasses import dataclass
from enum import Enum
from itertools import chain
from operator import attrgetter
from pathlib import Path

from joblib import Memory
from lark import Lark, Transformer, Tree
from lark.exceptions import UnexpectedInput
from lark.visitors import Discard

from laialaia.music.common import ChordType, Interval

memory = Memory(".cachedir", verbose=0)
BASE_DIR = Path(__file__).parent


@dataclass
class QualData:
    string: str
    intervals: list[Interval] | Interval | None
    replaces: list[int] | int | None

    def __post_init__(self):
        if self.intervals is not None and not isinstance(self.intervals, list):
            self.intervals = [self.intervals]
        if self.replaces is not None and not isinstance(self.replaces, list):
            self.replaces = [self.replaces]

    def __str__(self):
        return self.string

    def __eq__(self, other):
        return self.string == other.string


class Qual(Enum):
    MINOR = QualData("minor", Interval(2, -1), 2)
    DIM = QualData("diminished", [Interval(2, -1), Interval(4, -1)], [2, 4])
    AUG = QualData("augmented", Interval(4, 1), 4)
    FOURTH = QualData("fourth", Interval(3, 0), 3)
    DIM5 = QualData("diminished_fifth", Interval(4, -1), 4)
    OMIT3 = QualData("omit_third", None, 2)
    MINOR6 = QualData("minor_sixth", Interval(5, -1), None)
    SIXTH = QualData("sixth", Interval(5, 0), None)
    MAJOR7 = QualData("major_seventh", Interval(6, 0), None)
    MINOR7 = QualData("seventh", Interval(6, -1), None)
    MINOR9 = QualData("minor_ninth", Interval(1, -1), None)
    AUG9 = QualData("augmented_ninth", Interval(1, 1), None)
    NINTH = QualData("ninth", Interval(1, 0), None)
    MINOR13 = QualData("minor_thirteenth", Interval(5, -1), None)
    MAJ13 = QualData("thirteenth", Interval(5, 0), None)
    AUG11 = QualData("augmented_eleventh", Interval(3, 1), None)
    PERF11 = QualData("eleventh", Interval(3, 0), None)

    def __str__(self):
        return str(self.value)

    @classmethod
    def from_string(cls, string):
        return cls(QualData(string, None, None))


terminals = {
    "triad_qualities": {
        "m": Qual.MINOR,
        "º": Qual.DIM,
        "°": Qual.DIM,
        "aug": Qual.AUG,
        "+": Qual.AUG,
    },
    "extensions": {
        "4": Qual.FOURTH,
        "aug5": Qual.AUG,
        "5aug": Qual.AUG,
        "5b": Qual.DIM5,
        "b5": Qual.DIM5,
        "5-": Qual.DIM5,
        "5+": Qual.AUG,
        "#5": Qual.AUG,
        "5#": Qual.AUG,
        "5": Qual.OMIT3,
        "b6": Qual.MINOR6,
        "6b": Qual.MINOR6,
        "6-": Qual.MINOR6,
        "6": Qual.SIXTH,
        "maj7": Qual.MAJOR7,
        "7M": Qual.MAJOR7,
        "7+": Qual.MAJOR7,
        "7": Qual.MINOR7,
        "b9": Qual.MINOR9,
        "9b": Qual.MINOR9,
        "9-": Qual.MINOR9,
        "9+": Qual.AUG9,
        "#9": Qual.AUG9,
        "9#": Qual.AUG9,
        "9": Qual.NINTH,
        "b13": Qual.MINOR13,
        "13b": Qual.MINOR13,
        "13-": Qual.MINOR13,
        "13": Qual.MAJ13,
        "#11": Qual.AUG11,
        "11#": Qual.AUG11,
        "11+": Qual.AUG11,
        "11": Qual.PERF11,
    },
}


def dict2rule(d):
    return "\n    | ".join([f'"{pat}" -> {name}' for pat, name in d.items()])


with open(BASE_DIR / "chords.lark", encoding="utf-8") as f:
    grammar = f.read().format(**{key: dict2rule(val) for key, val in terminals.items()})


class ChordTypeInterpreter(Transformer):
    """Detects the type of a chord from it's parse tree"""

    def extensions(self, e):
        return e

    def chord(self, n):
        typ = ChordType.type_from_combinations(
            map(
                attrgetter("data"),
                filter(
                    None,
                    chain.from_iterable(
                        map(lambda x: x if isinstance(x, list) else (x,), n)
                    ),
                ),
            )
        )

        return typ


@dataclass
class Parts:
    root = None
    accidental = None
    slash = None
    qualities = None

    def __repr__(self):
        return f"Parts({self.root}, {self.accidental}, {self.slash}, {self.qualities})"

    def __post_init__(self):
        self.qualities = []


class ChordDecomposer(Transformer):
    def __init__(self, *args, **kwargs):
        self.parts = Parts()
        super().__init__(*args, **kwargs)

    def slash(self, sl):
        self.parts.slash = sl[0].data
        return Discard

    def pitch(self, p):
        return Tree(
            p[0].data.upper()
            + ((p[1].data == "flat" and "b") or "#" if len(p) > 1 else ""),
            [],
        )

    def extensions(self, ext):
        for e in ext:
            if e:
                self.parts.qualities.append(e.data)

        return Discard

    def chord(self, ch):
        qualities = []

        for i, node in enumerate(ch):
            if i == 0:
                self.parts.root = node.data
                self.parts.accidental = node.data[1:]
            else:
                qualities.append(node.data)

        self.parts.qualities = qualities + self.parts.qualities

        return self.parts


class ChordParser:
    grammar = Lark(grammar, start="chord")

    def __init__(self, chord):
        self.raw = chord
        self.tree = self.grammar.parse(chord)

    def type(self):
        return ChordTypeInterpreter().transform(self.tree)

    def decompose(self):
        return ChordDecomposer().transform(self.tree)

    def transpose(self, decomposed=None):
        dec = decomposed or self.decompose()

        ret = self.raw.replace(dec.root, "%root%", 1)
        if dec.slash:
            ret = ret.replace(dec.slash, "%slash%", 1)

        return ret

    def intervals(self, decomposed=None):
        qualities = (decomposed or self.decompose()).qualities
        intervals = {Interval(0, 0), Interval(2, 0), Interval(4, 0)}

        for qual in map(Qual.from_string, qualities):
            intervals = {
                i for i in intervals if i.number not in (qual.value.replaces or ())
            }
            for add in qual.value.intervals or ():
                intervals.add(add)

        return intervals


def is_chord(chord):
    try:
        ChordParser(chord)
        return True
    except UnexpectedInput:
        return False


def transpose(chord):
    try:
        dec = ChordParser(chord).decompose()
        return (dec.root, dec.slash), ChordParser(chord).transpose(dec)
    except UnexpectedInput:
        return None
