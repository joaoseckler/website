import markdown
from bs4 import BeautifulSoup

MD_CONF = {
    "extensions": [
        "meta",
        "fenced_code",
        "toc",
        "attr_list",
    ],
    "extension_configs": {
        "toc": {
            "permalink": "#",
            "permalink_title": "link permanente",
        }
    },
}


def add_target_blank(html):
    for link in html.find_all("a", attrs={"href": True}):
        href = link["href"]

        if href.startswith(("http://", "https://")):
            link["target"] = "_blank"
            link["rel"] = "noreferrer"


def parse(txt, filename):
    md = markdown.Markdown(**MD_CONF)
    html = BeautifulSoup(md.convert(txt), "lxml")

    title_element = html.find("h1")

    if not title_element:
        raise ValueError("Expected at least on h1 header")

    if title_element.a:
        title_element.a.extract()

    title = title_element.text
    title_element.extract()

    try:
        description = md.Meta["description"][0]
    except KeyError:
        description = "Sobre o site de cifras"

    slug = filename.stem

    add_target_blank(html)

    return str(html), description, title, slug
