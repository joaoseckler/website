from __future__ import annotations

import re
from enum import Flag, auto
from html import escape
from itertools import groupby, pairwise
from typing import Any, BinaryIO

from bs4 import BeautifulSoup

from laialaia.music.analysis import Analysis
from laialaia.music.harmony import Key
from laialaia.parse.chords import is_chord
from laialaia.parse.common import (
    CssClass,
    TranspositionDict,
    modulation_pattern,
    section_pattern,
    word_pattern,
)
from laialaia.parse.sheet_music import SheetMusicProcessor
from laialaia.svgly.htmlo import optimize


class LineType(Flag):
    CHORDS = auto()
    LYRICS = auto()
    SECTION = auto()
    ANCHORED = auto()  # Lyrics + chords
    VOLTA = auto()  # Lyrics + chords
    SHEET_MUSIC = auto()  # Lilypond
    ANCHORED_SHEET_MUSIC = auto()  # Lilypond + chords
    MODULATIONS = auto()
    EMPTY = auto()


class Line:
    def __init__(
        self,
        line: str,
        type: LineType | None = None,
    ):
        self.content: str = line
        self.type: LineType = type or self.parse_type()
        self.remove: bool = False
        self.anchored_chords: dict[int, list[tuple[int, str]]] | None = None
        self.anchored_sm_chords: list[str] | None = None
        self.volta_positions: dict[int, list[tuple[int, str]]] = {}
        self.volta_n: int = 0

        if self.type is not LineType.SHEET_MUSIC:
            self.content = self.content.replace("\n", "").replace(" ", "\u2002")

    def __str__(self):
        return f"{self.type} - {self.content[:40]}..."

    def classes(self):
        ret = ["line"]

        if self.type & LineType.EMPTY:
            ret.append("line-min-height")
        if self.type & LineType.CHORDS:
            ret.append("chordline")
        if self.type & LineType.SECTION:
            ret.append("sectionline")
        if self.type & LineType.ANCHORED:
            ret.append("anchored")
        if self.type & LineType.SHEET_MUSIC:
            ret.append("sheet-music")
        if self.type & LineType.ANCHORED_SHEET_MUSIC:
            ret.append("anchored-sheet-music")
        if self.type & LineType.VOLTA:
            ret.append(f"volta volta{self.volta_n}")

        return " ".join(ret)

    def to_volta(self):
        assert self.type & LineType.CHORDS, "Can only turn chords to volta chords"

        if not self.type & LineType.VOLTA:
            self.volta_n = 1
            self.type |= LineType.VOLTA

    def insert_volta(self, content: str):
        assert self.type & LineType.VOLTA, "Can only add volta to volta lines"
        self.volta_n += 1
        for match in re.finditer(r"\S+", content):
            self.volta_positions[match.start()] = self.volta_positions.get(
                match.start(), []
            ) + [(self.volta_n, match.group(0))]

    @staticmethod
    def is_section(word: str):
        return section_pattern.match(word)

    @staticmethod
    def is_modulation(word: str):
        return modulation_pattern.match(word)

    @staticmethod
    def is_chord_line_element(word: str):
        return is_chord(word) or word in ["(", ")", "...", ".", ","]

    def parse_type(self):
        if not self.content.strip():
            return LineType.EMPTY

        if self.content[0] == "%":
            return LineType.SHEET_MUSIC

        words = self.content.replace(",", " ").replace(".", " ").split()

        if not words:
            return LineType.EMPTY

        if all(self.is_chord_line_element(word) for word in words):
            return LineType.CHORDS

        if all(self.is_modulation(word) for word in words):
            return LineType.MODULATIONS

        if len(words) == 1 and self.is_section(words[0]):
            return LineType.SECTION

        return LineType.LYRICS

    def _anchor_to_lyrics(self, other: Line):
        self.type = LineType.ANCHORED

        if other.type & LineType.VOLTA:
            self.type |= LineType.VOLTA
            self.volta_n = other.volta_n

        self.anchored_chords = {}

        for match in re.finditer(r"\S+", other.content):
            start = match.start(0)
            self.anchored_chords[start] = [
                (0, match.group(0)),
                *other.volta_positions.get(start, []),
            ]

        self.content = self.content.ljust(len(other.content), "\u00a0")

    def _anchor_to_sm(self, chords: list[str]):
        self.anchored_sm_chords = chords
        self.type = LineType.ANCHORED_SHEET_MUSIC

    def anchor(self, other: Line | list[str]):
        if self.type is LineType.LYRICS and isinstance(other, Line):
            return self._anchor_to_lyrics(other)

        if self.type is LineType.SHEET_MUSIC and isinstance(other, list):
            return self._anchor_to_sm(other)

        raise AssertionError(
            f"Can only anchor to lyrics or sheet music. Got {self.type}"
        )

    def _chord_as_html(
        self,
        chord: str,
        classes: list[str] | None = None,
        attrs: dict[str, str] | None = None,
    ):
        if classes is None:
            classes = []

        classes = [CssClass.CHORD.value] + classes

        if attrs is None:
            attrs = {}

        attrs_html = " ".join(f'{key}="{val}"' for key, val in attrs.items())
        return f'<span {attrs_html} class="{' '.join(classes)}">{chord}</span>'

    def _anchored_as_html(self):
        html = ""
        prev_index = 0
        for (index, chords), (next_index, _) in pairwise(
            (*sorted(self.anchored_chords.items()), (None, None))
        ):
            chord_spans = ""
            for volta_n, chord in chords:
                classes = [f"{CssClass.VOLTA.value}{volta_n}"] if volta_n > 0 else None
                attrs = {}

                if next_index:
                    attrs["data-space"] = str(next_index - index if next_index else 0)
                    over_word = str(int(bool(re.match(r"\S", self.content[index]))))
                    attrs["data-over-word"] = over_word

                chord_spans += self._chord_as_html(escape(chord), classes, attrs)

            span = (
                f'<span class="{CssClass.POSITIONED.value}">'
                f"{escape(self.content[index])}{chord_spans}"
                f"</span>"
            )

            html += escape(self.content[prev_index:index])
            html += span
            prev_index = index + 1

        html += self.content[prev_index:]
        html = word_pattern.sub(r'<span style="white-space: nowrap">\g<0></span>', html)

        return html

    def _anchored_sm_as_html(self, key: Key, smp: SheetMusicProcessor):
        svgs = smp.process(self.content, str(key))

        html = smp.anchor(svgs, self.anchored_sm_chords)
        return html

    def _chords_as_html(self):
        def replace(match: re.Match):
            html = self._chord_as_html(match.group(0))
            for volta_n, chord in self.volta_positions.get(match.start(), []):
                html += self._chord_as_html(chord, [f"{CssClass.VOLTA.value}{volta_n}"])

            return html

        return re.sub(r"\S+", replace, self.content)

    def as_html(self, key: Key, smp: SheetMusicProcessor):
        if self.type is LineType.MODULATIONS:
            return ""

        if self.type & LineType.ANCHORED:
            content = self._anchored_as_html()
        elif self.type & LineType.ANCHORED_SHEET_MUSIC:
            content = self._anchored_sm_as_html(key, smp)
        elif self.type & LineType.CHORDS:
            content = self._chords_as_html()
        else:
            content = escape(self.content)

        return f'<div class="{self.classes()}">{content}</div>'


class Lines:
    def __init__(
        self,
        lines: list[str],
        key: str | None,
        transposition_dict: TranspositionDict,
        **model_args: Any,
    ):
        lines = self.remove_empty_start(lines)
        self.lines = [Line(line) for line in lines]
        self.transposition_dict = transposition_dict
        self.key = Key(key)
        self.key_changes: dict[int, str] = {}
        self.chords: list[str] = []
        self.analysis: Analysis | None = None

        self.gather_chords()
        self.analyse_harmony(**model_args)
        self.anchor()

    @property
    def analysed(self):
        return self.analysis is not None

    @staticmethod
    def remove_empty_start(lines: list[str]) -> list[str]:
        index = next(i for i, s in enumerate(lines) if s.strip())
        return lines[index:]

    @staticmethod
    def is_sm_or_chord(line: Line):
        return bool(line.type & (LineType.SHEET_MUSIC | LineType.CHORDS))

    def gather_chords(self):
        for prv, line in pairwise((None, *self.lines)):
            if line.type is LineType.CHORDS:
                chords = [
                    (match.start(), match.end(), match.group(0))
                    for match in re.finditer(r"\S+", line.content)
                    if is_chord(match.group(0))
                ]
                modulations = (
                    [
                        (match.start(), match.end(), match.group(0))
                        for match in re.finditer(r"\S+", prv.content)
                    ]
                    if prv and prv.type is LineType.MODULATIONS
                    else []
                )

                for start, end, chord in chords:
                    self.chords.append(chord)
                    self.transposition_dict.update(chord)

                    if modulations:
                        mod_start, mod_end, mod = modulations[0]
                        if not (start > mod_end or end < mod_start):
                            self.key_changes[len(self.chords) - 1] = mod

    volta_parens_patt = re.compile(r"^\s+(\()\s.+\s(\))$")
    parens_patt = re.compile(r"\s(\()\s.+\s(\))(\s|$)")

    def anchor_voltas(self, lines: list[Line]):
        if not lines:
            return
        target = lines.pop()
        target_parens = [
            (m.start(1), m.start(2)) for m in self.parens_patt.finditer(target.content)
        ]

        for line in reversed(lines):
            match = self.volta_parens_patt.match(line.content)
            if match and (match.start(1), match.start(2)) in target_parens:
                target.to_volta()
                target.insert_volta(line.content)
                line.remove = True
            else:
                break

    def anchor(self):
        for is_sm_or_chord, group in groupby(self.lines, key=self.is_sm_or_chord):
            if is_sm_or_chord:
                group = list(group)
                # Don't anchor voltas in chords before sheet music
                last_sm_index = next(
                    (
                        index
                        for index, line in reversed(list(enumerate(group)))
                        if line.type & LineType.SHEET_MUSIC
                    ),
                    -1,
                )
                self.anchor_voltas(group[last_sm_index + 1 :])

        self.lines = [line for line in self.lines if not line.remove]

        for nxt, prv in pairwise(reversed(self.lines)):
            if prv.type & LineType.CHORDS and nxt.type is LineType.LYRICS:
                nxt.anchor(prv)
                prv.remove = True

        self.lines = [line for line in self.lines if not line.remove]

        for is_sm_or_chord, group in groupby(self.lines, key=self.is_sm_or_chord):
            if is_sm_or_chord:
                group = list(group)
                sheet_music = "\n".join(
                    line.content.lstrip("%")
                    for line in group
                    if line.type == LineType.SHEET_MUSIC
                )

                if sheet_music:
                    chords = [
                        line.content for line in group if line.type == LineType.CHORDS
                    ]
                    anchored = next(
                        line for line in group if line.type == LineType.SHEET_MUSIC
                    )

                    anchored.content = sheet_music
                    anchored.anchor(chords)

                    for line in group:
                        line.remove = line is not anchored

        self.lines = [line for line in self.lines if not line.remove]

    @staticmethod
    def find_parent_by_class(origin, cl):
        el = origin
        while el := el.parent:
            if cl in el.get("class", []):
                return el

        return None

    def analyse_harmony(
        self,
        *,
        detect_modulations: bool = False,
        train_factor: float | None = None,
        model_parameters_file: str | None | BinaryIO = None,
    ):
        self.analysis = Analysis(
            self.chords,
            self.key,
            key_changes=None if detect_modulations else self.key_changes,
            train_factor=train_factor,
            model_parameters_file=model_parameters_file,
        )

        if self.key is None:
            self.key = self.analysis.reference_key

    def embed_analysis(self, html):
        doc = BeautifulSoup(html, "lxml")
        chord_els = [
            c
            for c in doc.find_all(class_="chord", attrs={"data-variant-of": False})
            if is_chord(c.text)
        ]

        assert len(chord_els) == len(self.chords)
        assert len(chord_els) == len(self.analysis.generic)

        for el, generic in zip(chord_els, self.analysis.generic):
            el["data-generic"] = generic
            if uuid := el.get("data-variant-id", False):
                for variant in doc.find_all(
                    class_="chord", attrs={"data-variant-of": uuid}
                ):
                    variant["data-generic"] = generic

        for i, key in self.analysis.key_changes.items():
            chord_els[i]["data-keychange"] = str(key)
            line_div = self.find_parent_by_class(chord_els[i], "line")
            if line_div:
                line_div["class"] = line_div.get("class", []) + ["has-keychange"]

        return str(doc)

    def as_html(self, smp: SheetMusicProcessor):
        html = "".join(line.as_html(self.key, smp) for line in self.lines)
        html = optimize(html, wrap=True)

        html = self.embed_analysis(html)

        return html
