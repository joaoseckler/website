import html
import json
import re
from enum import Enum

from laialaia.parse.chords import transpose


class TranspositionDict:
    def __init__(self):
        self._td = {}

    def update(self, chord):
        self._td[chord] = transpose(chord)

    def update_from_line(self, line):
        for chord in line.split:
            self.update(chord)

    def as_json(self):
        return json.dumps(self._td, ensure_ascii=False)


def escape(s):
    return html.escape(s.replace("\n", ""))


class CssClass(Enum):
    CHORDLINE = "chordline"
    CHORD = "chord"
    VOLTA = "volta"
    CHORDGROUP = "chordgroup"
    SECTION = "sectionline"
    POSITIONED = "pos"
    ANCHORED = "anchored"
    SHEET_MUSIC = "sheet-music"
    ANCHORED_SHEET_MUSIC = "anchored-sheet-music"


class Color(Enum):
    CHORD = "#b71c1c"
    SECTION = "#004d40"
    HOVERED = "#b71c1c"
    ACTIVE_BG = "#eeeeee"
    GRAY = "#444"
    BORDER_COLOR = "#e9dbdb"


section_pattern = re.compile(r"\[.*\]")
word_pattern = re.compile(
    r"\S*(\<span[^>]*\>[^<]*\<span[^>]*\>[^>]*\</span>[^>]*\</span\>[^\s<]*)+\S*"
)

key_pattern = re.compile(r"^[A-H][#b]?m?$")
modulation_pattern = re.compile("^([iI]{1,3}|iv|IV|vi{0,2}|VI{0,2})[b#]?$")

lysincopation = """set-duration = #(define-music-function (music n) (ly:music? ly:duration?)
 (ly:music-set-property! music 'duration n)
 music
)

% Create sincopation
sinc =
#(define-music-function (n1 n2 n3)
                        (ly:music? ly:music? ly:music?)
                        #{
                        $(set-duration n1 #{ 16 #})
                        $(set-duration n2 #{ 8 #})
                        $(set-duration n3 #{ 16 #})

                        #})"""
