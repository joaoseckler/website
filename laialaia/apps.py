from django.apps import AppConfig


class LaialaiaBackendConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "laialaia"
