from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from laialaia import views

urlpatterns = [
    path("", views.CifraList.as_view()),
    path("caderno", views.Caderno.as_view()),
    path("cifras", views.CifraList.as_view()),
    path("cifra/<str:pk>/", views.CifraDetail.as_view()),
    path("pages", views.TextPageList.as_view()),
    path("page/<str:pk>/", views.TextPageDetail.as_view()),
    path("sumario/<str:pk>/", views.SummaryDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
