from __future__ import annotations

import re
from enum import Flag, auto
from functools import cache

NOTES = "CDEFGAB"
SEMITONES = [0, 2, 4, 5, 7, 9, 11]


class Interval:
    P1: Interval
    m2: Interval
    M2: Interval
    m3: Interval
    M3: Interval
    P4: Interval
    a4: Interval
    d5: Interval
    P5: Interval
    m6: Interval
    M6: Interval
    m7: Interval
    M7: Interval

    # Major and perfect are considered "regular" (acc == 0)
    regulars = [0, 2, 4, 5, 7, 9, 11]

    def __init__(self, a1, a2=0):
        if isinstance(a1, str):
            number, acc = self.parse_string(a1)
        else:
            number, acc = a1, a2

        assert number < 7

        self.number = number
        self.acc = acc
        self.n = self.regulars[number] + acc

    @classmethod
    def common_intervals(cls) -> list[Interval]:
        return [
            cls.P1,
            cls.m2,
            cls.M2,
            cls.m3,
            cls.M3,
            cls.P4,
            cls.a4,
            cls.d5,
            cls.P5,
            cls.m6,
            cls.M6,
            cls.m7,
            cls.M7,
        ]

    @classmethod
    def from_pitches(cls, p1, p2):
        note = (p2.note - p1.note) % 7
        n = (p2.n - p1.n) % 12
        acc = n - SEMITONES[note]

        return cls(note, acc)

    @staticmethod
    def parse_string(s):
        mod, number = s[:-1], int(s[-1])
        number -= 1

        if number in (0, 3, 4):
            if mod == "P":
                return number, 0
        else:
            if mod == "M":
                return number, 0
            if mod == "m":
                return number, -1

        acc = -1 if number not in (0, 3, 4) and "d" in mod else 0

        for m in mod:
            if m == "a":
                acc += 1
            elif m == "d":
                acc -= 1
            else:
                raise ValueError(f"Cannot parse interval: {s}")

        return number, acc

    def __eq__(self, other):
        return self.number == other.number and self.acc == other.acc

    def __hash__(self):
        return hash((self.number, self.acc))

    def modifier(self):
        mod = ""

        if self.number in (0, 3, 4):
            if self.acc > 0:
                mod = "a" * self.acc
            elif self.acc < 0:
                mod = "d" * -self.acc
            else:
                mod = "P"

        else:
            if self.acc > 0:
                mod = "a" * self.acc
            elif self.acc == 0:
                mod = "M"
            elif self.acc == -1:
                mod = "m"
            elif self.acc < -1:
                mod = "d" * (-self.acc - 1)

        return mod

    def of_chord(self, chord):
        if self in chord.intervals:
            return str(self.number + 1)

        return str(self)

    def inverse(self):
        if self.number in (0, 3, 4):
            return Interval((7 - self.number) % 7, -self.acc)

        return Interval((7 - self.number) % 7, -1 - self.acc)

    def transpose(self, other):
        number = (self.number + other.number) % 7
        semitones = (self.n + other.n) % 12
        acc = semitones - self.regulars[number]

        return Interval(number, acc)

    def __str__(self):
        return f"{self.modifier()}{self.number + 1}"

    def __repr__(self):
        return f"Interval({self})"


for name in (
    "P1",
    "m2",
    "M2",
    "m3",
    "M3",
    "P4",
    "a4",
    "d5",
    "P5",
    "m6",
    "M6",
    "m7",
    "M7",
):
    setattr(Interval, name, Interval(name))


class Pitch:
    def __init__(self, *args):
        if len(args) == 1 and isinstance(args[0], str):
            self._init_from_str(args[0])
        elif len(args) == 2 and isinstance(args[0], int) and isinstance(args[1], int):
            self._init_from_ints(*args)
        else:
            raise ValueError("Pitch constructor expected one string or two ints")

    def _init_from_str(self, p):
        mo = re.match("([A-H])([b#])?", p)
        assert mo is not None

        letter = "B" if p[0] == "H" else p[0]

        self._init_from_ints((ord(letter) - ord("A") - 2) % 7, self.acc2n(mo.group(2)))

    def _init_from_ints(self, note, acc):
        self.note = note
        self.acc = acc
        self.n = (SEMITONES[self.note] + self.acc) % 12  # semitones

    @staticmethod
    def acc2n(acc):
        match acc:
            case "#":
                return 1
            case "b":
                return -1
            case _:
                return 0

    @staticmethod
    def n2acc(n):
        if n >= 0:
            return "#" * n
        return "b" * (-n)

    _max_acc = 1
    _n_acc = _max_acc * 2 + 1

    @classmethod
    def max_id(cls):
        return len(SEMITONES) * cls._n_acc

    @cache
    def id(self) -> int:
        assert (
            abs(self.acc) <= self._max_acc
        ), f"Can't get id of pitch with more abs(acc) > {self._max_acc}"

        return self.note * self._n_acc + self.acc + self._max_acc

    @classmethod
    def from_id(cls, id):
        note = id // cls._n_acc
        acc = id % cls._n_acc - cls._max_acc

        return cls(note, acc)

    def transpose(self, interval):
        note = (self.note + interval.number) % 7

        # Compare self's semitones + interval semitones with the
        # semitones of the chosen note without accidents to determine
        # final accidentals
        actual = (self.n + interval.n) % 12
        no_acc = SEMITONES[note]
        acc = actual - no_acc
        if acc > 6:
            acc -= 12

        return Pitch(f"{NOTES[note]}{self.n2acc(acc)}")

    def __str__(self):
        return f"{NOTES[self.note]}{self.n2acc(self.acc)}"

    def __repr__(self):
        return f"Pitch({NOTES[self.note]}{self.n2acc(self.acc)})"

    def __hash__(self):
        return hash((self.note, self.acc))

    def __eq__(self, other):
        return self.acc == other.acc and self.note == other.note


class ChordType(Flag):
    MAJOR = auto()
    MINOR = auto()
    DOMINANT = auto()
    DIMINISHED = auto()
    HALF_DIMINISHED = auto()

    @staticmethod
    def type_from_combinations(chars):
        s = set(chars)

        relations = {
            (
                "minor",
                "diminished_fifth",
                "seventh",
            ): ChordType.HALF_DIMINISHED,
            ("minor", "diminished_fifth"): ChordType.DIMINISHED,
            ("minor",): ChordType.MINOR,
            ("diminished",): ChordType.DIMINISHED,
            ("seventh",): ChordType.DOMINANT,
        }

        for tup, ct in relations.items():
            if set(tup) <= s:
                return ct

        return ChordType.MAJOR

    def is_major(self):
        return bool(self & (ChordType.MAJOR | ChordType.DOMINANT))
