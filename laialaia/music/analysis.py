from itertools import groupby
from operator import attrgetter, itemgetter
from typing import BinaryIO

import numpy as np

from .harmony import Chord, ChordFunctions, Function, Key
from .hmm import KeyDetectionHMM


class Range:
    def __init__(self, a: int, b: int):
        assert a <= b, f"Range received disordered {a} and {b}"
        self.beg: int = a
        self.end: int = b
        self.length: int = self.end - self.beg

    def intersects(self, other):
        return (self.end > other.beg and self.beg < other.end) or (
            other.end > self.beg and other.beg < self.end
        )

    def __str__(self):
        return f"{self.beg}-{self.end}"

    def __repr__(self):
        return f"{type(self).__name__}({self})"

    def __hash__(self):
        return hash((self.beg, self.end))

    def __eq__(self, other):
        return self.beg == other.beg and self.end == other.end

    def __le__(self, other):
        return self.beg >= other.beg and self.end <= other.end

    def __ge__(self, other):
        return self.beg <= other.beg and self.end >= other.end

    def __lt__(self, other):
        return not self >= other

    def __gt__(self, other):
        return not self <= other

    def __len__(self):
        return self.length

    def __iter__(self):
        return iter((self.beg, self.end))


class KeyDetecter:
    # Chance of staying in the same key
    permanence_prob = 0.8
    permanence_transition_weight = Key.max_id() * permanence_prob

    # Chance of transitioning to an unexpected key
    random_transition_prob = 0.02
    random_transition_weight = Key.max_id() * random_transition_prob

    # Weight for any random chord
    random_chord_weight = 0.1

    # Modulations shorter than this will be discarded
    min_progression_length = 3

    # Modulations shorter than this at the end of song will be discarded
    # (The ends often contain repetitions that confuse the sequential logic of
    # the hmm)
    min_final_progression_length = 10

    # weight
    weight_1 = 2.0
    weight_5 = 2.0
    weight_4 = 1.5

    def __init__(
        self,
        chords: list[Chord],
        reference_key: None | Key = None,
        train_factor: float | None = 1.0,
        model_parameters_file: str | BinaryIO | None = None,
    ):
        self.n_keys = Key.max_id()
        self.n_chords = Chord.max_id()
        self.chords = chords
        self.reference_key = reference_key
        self.train_factor = train_factor
        self.chord_ids = np.array([c.id() for c in chords])
        self.possible_keys_by_chord = self.calc_possible_keys_by_chord()

        self.possible_keys_objects = set.union(*self.possible_keys_by_chord)
        self.possible_keys = set(k.id() for k in self.possible_keys_objects)
        self.occurring_keys = self.possible_keys & {
            k.concrete_hf[0] for k in self.possible_keys_objects
        }

        self.model = KeyDetectionHMM(
            init=self.calc_init(),
            trans=self.calc_transition(),
            emit=self.calc_emissions(),
        )

        self.model.load_parameters(
            train_factor=self.train_factor, file=model_parameters_file
        )

    def calc_possible_keys_by_chord(self):
        groups = [set() for _ in range(len(self.chords))]
        union_next = [False] * len(self.chords)

        for key in (Key.from_id(i) for i in range(Key.max_id())):
            functions = ChordFunctions(key, self.chords)

            for i, function in enumerate(functions):
                msg = ""
                if function & (Function.HF | Function.M4):
                    msg += f"        é HF de {key}"
                    groups[i].add(key)

                if groups and function & (
                    Function.D | Function.SD | Function.TS | Function.DD
                ):
                    msg += f"        é D, SD ou TS ({function}) de {key}"
                    union_next[i] = True

        for i in range(len(groups) - 2, -1, -1):
            if union_next[i]:
                groups[i] |= groups[i + 1]

        return list(groups)

    def calc_transition(self):
        transitions = np.zeros((self.n_keys, self.n_keys))

        for key in self.possible_keys_objects:
            for other in range(self.n_keys):
                weight = 0.1
                if key.id() == other:
                    weight = self.permanence_transition_weight
                elif key.id() != other and other not in self.occurring_keys:
                    weight = self.random_transition_weight

                transitions[key.id(), other] = weight

        row_totals = transitions.sum(axis=1, keepdims=True)
        transitions /= row_totals.max()

        return transitions

    def calc_weight(self, key: Key, chord: Chord):
        weight = 1.0
        for i, c in enumerate(key.concrete_hf):
            if c.type & chord.type and c.pitch == chord.pitch:
                match i:
                    case 0:
                        weight = self.weight_1
                    case 4:
                        weight = self.weight_5
                    case 3:
                        weight = self.weight_4
                    case _:
                        weight = 1

        occurrences = np.where(self.chord_ids == chord.id())[0].size

        factor = 1 + occurrences * 0.2

        return weight * factor

    def calc_emissions(self):
        emissions = np.full((self.n_keys, self.n_chords), self.random_chord_weight)

        for chord_i, possible_keys in enumerate(self.possible_keys_by_chord):
            chord = self.chords[chord_i]
            for key in possible_keys:
                emissions[key.id(), chord.id()] = self.calc_weight(key, chord)

        row_totals = emissions.sum(axis=1, keepdims=True)
        max_weight_sum = row_totals.max()

        if max_weight_sum != 0:
            emissions /= max_weight_sum

        return emissions

    def calc_init(self):
        ref_id = self.reference_key.id() if self.reference_key else -1
        init = np.zeros((self.n_keys,))

        for i in range(self.n_keys):
            if i == ref_id:
                init[i] = 10
            elif i in self.possible_keys:
                init[i] = 1

        return init / init.sum()

    def remove_shorts(self, keys: list[Key]):
        shorts = []
        end_shorts = []
        for _, group_it in groupby(enumerate(keys), key=itemgetter(1)):
            group = tuple(group_it)
            i = group[0][0]
            length = len(group)
            end = i + length

            if end == len(keys) and (
                length < self.min_final_progression_length
                or length < self.min_progression_length
            ):
                end_shorts.append((i, end, length))

            elif length < self.min_progression_length:
                shorts.append((i, end, length))

        for start, end, length in end_shorts:
            if start > 0:
                keys[start:end] = [keys[start - 1]] * length

        for start, end, length in shorts:
            if start == 0:
                keys[:end] = [keys[end]] * length
            elif keys[start - 1] == keys[end]:
                keys[start:end] = [keys[end]] * length

        return keys

    def detect(self):
        return self.remove_shorts(
            [Key.from_id(k) for k in self.model.predict([c.id() for c in self.chords])]
        )


class SingleKeyAnalysis:
    def __init__(
        self,
        chords: list[str] | list[Chord],
        key: str | Key,
        range_: Range | None = None,
    ):
        self.chords: list[Chord] = [
            Chord(c) if isinstance(c, str) else c for c in chords
        ]
        self.key = key if isinstance(key, Key) else Key(key)
        self.functions = list(ChordFunctions(self.key, self.chords))
        self.generic = self.calc_generic()
        self.range = range_ or Range(0, 0)

    def calc_generic(self):
        return [
            chord.generic(self.key, function)
            for chord, function in zip(self.chords, self.functions)
        ]

    def __repr__(self):
        return f"{type(self).__name__}({self.key}, [{self.range}])"


class Analysis:
    def __init__(
        self,
        chords: list[str],
        reference_key: Key | None = None,
        key_changes: dict[int, str] | None = None,
        train_factor: float | None = None,
        model_parameters_file: str | BinaryIO | None = None,
    ):
        self.chords = [Chord(c) for c in chords]
        self.reference_key = reference_key
        self.train_factor = train_factor
        self.model_parameters_file = model_parameters_file
        self.analyses: list[SingleKeyAnalysis]

        if key_changes is None or self.reference_key is None:
            self.analyses = self.detect_keys(self.reference_key)
            if self.reference_key is None:
                self.reference_key = self.calc_reference_key()
        if key_changes is not None:
            self.analyses = self.calc_analyses_from_key_changes(key_changes)

        self.functions = self.calc_functions()
        self.generic = self.calc_generic()
        self.key_changes = (
            key_changes if key_changes is not None else self.calc_keychanges()
        )

        assert len(self.generic) == len(self.chords)

    def calc_analyses_from_key_changes(self, key_changes: dict[int, str]):
        assert self.reference_key is not None

        analyses = []
        start = 0
        key = self.reference_key

        for end, mod in (
            *sorted(key_changes.items()),
            (len(self.chords), None),
        ):
            if end > start:
                analyses.append(
                    SingleKeyAnalysis(
                        self.chords[start:end], key, range_=Range(start, end)
                    )
                )
            key = self.reference_key.modulate(mod) if mod else key
            start = end

        return analyses

    def detect_keys(self, reference_key: None | Key):
        kd = KeyDetecter(
            self.chords,
            reference_key,
            train_factor=self.train_factor,
            model_parameters_file=self.model_parameters_file,
        )
        keys = kd.detect()
        analyses = []

        for key, group in groupby(enumerate(keys), key=itemgetter(1)):
            (start, _), *rest = group
            try:
                *__, (end, ___) = rest
            except ValueError:
                end = start
            analyses.append(
                SingleKeyAnalysis(
                    self.chords[start : end + 1],
                    key,
                    range_=Range(start, end + 1),
                )
            )

        return list(sorted(analyses, key=lambda an: an.range.beg))

    def calc_reference_key(self) -> Key:
        n = len(self.chords)

        return next(
            (an for an in self.analyses if an.range.length / n > 0.3), self.analyses[0]
        ).key

    def calc_keychanges(self):
        kc = {
            an.range.beg: str(
                self.reference_key.roman_numeral(an.key.pitch, an.key.major)
            )
            for an in self.analyses
            if not (an.key == self.reference_key and an.range.beg == 0)
        }

        return kc

    def calc_generic(self):
        return sum(
            map(attrgetter("generic"), self.analyses),
            [],
        )

    def calc_functions(self):
        return sum(
            map(attrgetter("functions"), self.analyses),
            [],
        )

    def key_at(self, i: int):
        return next(an.key for an in self.analyses if an.range.beg <= i < an.range.end)
