import math
import pickle
import re
from collections.abc import Sequence
from itertools import product
from pathlib import Path
from typing import Any, BinaryIO, cast, override

import numpy as np
from laialaia.music.harmony import Chord, Key
from numpy.typing import NDArray


class TrainingHistoryManager:
    directory = Path(__file__).parent / "train_history"
    current_name = "train_data.pkl"
    old_names = "train_data_%d.pkl"

    def __init__(self):
        self.directory.mkdir(exist_ok=True)
        assert "(" not in self.old_names

    def get_current(self, file: str | BinaryIO | None):
        try:
            if not file:
                with open(self.directory / self.current_name, "rb") as f:
                    return pickle.load(f)
            if isinstance(file, str):
                with open(file, "rb") as f:
                    return pickle.load(f)
            else:
                file.seek(0)
                return pickle.load(file)

        except FileNotFoundError:
            return None

    def last_file(self) -> Path | None:
        glob = self.old_names.replace("%d", "*")
        files = sorted(self.directory.glob(glob), reverse=True)

        return next(iter(files), None)

    def last_file_number(self) -> int:
        file = self.last_file()

        if not file:
            return 0

        pattern = self.old_names.replace("%d", r"(\d+)")
        match = re.search(pattern, file.name)

        if not match:
            return 0

        return int(match.group(1))

    def move_current(self):
        current = self.directory / self.current_name

        if not current.is_file():
            return

        number = self.last_file_number() + 1
        current.rename(self.directory / (self.old_names % number))

    def dump(self, obj: Any, file: BinaryIO | str | None):
        if not file:
            self.move_current()
            with open(self.directory / self.current_name, "wb") as f:
                pickle.dump(obj, f)

        elif isinstance(file, str):
            with open(file, "wb") as f:
                pickle.dump(obj, f)
        else:
            pickle.dump(obj, file)

    load = get_current


class HiddenMarkovModel:
    def __init__(
        self,
        n_states: int,
        max_obs: int,
        init: Sequence[float] | None = None,
        trans: Sequence[Sequence[float]] | None = None,
        emit: Sequence[Sequence[float]] | None = None,
    ) -> None:
        if init is None:
            self.init = np.full((n_states,), fill_value=1 / n_states)
        else:
            self.init = np.array(init)
            assert math.isclose(self.init.sum(), 1), "init doesn't add up to 1"

        self.history = TrainingHistoryManager()
        self.n_states = n_states
        self.max_obs = max_obs

        self.trans = np.array(trans) if trans is not None else None
        self.emit = np.array(emit) if emit is not None else None

    def viterbi(self, obs: Sequence[int]):
        assert (
            self.init is not None and self.trans is not None and self.emit is not None
        ), "HMM parameters not given at initializations and model was not trained"

        n_obs = len(obs)
        prob = np.zeros((n_obs, self.n_states))
        prev = np.zeros((n_obs, self.n_states))

        prob[0] = self.init * self.emit.transpose()[obs[0]]
        for t in range(1, n_obs):
            for s, r in product(range(self.n_states), range(self.n_states)):
                new_prob: float = (
                    prob[t - 1][r] * self.trans[r, s] * self.emit[s, obs[t]]
                )
                if new_prob > prob[t][s]:
                    prob[t][s] = new_prob
                    prev[t][s] = r

        path: NDArray[np.int_] = np.zeros((n_obs,), dtype=int)
        path[n_obs - 1] = max(
            range(self.n_states),
            key=lambda s: cast(float, prob[n_obs - 1, s]),
        )
        for t in range(n_obs - 2, -1, -1):
            path[t] = prev[t + 1][path[t + 1]]

        return [int(p) for p in path]

    def merge_parameters(
        self,
        new_trans: NDArray[np.float64],
        new_emit: NDArray[np.float64],
        train_factor: float | None = 1.0,
    ):
        if train_factor is None:
            train_factor = 1.0

        if self.trans is not None:
            self.trans = train_factor * new_trans + (1 - train_factor) * self.trans
        else:
            self.trans = new_trans

        if self.emit is not None:
            self.emit = train_factor * new_emit + (1 - train_factor) * self.emit
        else:
            self.emit = new_emit

    def train(
        self,
        observations_list: Sequence[Sequence[int]],
        observation_states_list: Sequence[Sequence[int]],
        train_factor=1.0,
    ):
        remove_pairs = np.array(
            [
                [prv[-1], nxt[0]]
                for prv, nxt in zip(
                    observation_states_list[:-1:], observation_states_list[1:]
                )
            ]
        )

        observations = np.concat(observations_list)
        observation_states = np.concat(observation_states_list)

        assert observation_states.shape == observations.shape

        occ_states, occ_state_counts = np.unique(observation_states, return_counts=True)
        state_counts = np.zeros((self.n_states,))

        for occ_state, occ_state_count in zip(occ_states, occ_state_counts):
            state_counts[occ_state] = occ_state_count

        state_pairs = np.stack(
            (observation_states[:-1], observation_states[1:]), axis=1
        )

        pairs, counts = np.unique(state_pairs, axis=0, return_counts=True)
        new_trans = np.zeros((self.n_states, self.n_states), dtype=float)

        for prv, nxt in remove_pairs:
            new_trans[prv, nxt] -= 1

        for (prv, nxt), count in zip(pairs, counts):
            new_trans[prv, nxt] += count
            new_trans[prv, nxt] /= state_counts[prv]

        all_so_pairs = np.stack((observation_states, observations), axis=1)
        so_pairs, so_counts = np.unique(all_so_pairs, axis=0, return_counts=True)
        new_emit = np.zeros((self.n_states, self.max_obs), dtype=float)
        for (state, obs), so_count in zip(so_pairs, so_counts):
            new_emit[state, obs] = so_count / state_counts[state]

        self.merge_parameters(new_trans, new_emit, train_factor)

    predict = viterbi

    def dump_parameters(self, file: BinaryIO | str | None = None):
        self.history.dump((self.trans, self.emit), file)

    def load_parameters(self, train_factor=1.0, file: str | BinaryIO | None = None):
        obj = self.history.load(file)

        if obj is None:
            return

        trans, emit = obj
        self.merge_parameters(trans, emit, train_factor)


class KeyDetectionHMM(HiddenMarkovModel):
    @override
    def __init__(
        self,
        init: Sequence[float] | None = None,
        trans: Sequence[Sequence[float]] | None = None,
        emit: Sequence[Sequence[float]] | None = None,
    ):
        super().__init__(
            n_states=Key.max_id(),
            max_obs=Chord.max_id(),
            init=init,
            trans=trans,
            emit=emit,
        )
