from __future__ import annotations

import re
from dataclasses import dataclass
from enum import Flag, auto
from functools import cache
from itertools import chain
from operator import attrgetter
from typing import cast

from laialaia.music.common import ChordType, Interval, Pitch
from laialaia.parse.chords import ChordParser


class Function(Flag):
    U = 0
    HF = auto()  # Regular harmonic field
    D = auto()  # dominant
    SD = auto()  #  subdominant
    SDII = auto()  #  ii
    SDIV = auto()  #  iv
    TS = auto()  # tritone substitution
    M4 = auto()  # Minor 4th
    DD = auto()  # Diminished dominant


@dataclass
class RomanRepresentation:
    note: int
    is_major: bool
    acc: int
    roman_numerals = ["i", "ii", "iii", "iv", "v", "vi", "vii"]
    pattern = re.compile(
        f"^({'|'.join(roman_numerals)}"
        f"|{'|'.join(r.upper() for r in roman_numerals)})([#b]*)$"
    )

    def __str__(self):
        apply_case = str.upper if self.is_major else str.lower
        roman = self.roman_numerals[self.note]

        if self.acc >= 0:
            return apply_case(roman) + self.acc * "#"

        return apply_case(roman) + (-self.acc) * "b"

    @classmethod
    def from_string(cls, roman):
        match = cls.pattern.match(roman)
        assert match is not None, f"Invalid roman representation: {roman}"
        note = cls.roman_numerals.index(match.group(1).lower())
        acc = sum(1 if c == "#" else -1 for c in match.group(2))
        is_major = match.group(1).isupper()

        return cls(note=note, is_major=is_major, acc=acc)

    def as_interval(self):
        return Interval(self.note, self.acc)


@dataclass
class Degree:
    interval: Interval
    type: ChordType


class HarmonicField:
    data: None | list[Degree] = None
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super().__new__(cls)
        return cls._instance

    def __init__(self):
        self.types = list(map(attrgetter("type"), self.data))
        self.intervals = list(map(attrgetter("interval"), self.data))
        self.semitones = list(map(attrgetter("n"), self.intervals))

    def __iter__(self):
        return iter(self.data)


# pylint: disable-next=too-few-public-methods
class MajorHarmonicField(HarmonicField):
    _instance = None

    data = [
        Degree(Interval(0, 0), ChordType.MAJOR),
        Degree(Interval(1, 0), ChordType.MINOR),
        Degree(Interval(2, 0), ChordType.MINOR),
        Degree(Interval(3, 0), ChordType.MAJOR),
        Degree(Interval(4, 0), ChordType.DOMINANT | ChordType.MAJOR),
        Degree(Interval(5, 0), ChordType.MINOR),
        Degree(Interval(6, 0), ChordType.HALF_DIMINISHED),
    ]


# pylint: disable-next=too-few-public-methods
class MinorHarmonicField(HarmonicField):
    _instance = None

    data = [
        Degree(Interval(0, 0), ChordType.MINOR),
        Degree(Interval(1, 0), ChordType.HALF_DIMINISHED),
        Degree(Interval(2, -1), ChordType.MAJOR),
        Degree(Interval(3, 0), ChordType.MINOR),
        Degree(
            Interval(4, 0),
            ChordType.MINOR | ChordType.DOMINANT | ChordType.MAJOR,
        ),
        Degree(Interval(5, -1), ChordType.MAJOR),
        Degree(Interval(6, -1), ChordType.MAJOR | ChordType.DOMINANT),
    ]


class Key:
    def __init__(self, str_repr=None, pitch=None, major=None):
        if str_repr:
            self.pitch, self.major = self.parse_key(str_repr)
        else:
            self.pitch = pitch
            self.major = major

        self.hf = MajorHarmonicField() if self.major else MinorHarmonicField()
        self.concrete_hf = self.calc_concrete_hf()

    def calc_concrete_hf(self):
        concrete = []
        for degree in self.hf:
            pitch = self.pitch.transpose(degree.interval)
            concrete.append(Chord(pitch=pitch, type=degree.type))

        return concrete

    @classmethod
    def from_pitch(cls, pitch, major):
        return cls(pitch=pitch, major=major)

    @classmethod
    def max_id(cls) -> int:
        return Pitch.max_id() * 2

    @staticmethod
    @cache
    def _calc_id(pitch: Pitch, major: bool):
        return pitch.id() * 2 + int(major)

    def id(self) -> int:
        return self._calc_id(self.pitch, self.major)

    @classmethod
    def from_id(cls, id: int):
        pitch = Pitch.from_id(id // 2)
        major = bool(id % 2)

        return cls(pitch=pitch, major=major)

    @classmethod
    def transpose_id(cls, id: int, interval: Interval):
        pitch = Pitch.from_id(id // 2).transpose(interval)
        major = bool(id % 2)

        return cls._calc_id(pitch, major)

    @staticmethod
    def parse_key(key):
        mo = re.match("([A-H][b#]?)(m?)", key)
        assert mo is not None

        return (
            Pitch(mo.group(1)),
            mo.group(2) != "m",
        )

    def calc_major(self, major, acc, note):
        if major is None:
            if acc == 0:
                major = self.hf.types[note].is_major()
            else:
                major = True

        return major

    def modulate(self, roman):
        roman = RomanRepresentation.from_string(roman)
        interval = roman.as_interval()

        return self.__class__(
            pitch=self.pitch.transpose(interval), major=roman.is_major
        )

    def roman_numeral(self, pitch, major=None):
        note = (pitch.note - self.pitch.note) % 7
        n = (pitch.n - self.pitch.n) % 12
        acc = n - self.hf.semitones[note]

        return RomanRepresentation(note, self.calc_major(major, acc, note), acc)

    def roman_numeral_from_semitones(self, semitones, major=None):
        assert 0 <= semitones < 12
        semitones = (semitones - self.pitch.n) % 12

        note = (
            next(
                i
                for (i, n) in enumerate(chain(self.hf.semitones, (12,)))
                if n > semitones
            )
            - 1
        )
        reference_st = self.hf.semitones[note]
        acc = semitones - reference_st

        return RomanRepresentation(note, self.calc_major(major, acc, note), acc)

    def __str__(self):
        return f"{self.pitch}{'' if self.major else 'm'}"

    def __eq__(self, other):
        return self.major == other.major and self.pitch == other.pitch

    def __hash__(self):
        return hash((self.pitch, self.major))

    def __repr__(self):
        return f"Key({self})"


class Chord:
    def __init__(
        self,
        chord: str | None = None,
        pitch: Pitch | None = None,
        type: ChordType | None = None,
    ):
        if chord is not None:
            parsed = ChordParser(chord)
            self.raw = chord
            self.parts = parsed.decompose()
            self.intervals = parsed.intervals(self.parts)
            self.transposition = parsed.transpose(self.parts)
            self.pitch = Pitch(self.parts.root)
            self.type = parsed.type()
        elif pitch is not None and type is not None:
            self.pitch = pitch
            self.type = type
            self.raw = self.calc_raw()
        else:
            raise AssertionError(
                "Chord: expected string representation of pitch and type"
            )

    def __str__(self):
        return self.raw

    def __repr__(self):
        return f"{type(self).__name__}({self})"

    def __hash__(self):
        return hash(self.raw)

    def calc_raw(self):
        raw = str(self.pitch)
        if self.type & ChordType.MINOR:
            raw += "m"
        if self.type & ChordType.HALF_DIMINISHED:
            raw = raw.rstrip("m") + "m7(b5)"
        if self.type & ChordType.DIMINISHED:
            raw = raw.rstrip("m") + "°"
        if self.type & ChordType.DOMINANT:
            raw += "7"

        return raw

    @staticmethod
    @cache
    def _calc_id(pitch: Pitch, typ: ChordType):
        return pitch.id() * len(ChordType) + list(ChordType).index(typ)

    def id(self) -> int:
        return self._calc_id(self.pitch, self.type)

    @classmethod
    def max_id(cls) -> int:
        return Pitch.max_id() * len(ChordType)

    @classmethod
    def from_id(cls, id: int) -> Chord:
        pitch = Pitch.from_id(id // len(ChordType))
        typ = cast(ChordType, list(ChordType)[id % len(ChordType)])

        return cls(pitch=pitch, type=typ)

    @classmethod
    def transpose_id(cls, id: int, interval: Interval) -> int:
        pitch = Pitch.from_id(id // len(ChordType)).transpose(interval)
        typ = list(ChordType)[id % len(ChordType)]

        return cls._calc_id(pitch, typ)

    def is_major(self):
        return Interval(2, 0) in self.intervals

    def generic(self, key: Key, function: Function):
        final = ""

        if function & Function.HF:
            roman = str(key.roman_numeral(self.pitch, self.is_major()))
        elif function & Function.D:
            final_roman = str(key.roman_numeral_from_semitones((self.pitch.n + 5) % 12))
            if final_roman.lower() != "i":
                final = "/" + str(final_roman)
            roman = "V"

        elif function & Function.SDII:
            final_roman = str(key.roman_numeral_from_semitones((self.pitch.n - 2) % 12))
            if final_roman.lower() != "i":
                final = "/" + final_roman
            roman = "ii"

        elif function & Function.SDIV and not function & Function.HF:
            final_roman = str(key.roman_numeral_from_semitones((self.pitch.n - 5) % 12))
            if final_roman.lower() != "i":
                final = "/" + final_roman
            roman = "IV" if self.is_major() else "iv"

        elif function & Function.TS:
            final = "/" + str(key.roman_numeral_from_semitones((self.pitch.n - 1) % 12))
            roman = "IIb"
        else:
            roman = str(key.roman_numeral(self.pitch, self.is_major()))

        ret = re.sub("%root%m?", roman, self.transposition, count=1) + final

        if self.parts.slash:
            slash = Interval.from_pitches(self.pitch, Pitch(self.parts.slash))
            ret = ret.replace("%slash%", str(slash.of_chord(self)))

        return ret


class ChordFunctions:
    def __init__(self, key: Key, chords: list[Chord]):
        self.key = key
        self.functions = [Function.U] * len(chords)
        self.chords = chords
        self.n = len(self.chords)

        for i in range(self.n - 1, -1, -1):
            self.find_function(i)

    def __iter__(self):
        return iter(self.functions)

    def __getitem__(self, i):
        return self.functions[i]

    def next_chord(self, i):
        try:
            return self.chords[i + 1]
        except IndexError:
            return None

    def detect_dominant(self, i):
        chord = self.chords[i]
        nxt = self.next_chord(i)
        nxt2 = self.next_chord(i + 1)

        if nxt is None:
            return Function.U

        fourth = chord.pitch.transpose(Interval.P4)

        if (
            nxt2
            and nxt.type & ChordType.DIMINISHED
            and nxt2.pitch == fourth
            and nxt.pitch.n in ((chord.pitch.n + i) % 12 for i in (1, 4, 7, 10))
        ):
            self.functions[i + 1] |= Function.DD

        if chord.type == ChordType.DOMINANT and (
            fourth == nxt.pitch
            or (  # G7  Gm  C7  F
                chord.pitch == nxt.pitch
                and nxt.type & (ChordType.MINOR | ChordType.HALF_DIMINISHED)
            )
            or (  #  Bbm  C7  Bbm  C7  Bbm  C7  Fm
                nxt2
                and self.functions[i + 1] & Function.SD
                and nxt2.pitch == chord.pitch
                and nxt.type == chord.type
            )
        ):
            return Function.D

        return Function.U

    def detect_tritone_substitution(self, i: int):
        chord = self.chords[i]
        nxt = self.next_chord(i)
        nxt2 = self.next_chord(i + 1)

        if not nxt:
            return Function.U

        m2 = chord.pitch.transpose(Interval(1, -1).inverse())
        a1 = chord.pitch.transpose(Interval(0, 1).inverse())

        if chord.type == ChordType.DOMINANT and (
            (nxt.pitch in (m2, a1))
            or (nxt2 and self.functions[i + 1] & Function.SD and nxt2.pitch in (m2, a1))
        ):
            return Function.TS
        return Function.U

    def detect_subdominant(self, i: int):
        chord = self.chords[i]
        nxt = self.next_chord(i)

        if not nxt:
            return Function.U

        sd = (
            chord.type & (ChordType.MINOR | ChordType.HALF_DIMINISHED)
            and Interval.from_pitches(chord.pitch, nxt.pitch)
            in (Interval.P4, Interval.M2)
            and nxt.type & ChordType.DOMINANT
        )

        if not sd:
            return Function.U

        ret = Function.SD
        if sd and Interval.from_pitches(chord.pitch, nxt.pitch) == Interval.P4:
            ret |= Function.SDII
        if sd and Interval.from_pitches(chord.pitch, nxt.pitch) == Interval.M2:
            ret |= Function.SDIV

        return ret

    def detect_harmonic_field(self, i: int):
        chord = self.chords[i]

        roman = self.key.roman_numeral(chord.pitch)
        n = roman.note
        hf_intervals = set(
            self.key.hf.intervals + ([Interval(6, 0)] if roman.note == 4 else [])
        )

        if (
            roman.acc == 0
            and bool(self.key.hf.types[n] & chord.type)
            and all(
                Interval.from_pitches(self.key.pitch, chord.pitch.transpose(i))
                in hf_intervals
                for i in chord.intervals
                if i.number in (0, 2, 4, 6)
            )
        ):
            return Function.HF

        return Function.U

    def detect_minor_4th(self, i: int):
        if not i:
            return Function.U

        chord = self.chords[i]
        previous = self.chords[i - 1]

        if (
            self.key.major
            and chord.type & ChordType.MINOR
            and Interval.from_pitches(self.key.pitch, chord.pitch) == Interval.P4
            and (
                (
                    self.detect_harmonic_field(i - 1)
                    and Interval.from_pitches(self.key.pitch, previous.pitch).number
                    in [1, 3, 5]
                )
                or (
                    previous.pitch.n == chord.pitch.n + 1
                    and previous.type & ChordType.HALF_DIMINISHED
                )
            )
        ):
            return Function.M4
        return Function.U

    def find_function(self, i: int):
        function = Function.U
        chord = self.chords[i]
        nxt = self.next_chord(i)

        if nxt and nxt.type == chord.type and nxt.pitch == chord.pitch:
            return self.functions[i + 1]

        function |= self.detect_dominant(i)
        function |= self.detect_subdominant(i)
        function |= self.detect_tritone_substitution(i)
        function |= self.detect_minor_4th(i)
        function |= self.detect_harmonic_field(i)

        self.functions[i] |= function
