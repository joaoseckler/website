from collections import namedtuple

from laialaia.metadata import parse_header
from laialaia.metadata.version_names import get_cifra_slug, resolve_version
from laialaia.models.cifra import Album, Artist, Cifra, CifraVersion, GenericKeyValue
from laialaia.models.page import TextPage
from laialaia.parse import md
from laialaia.parse.cifras import Lines
from laialaia.parse.common import TranspositionDict
from laialaia.parse.sheet_music import SheetMusicProcessor
from laialaia.util import normalize

VersionNames = namedtuple("VersionNames", ["display", "slug", "display_type"])


class ObjectExistsError(Exception):
    pass


class TxtRegistrar:
    def __init__(self, update=False, dev=False):
        self.update = update
        self.development = dev

    def parse_txt(self, txt):
        metadata, raw_lines = parse_header(txt.strip().split("\n"))
        transposition_dict = TranspositionDict()
        lines = Lines(
            raw_lines,
            metadata["key"],
            transposition_dict=transposition_dict,
            detect_modulations=metadata.pop("detect_modulations"),
        )

        if metadata["key"] is None:
            metadata["key"] = lines.key

        smp = SheetMusicProcessor(transposition_dict, dev=self.development)
        html = lines.as_html(smp)

        version_names = VersionNames(*resolve_version(metadata, []))
        raw = "\n".join(raw_lines)

        return metadata, raw, html, version_names, transposition_dict.as_json()

    def get_update_or_create(self, cls, **kwargs):
        if self.update:
            return cls.objects.get_or_create(**kwargs)
        return cls.objects.update_or_create(**kwargs)

    def handle_album(self, metadata):
        if not metadata["album"]:
            return None

        authors = metadata["album"]["authors"] or metadata["interpreters"]
        album_authors = [
            self.get_update_or_create(
                Artist,
                slug=normalize(a),
                defaults={"name": a},
            )[0]
            for a in authors
        ]

        album, album_was_created = self.get_update_or_create(
            Album,
            slug=normalize(metadata["album"]["title"]),
            defaults={
                "title": metadata["album"]["title"],
                "year": metadata["year"],
            },
        )

        if album_was_created or self.update:
            for author in album_authors:
                author.save()
                album.authors.add(author)

            album.save()

        return album

    def handle_interpreters(self, metadata, version):
        if not metadata["interpreters"]:
            return

        interpreters = [
            self.get_update_or_create(Artist, slug=normalize(a), defaults={"name": a})[
                0
            ]
            for a in metadata["interpreters"]
        ]

        for interpreter in interpreters:
            interpreter.save()
            version.interpreters.add(interpreter)

        version.save()

    def handle_info(self, metadata, version):
        for k, v in metadata["info"].items():
            kv, created = self.get_update_or_create(
                GenericKeyValue,
                cifra_version=version,
                key=k,
                defaults={"value": v},
            )
            if not created and self.update:
                kv.value = v

            kv.save()

        version.info.exclude(key__in=metadata["info"]).delete()

    def delete(self, txt):
        metadata, _ = parse_header(txt.strip().split("\n"))

        cifra = Cifra.objects.get(
            slug=get_cifra_slug(metadata),
        )

        cifra.delete()

    def register(self, txt):
        (
            metadata,
            raw,
            html,
            version_names,
            transposition_dict,
        ) = self.parse_txt(txt)

        cifra, cifra_was_created = self.get_update_or_create(
            Cifra,
            slug=get_cifra_slug(metadata),
            defaults={"title": metadata["title"]},
        )
        cifra.save()

        existing_version = CifraVersion.objects.filter(
            slug=version_names.slug, cifra=cifra
        )

        if not self.update and existing_version.exists():
            raise ObjectExistsError(f"{metadata['title']} already exists.")

        album = self.handle_album(metadata)

        if cifra_was_created or self.update:
            authors = [
                self.get_update_or_create(
                    Artist,
                    slug=normalize(a),
                    defaults={"name": a},
                )[0]
                for a in metadata["authors"]
            ]
            for author in authors:
                author.save()
                cifra.authors.add(author)

            cifra.save()

        version_options = {
            "slug": version_names.slug,
            "cifra": cifra,
            "album": album,
            "year": metadata["year"],
            "raw": raw,
            "html": html,
            "key": metadata["key"],
            "main": metadata["main"],
            "display_version": version_names.display,
            "display_version_type": version_names.display_type,
            "transposition_dict": transposition_dict,
        }

        if existing_version.exists():
            version = existing_version[0]
            for key, val in version_options.items():
                setattr(version, key, val)
            version.save()
        else:
            version = CifraVersion.objects.create(**version_options)

        self.handle_interpreters(metadata, version)
        self.handle_info(metadata, version)


def register_from_txt(txt, update=False, dev=False):
    registrar = TxtRegistrar(update, dev=dev)

    return registrar.register(txt)


def delete_from_txt(txt):
    registrar = TxtRegistrar(False)

    return registrar.delete(txt)


def register_from_md(txt, filename, update=False):
    text, description, title, slug = md.parse(txt, filename)

    existing_version = TextPage.objects.filter(slug=slug)
    if existing_version.exists() and not update:
        raise ObjectExistsError(f"{title} already exists")

    obj, _ = TextPage.objects.update_or_create(
        slug=slug,
        defaults={"title": title, "description": description, "text": text},
    )

    obj.save()
