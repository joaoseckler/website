from django.http import FileResponse
from django.utils.decorators import method_decorator
from rest_framework import filters, generics
from rest_framework.views import APIView

from laialaia.decorators import control_cache
from laialaia.filters import PrioritizedSearchFilter
from laialaia.models.cifra import Cifra
from laialaia.models.page import TextPage
from laialaia.models.summary import Summary
from laialaia.serializers.cifra import CifraSerializer, ShallowCifraSerializer
from laialaia.serializers.page import TextPageSerializer
from laialaia.serializers.summary import SummarySerializer
from laialaia.songbook import generate_songbook

control_cache_options = {
    "max-age": 86_400,  # 1 day
    "stale-while-revalidate": 31_536_000,  # 1 year
    "stale-if-error": 31_536_000,  # 1 year
}


@method_decorator(control_cache(**control_cache_options), name="dispatch")
class CifraList(generics.ListAPIView):
    queryset = Cifra.objects.all()
    serializer_class = ShallowCifraSerializer

    filter_backends = [PrioritizedSearchFilter, filters.OrderingFilter]
    search_fields = [
        "title",
        "slug",
        "authors__name",
        "versions__year",
        "versions__version",
        "versions__slug",
        "versions__info__value",
        "versions__raw",
    ]
    ordering_fields = [
        "title",
        "authors__name",
    ]
    ordering = ["slug"]


@method_decorator(control_cache(**control_cache_options), name="dispatch")
class CifraDetail(generics.RetrieveAPIView):
    queryset = Cifra.objects.all()
    serializer_class = CifraSerializer


class TextPageList(generics.ListAPIView):
    queryset = TextPage.objects.all()
    serializer_class = TextPageSerializer


class TextPageDetail(generics.RetrieveAPIView):
    queryset = TextPage.objects.all()
    serializer_class = TextPageSerializer


class SummaryDetail(generics.RetrieveAPIView):
    queryset = Summary.objects.all()
    serializer_class = SummarySerializer


class Caderno(APIView):
    def post(self, request):
        open_file = generate_songbook(request.data)

        return FileResponse(
            open_file,
            as_attachment=True,
        )
