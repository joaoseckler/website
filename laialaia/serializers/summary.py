from rest_framework import serializers

from laialaia.models.summary import Summary, SummaryEntry


class SummaryEntrySerializer(serializers.ModelSerializer):
    class Meta:
        model = SummaryEntry
        fields = ["slug", "title", "display_info", "page"]


class SummarySerializer(serializers.ModelSerializer):
    entries = SummaryEntrySerializer(many=True)

    class Meta:
        model = Summary
        fields = ["uuid", "entries", "title", "use_page_order"]
