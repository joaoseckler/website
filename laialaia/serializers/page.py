from rest_framework import serializers

from laialaia.models.page import TextPage


class TextPageSerializer(serializers.ModelSerializer):
    class Meta:
        model = TextPage
        fields = ["slug", "text", "title", "description"]
