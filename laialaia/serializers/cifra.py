from collections import OrderedDict

from rest_framework import serializers

from laialaia.models.cifra import (
    Album,
    Artist,
    Cifra,
    CifraVersion,
    GenericKeyValue,
)


class ArtistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = ["slug", "name"]


class AlbumSerializer(serializers.ModelSerializer):
    authors = ArtistSerializer(many=True)

    class Meta:
        model = Album
        fields = ["slug", "title", "authors", "year"]


class GenericKeyValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = GenericKeyValue
        fields = ["key", "value"]


class ShallowCifraVersionSerializer(serializers.ModelSerializer):
    interpreters = ArtistSerializer(many=True)
    album = AlbumSerializer(many=False)
    info = GenericKeyValueSerializer(many=True)
    year = serializers.IntegerField()

    def to_representation(self, instance):
        new = OrderedDict()
        rep = super().to_representation(instance)

        for item in rep["info"]:
            new[item["key"]] = item["value"]

        rep["info"] = new

        return rep

    class Meta:
        model = CifraVersion
        fields = [
            "slug",
            "interpreters",
            "album",
            "year",
            "display_version",
            "info",
            "key",
        ]


class CifraVersionSerializer(ShallowCifraVersionSerializer):
    class Meta:
        model = CifraVersion
        fields = [
            "slug",
            "interpreters",
            "album",
            "year",
            "raw",
            "html",
            "display_version",
            "display_version_type",
            "info",
            "key",
            "transposition_dict",
            "main",
        ]


class ShallowCifraSerializer(serializers.ModelSerializer):
    versions = ShallowCifraVersionSerializer(many=True)
    authors = ArtistSerializer(many=True)

    class Meta:
        model = Cifra
        fields = ["slug", "title", "authors", "versions"]
        read_only_fields = ["versions", "authors"]


class CifraSerializer(ShallowCifraSerializer):
    versions = CifraVersionSerializer(many=True)
