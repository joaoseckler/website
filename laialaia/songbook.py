import io
import subprocess as sp
import tempfile
from pathlib import Path
from urllib import parse

from django.conf import settings
from fpdf import FPDF
from laialaia.models.cifra import Cifra
from laialaia.models.summary import Summary, SummaryEntry
from laialaia.util import display_authors
from pypdf import PaperSize, PdfReader, PdfWriter
from pypdf.errors import PageSizeNotDefinedError

FONT_FILENAME = Path(__file__).parent / "fonts" / "NotoSansMono-Regular.ttf"


class SongBookGenerator:
    def __init__(self, base_dir, data):
        self.base_dir = Path(base_dir)
        self.data = data

        self.pages = {}
        self.data_dir = self.base_dir / "data"
        self.data_dir.mkdir(exist_ok=True)
        self.chrome_options = [
            "--headless=new",
            "--no-pdf-header-footer",
            "--virtual-time-budget=2000",
            "--no-sandbox",
            f"--user-data-dir={self.data_dir}",
        ]

    @staticmethod
    def stamp_page(page, number, side=None):
        match side:
            case None:
                left = number % 2 == 0
            case True, "l", "L", "left":
                left = True
            case False, "r", "R", "right":
                left = False

        pdf = FPDF(format="A4")
        pdf.set_auto_page_break(False)
        pdf.add_page()
        pdf.add_font("Noto Sans Mono", fname=str(FONT_FILENAME))
        pdf.set_font("Noto Sans Mono", size=10)
        pdf.set_y(-10)
        pdf.cell(0, None, str(number), align=("L" if left else "R"))

        file = io.BytesIO(pdf.output())
        stamp = PdfReader(file).pages[0]
        page.merge_page(stamp, over=True)

    def page_numbers(self, merger, summary_pages, total_pages):
        numbers = list(range(total_pages))

        if self.data["firstPage"]:
            numbers = numbers[summary_pages:]

        if 0 in numbers:
            numbers = numbers[1:]

        for number in numbers:
            page = merger.pages[number]
            self.stamp_page(page, number + 1)

    def path2pdf(self, path, filename, key=None, generic=False, cifra=False):
        url = f"{settings.LAIALAIA_FRONT_URL}{path}"

        if cifra:
            search_params = f"?printing&font={self.data['minFontSize']}"

            if key:
                search_params += f"&tom={parse.quote(key)}"
            if generic:
                search_params += "&generic"

            url = url.replace("#", f"{search_params}#")

        cmd = [
            "google-chrome",
            *self.chrome_options,
            f"--print-to-pdf={filename}",
            url,
        ]

        with sp.Popen(
            cmd,
            stdout=sp.PIPE,
            stderr=sp.PIPE,
        ) as process:
            process.communicate()  # wait but no deadlocks
            returncode = process.returncode

        if returncode != 0 or not Path(filename).is_file():
            raise ValueError(f"Error while generating PDF from {path}")

    def make_summary(self, use_page_order=True):
        path = self.base_dir / f"summary_{'page' if use_page_order else 'alpha'}"

        summary = Summary.objects.create(
            title=self.data["title"] if "title" in self.data else "",
            use_page_order=use_page_order,
        )

        summary.save()

        try:
            for slug in self.data["chosenSlugs"]:
                cifra_slug = slug.split("#")[0]
                obj = Cifra.objects.get(slug=cifra_slug)

                entry = SummaryEntry.objects.create(
                    slug=slug,
                    title=obj.title,
                    display_info=display_authors(obj.authors.all()),
                    summary=summary,
                    page=self.pages[slug] if slug in self.pages else 0,
                )

                entry.save()

            self.path2pdf(f"/sumario/{summary.uuid}", path)
            return path

        finally:
            summary.delete()

    def get_initial_pages_and_dummy_summary(self):
        if self.data["firstPage"]:
            dummy_path = self.make_summary()
            dummy_pdf = PdfReader(dummy_path)

            return self.non_empty_pages(dummy_pdf), dummy_pdf

        return 0, None

    @staticmethod
    def non_empty_pages(reader):
        pages = len(reader.pages)
        while pages > 1 and not list(reader.pages)[pages - 1].extract_text():
            pages -= 1

        return pages

    def generate(self):
        filenames = {}

        for i, (slug, data) in enumerate(self.data["chosenSlugs"].items()):
            filename = self.base_dir / f"{i}.pdf"
            key = data["key"]
            generic = data["generic"]
            self.path2pdf(f"/{slug}", filename, key, generic, cifra=True)
            filenames[slug] = filename

        (
            summary_pages,
            dummy_summary,
        ) = self.get_initial_pages_and_dummy_summary()
        merger = PdfWriter()

        total_pages = summary_pages

        for slug, filename in filenames.items():
            reader = PdfReader(filename)
            pages = self.non_empty_pages(reader)

            if total_pages % 2 == 0 and pages % 2 == 0:
                try:
                    merger.add_blank_page()
                except PageSizeNotDefinedError:
                    try:
                        box = dummy_summary.pages[0].mediabox
                        merger.add_blank_page(height=box.height, width=box.width)
                    except AttributeError:
                        merger.add_blank_page(
                            height=PaperSize.A4.height,
                            width=PaperSize.A4.width,
                        )
                total_pages += 1

            merger.append(reader, (0, pages))
            self.pages[slug] = total_pages
            total_pages += pages

        if self.data["firstPage"]:
            summary_reader = PdfReader(self.make_summary())
            pages = self.non_empty_pages(summary_reader)
            merger.merge(
                position=0,
                fileobj=summary_reader,
                pages=(0, pages),
            )

        if self.data["lastPage"]:
            if len(merger.pages) % 2 == 0:
                merger.add_blank_page()

            summary_reader = PdfReader(self.make_summary(False))
            pages = self.non_empty_pages(summary_reader)
            merger.append(summary_reader, (0, pages))

        self.page_numbers(merger, summary_pages, total_pages)

        file = io.BytesIO()
        merger.write_stream(file)
        file.seek(0)
        return file


def generate_songbook(data):
    with tempfile.TemporaryDirectory() as base_dir:
        sbg = SongBookGenerator(base_dir, data)
        return sbg.generate()
