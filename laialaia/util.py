import re
from itertools import chain, combinations, groupby
from operator import itemgetter

from bs4 import BeautifulSoup
from django.utils.text import slugify
from unidecode import unidecode


def every_two(iterable):
    it = iter(iterable)
    return zip(it, it)


def display_join(array):
    if not array:
        return ""

    start = array[:-1]

    if start:
        return f"{', '.join(start)} e {array[-1]}"

    return array[-1]


def display_authors(authors):
    return display_join(list(map(lambda a: a.name, authors)))


def consecutives(data):
    for _, g in groupby(enumerate(data), lambda p: p[1] - p[0]):
        yield map(itemgetter(1), g)


def powerset(data):
    s = list(data)
    return chain.from_iterable(combinations(s, r) for r in range(len(s) + 1))


def normalize(string):
    s = BeautifulSoup(string, "lxml").get_text()

    return slugify(
        re.sub(
            r"[()\[\]\{\}]",
            "",
            unidecode(s).replace(" ", "-").replace("\n", "-").lower(),
        )
    )
