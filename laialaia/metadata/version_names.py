from enum import Enum

from laialaia.util import normalize


class VersionDisplayType(Enum):
    def reference():
        return ["Versão", "Intérprete", "Intérpretes", "Álbum"]

    VERSION = (1, reference())
    INTERP = (2, reference()[1:])
    ALBUM = (3, reference()[3:])
    INTERP_ALBUM = (4, [])


def comma_join(it):
    return ", ".join(it[:-1]) + (" e " if len(it) > 1 else "") + it[-1]


def make_version_display_of_type(header, vdt: VersionDisplayType):
    title = header["title"]
    ano = header["year"]
    interp = header["interpreters"]
    album = header["album"]
    version = header["display_version"]

    ano = f" ({ano})" if ano else ""

    if vdt.value[1] == VersionDisplayType.INTERP_ALBUM.value[1]:
        if not album or not interp:
            raise ValueError(f"{title}: Couldn't distinguish versions")

        return f"{comma_join(interp)} ({album['title']})" + ano, vdt

    if version and "Versão" in vdt.value[1]:
        return version + ano, VersionDisplayType.VERSION

    if interp and "Intérprete" in vdt.value[1]:
        return comma_join(interp) + ano, VersionDisplayType.INTERP

    if album and "Álbum" in vdt.value[1]:
        if album["authors"]:
            album_interp = album["authors"]
            full_album = f"{album['title']} - {comma_join(album['authors'])}"
            if not interp or set(album_interp) == set(interp):
                return full_album + ano, VersionDisplayType.ALBUM
            else:
                return (
                    f"{comma_join(interp)} ({full_album})" + ano,
                    VersionDisplayType.ALBUM,
                )

        else:
            return (
                f"{comma_join(interp)} - {album['title']}" + ano,
                VersionDisplayType.ALBUM,
            )

    raise ValueError(f"{title}: Couldn't define version")


def resolve_version(header, used_versions):
    for vdt in VersionDisplayType:
        vd = make_version_display_of_type(header, vdt)
        if vd[0] not in used_versions:
            break

    if vd[0] in used_versions:
        raise ValueError(f"{header['title']}: Couldn't distinguish versions")

    return vd[0], normalize(vd[0]), vd[1].value[0]


def get_cifra_slug(header):
    return normalize(header["title"])
