import html
import re

import markdown
from bs4 import BeautifulSoup
from unidecode import unidecode


def parse_album(album):
    if not album:
        return None

    else:
        title_authors = album.split(" - ")
        if len(title_authors) == 1:
            title = title_authors[0]
            authors = []
        else:
            title, authors = title_authors[:2]
            authors = authors.split(" | ")

    return {
        "title": title,
        "authors": authors,
    }


def parse_artists(artists):
    if artists:
        return artists.split(" | ")


def standardize_header(header):
    for key in "Interpretes", "Interprete", "Intérpretes", "Intérprete":
        if key in header:
            header["Intérprete"] = header.pop(key)

    main = None
    if header.pop("Main", None) or header.pop("main", None):
        main = True
    if header.pop("Alt", False) or header.pop("alt", False):
        main = False

    detect_modulations = False
    for key in header:
        if re.match(
            "detecta?r? modula(cao|coes|tions?)", unidecode(key), re.IGNORECASE
        ):
            val = header.pop(key)
            detect_modulations = unidecode(val).lower() not in (
                "nao",
                "not",
                "no",
                "false",
            )
            break

    return {
        "title": header.pop("title", None),
        "authors": parse_artists(header.pop("author", None)),
        "interpreters": parse_artists(header.pop("Intérprete", None)),
        "album": parse_album(header.pop("Álbum", None)),
        "year": header.pop("Ano", None),
        "display_version": header.pop("Versão", None),
        "main": main,
        "key": header.pop("Tom", None),
        "info": header,
        "detect_modulations": detect_modulations,
    }


def custom_md(text):
    return re.sub(r"~([^~]*)~", r"<ins>\g<1></ins>", text)


def inline_md(text):
    assert "\n" not in text, "inline_md shouldn't receive more than one line!"

    if not text:
        return text

    md = markdown.Markdown()
    new = custom_md(html.escape(text))
    new = md.convert(new)
    soup = BeautifulSoup(new, "lxml")

    new = next(soup.body.children).decode_contents()
    new = new.replace("<a href=", '<a target="_blank" rel="noreferrer" href=')

    return new


def parse_header(lines):
    header = {}

    while not lines[0].strip():
        del lines[0]

    data = lines[0].split(" - ")
    header["title"] = data[0].strip()

    try:
        header["author"] = data[1].strip()
    except IndexError:
        header["author"] = ""

    del lines[0]

    while ":" in lines[0]:
        data = lines[0].split(":", maxsplit=1)
        header[data[0].strip()] = inline_md(data[1].strip())
        del lines[0]

    return standardize_header(header), lines
