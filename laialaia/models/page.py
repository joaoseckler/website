from django.db import models


class TextPage(models.Model):
    slug = models.CharField("Page slug", max_length=200, primary_key=True)
    title = models.CharField(
        "Page title",
        max_length=200,
    )
    text = models.TextField("HTML text")
    description = models.TextField("Short description")
