from django.core.exceptions import ValidationError
from django.db import models

from laialaia.metadata.version_names import VersionDisplayType


class Artist(models.Model):
    slug = models.CharField("Artist slug", max_length=100, primary_key=True)
    name = models.CharField(
        "Artist name",
        max_length=100,
    )

    def __str__(self):
        return self.name


class Album(models.Model):
    slug = models.CharField("Album slug", max_length=200, primary_key=True)
    title = models.CharField(
        "Album title",
        max_length=200,
    )
    authors = models.ManyToManyField(Artist)
    year = models.IntegerField("Year of album release", null=True)

    def __str__(self):
        return self.title


class Cifra(models.Model):
    slug = models.SlugField(
        "Slug (primary key)",
        max_length=100,
        primary_key=True,
    )

    title = models.CharField("Title", max_length=100)
    authors = models.ManyToManyField(Artist)

    def __str__(self):
        return self.title


class CifraVersion(models.Model):
    slug = models.SlugField(
        "Slug",
        max_length=100,
    )
    cifra = models.ForeignKey(
        Cifra,
        on_delete=models.CASCADE,
        related_name="versions",
        help_text="Cifra of which this is a version",
    )

    interpreters = models.ManyToManyField(Artist)

    album = models.ForeignKey(
        Album,
        on_delete=models.SET_NULL,
        related_name="cifra_versions",
        help_text="Album of this version",
        null=True,
    )

    year = models.IntegerField("Year of this version", null=True)
    key = models.CharField("Key of song", max_length=5)
    main = models.BooleanField(
        "Whether this is the main version (use null if indiferente", null=True
    )

    raw = models.TextField("Cifra in text format")
    html = models.TextField("HTML of body of cifra")
    display_version = models.CharField(
        "Short and unique but readable form of version, to be displayed",
        max_length=300,
    )
    display_version_type = models.IntegerField(
        "Type of display version",
        choices=[(t.name, t.value[0]) for t in VersionDisplayType],
    )
    transposition_dict = models.TextField(
        "JSON representation of data about transposition of chords"
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["cifra", "slug"], name="unique_version_slug_per_cifra"
            )
        ]

    def clean(self):
        if self.album and self.year and self.album.year != self.year:
            raise ValidationError(
                "Year of version cannot be different from year of "
                "album, if such exists"
            )

    def __str__(self):
        return self.display_version


class GenericKeyValue(models.Model):
    cifra_version = models.ForeignKey(
        CifraVersion,
        on_delete=models.CASCADE,
        related_name="info",
        help_text="Cifra version to which this belongs",
    )
    key = models.CharField("Key", max_length=100)
    value = models.TextField("Value", blank=True)

    def __repr__(self):
        return f"<GenericKeyValue ({self.key}: {self.value})>"
