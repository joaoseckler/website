import uuid

from django.db import models


class Summary(models.Model):
    uuid = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField("Title of the songbook", max_length=200)
    use_page_order = models.BooleanField("Order by page if True, by slug otherwise")


class SummaryEntry(models.Model):
    slug = models.SlugField("Slug", max_length=100)
    title = models.CharField("Title", max_length=100)
    display_info = models.CharField(
        "Ready to display information (usually authors or version)",
        max_length=200,
    )
    summary = models.ForeignKey(
        Summary, on_delete=models.CASCADE, related_name="entries"
    )
    page = models.IntegerField("Page number")
