import time
from argparse import BooleanOptionalAction
from dataclasses import dataclass
from pathlib import Path
from random import shuffle
from tempfile import TemporaryFile
from typing import Any

from django.core.management.base import BaseCommand, CommandError
from laialaia import __file__ as laialaia_file
from laialaia.metadata import parse_header
from laialaia.music.common import Interval
from laialaia.music.harmony import Chord, Key
from laialaia.music.hmm import KeyDetectionHMM
from laialaia.parse.cifras import Lines
from laialaia.parse.common import TranspositionDict


@dataclass
class FileData:
    name: str
    metadata: dict[str, Any]
    lines: list[str]

    def __iter__(self):
        return iter((self.name, self.metadata, self.lines))


class Command(BaseCommand):
    help = "Manage the HMM model for automatic modulation detection"
    txt_dir = Path(laialaia_file).parent / "raw" / "txt"
    default_includes_filename = Path(__file__).parent / "train_includes.txt"
    erase_line = "\033[1A\033[2K\r"

    def add_arguments(self, parser):
        parser.add_argument(
            "--train",
            action="store_true",
            help="Only train the model",
        )
        parser.add_argument(
            "--test",
            action="store_true",
            help="Only test the model",
        )
        parser.add_argument(
            "--store-model",
            action=BooleanOptionalAction,
            default=None,
            help="Store the result of training",
        )
        parser.add_argument(
            "--train-test-proportion",
            type=float,
            help="Proportion between testing and training",
            default=0.5,
        )
        parser.add_argument(
            "--excludes-file",
            default=None,
            help="Path to file with txt files to exclude",
            type=Path,
        )
        parser.add_argument(
            "--includes-file",
            default=self.default_includes_filename,
            help="Path to file with txt files to exclude",
            type=Path,
        )
        parser.add_argument(
            "--exclude",
            default=[],
            help="Files to exclude",
            type=list,
            nargs="*",
        )
        parser.add_argument(
            "--includes",
            default=[],
            help="Files to include",
            type=list,
            nargs="*",
        )
        parser.add_argument(
            "--train-factor",
            default=1,
            help=(
                "Factor by which training is considered over rule based probabilities"
            ),
            type=float,
        )

    def get_all_files(self):
        if not self.txt_dir.is_dir():
            raise CommandError(
                "Could not find txt dir to read all files from\n"
                f"(tried {self.txt_dir})"
            )
        return [file.name for file in self.txt_dir.glob("*.txt")]

    def read_files(
        self, includes_file=None, excludes_file=None, **options
    ) -> list[FileData]:
        all_files = self.get_all_files()

        includes = options.get("include", [])
        excludes = options.get("exclude", [])
        if includes_file:
            with open(includes_file, "r") as f:
                includes += [
                    line.strip() for line in f.read().split("\n") if line.strip()
                ]

        if excludes_file:
            with open(includes_file, "r") as f:
                excludes = {line.strip() for line in f.read().split("\n")}
            includes = [file for file in includes if file not in excludes]

        if wrong := set(includes) - set(all_files):
            raise CommandError(f"included files don't exist: {', '.join(wrong)}")

        if not includes:
            raise CommandError("no files to use for training found!")

        detect_count = 0
        files = []
        for file in includes:
            with open(self.txt_dir / file, "r", encoding="utf-8") as f:
                txt = f.read()

            metadata, lines = parse_header(txt.split("\n"))
            if not metadata["detect_modulations"]:
                files.append(FileData(metadata=metadata, lines=lines, name=file))
            else:
                detect_count += 1

        msg = f"Using {len(files)}/{len(all_files)} files"

        if excludes_file or detect_count:
            msg += " ("
            if excludes_file:
                msg += f"{len(excludes)} excluded"
                if detect_count:
                    msg += ", "
            if detect_count:
                msg += f"{detect_count} not candidates"
            msg += ")"

        self.stdout.write(msg)

        return files

    def split_files(self, files, **options) -> tuple[list[FileData], list[FileData]]:
        new = list(files)
        shuffle(new)
        n = round(len(new) * options["train_test_proportion"])

        if options["verbosity"] > 0:
            self.stdout.write(
                f"Using {n} files for training and {len(new) - n} for testing"
            )

        return new[:n], new[n:]

    def train(self, files, *_args, **options):
        observations = []
        observation_states = []
        n = len(files)
        verbosity = options.get("verbosity", 0)

        self.stdout.write("\n")

        for i, (filename, metadata, raw) in enumerate(files):
            progress = f"({i + 1}/{n})"
            self.stdout.write(
                f"{'' if verbosity > 1 else self.erase_line}{progress:<8} - {filename}"
            )

            lines = Lines(raw, metadata["key"], TranspositionDict())
            chords = [chord.id() for chord in lines.analysis.chords]
            keys = [
                lines.analysis.key_at(i).id() for i in range(len(lines.analysis.chords))
            ]

            for interval in Interval.common_intervals():
                t_chords = [Chord.transpose_id(i, interval) for i in chords]
                t_keys = [Key.transpose_id(i, interval) for i in keys]
                observations.append(t_chords)
                observation_states.append(t_keys)

        model = KeyDetectionHMM()
        model.train(observations, observation_states)
        if options.get("store_model", False):
            model.dump_parameters()
        else:
            model.dump_parameters(options["tmp_file"])

    def test(self, files, *_args, **options):
        n = len(files)

        verbosity = options.get("verbosity", 0)
        train_factor = options.get("train_factor", 1.0)
        self.stdout.write()
        rating_sum = 0
        modulating_count = 0
        modulating_rating_sum = 0
        modulating_avg = float("nan")

        for i, (filename, metadata, raw) in enumerate(files):
            key = metadata.get("key", None)

            lines = Lines(raw, key, TranspositionDict())
            chords = [chord for chord in lines.analysis.chords]
            expected_analysis = lines.analysis
            expected_keys = [
                expected_analysis.key_at(i).id() for i in range(len(chords))
            ]

            lines = Lines(
                raw,
                key,
                TranspositionDict(),
                detect_modulations=True,
                train_factor=train_factor,
                model_parameters_file=(
                    None if options["store_model"] else options["tmp_file"]
                ),
            )
            analysis = lines.analysis
            keys = [analysis.key_at(i).id() for i in range(len(chords))]

            rating = sum(e == r for e, r in zip(expected_keys, keys)) / len(chords)
            rating_sum += rating
            avg_rating = rating_sum / (i + 1)

            if len(set(expected_keys)) > 1:
                modulating_count += 1
                modulating_rating_sum += rating
                modulating_avg = modulating_rating_sum / (modulating_count)

            progress = f"({i + 1}/{n})"

            if verbosity > 1:
                self.stdout.write(f"\n{filename}: rating = {rating}\n")

            if verbosity > 0:
                self.stdout.write(
                    f"{'' if verbosity > 1 else self.erase_line}"
                    f"{progress:<8}Average rating: {avg_rating:.5f}"
                    f"\t\tonly modulating: {modulating_avg:.5f}"
                )

            if verbosity > 2 and analysis.key_changes != expected_analysis.key_changes:
                msg = f"\n\n{filename} (key: {key})\n"
                msg += f"{'i':<3} {'chord':<8} {'function':<28} {'resulting':<4} expected\n"
                msg += "\n".join(
                    [
                        f"{i:<3} {str(c):<8} {analysis.functions[i]:<28} {str(analysis.key_at(i)):<4}"
                        f" {str(expected_analysis.key_at(i))}"
                        for i, c in enumerate(chords)
                    ]
                )
                self.stdout.write(msg)

        self.stdout.write(f"Average rating: {avg_rating}")
        self.stdout.write(f"Average rating, only  modulating: {modulating_avg}")

    def handle(self, *args, **options):
        train = options.get("train", False)
        test = options.get("test", False)

        with TemporaryFile() as tmp_file:
            options["tmp_file"] = tmp_file

            if test and train:
                test = False
                train = False

            files = self.read_files(**options)

            if train or test:
                train_files = files
                test_files = files

            else:
                train_files, test_files = self.split_files(files, **options)

            start_time = time.process_time()

            if not test:
                if options["store_model"] is None and train:
                    options["store_model"] = True
                self.train(train_files, *args, **options)

            if not train:
                self.test(test_files, *args, **options)

            if options["verbosity"] > 1:
                self.stdout.write(
                    f"Ran in {time.process_time() - start_time:.5f} seconds"
                )
