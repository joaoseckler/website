from pathlib import Path

from django.core.exceptions import ObjectDoesNotExist
from django.core.management.base import BaseCommand, CommandError
from laialaia import __file__ as laialaia_file
from laialaia.register import (
    ObjectExistsError,
    delete_from_txt,
    register_from_txt,
)

TXT_DIR = Path(laialaia_file).parent / "raw" / "txt"


class Command(BaseCommand):
    help = "Register text file of a cifra into database"

    def add_arguments(self, parser):
        group = parser.add_mutually_exclusive_group(required=True)
        group.add_argument("files", nargs="*", default=[])
        group.add_argument(
            "-a",
            "--all",
            action="store_true",
            help="Register all existing cifras in txt folder",
        )

        parser.add_argument(
            "-u",
            "--update",
            action="store_true",
            help="Update existing versions",
        )

        parser.add_argument(
            "-d",
            "--delete",
            action="store_true",
            help="Delete the corresponding cifra",
        )
        parser.add_argument(
            "--development",
            action="store_true",
            help="Development mode (don't include all keys)",
        )

    def get_all_files(self):
        if not TXT_DIR.is_dir():
            raise CommandError(
                "Could not find txt dir to read all files from\n" f"(tried {TXT_DIR})"
            )
        return TXT_DIR.glob("*.txt")

    def handle(self, *args, **options):
        if options["all"]:
            files = list(self.get_all_files())
        else:
            files = list(map(Path, options["files"]))

        n = len(files)

        for i, file in enumerate(files):
            with open(file, "r", encoding="utf-8") as f:
                txt = f.read()

            progress = f"{i + 1}/{n}"
            if options["delete"]:
                try:
                    self.stdout.write(f"{progress:<8} Deleting {file.name}...")
                    delete_from_txt(txt)
                except ObjectDoesNotExist as error:
                    self.stderr.write(f"Error: {error}")
            else:
                try:
                    self.stdout.write(f"{progress:<8} Adding {file.name}...")
                    register_from_txt(
                        txt,
                        update=options["update"],
                        dev=options["development"],
                    )
                except ObjectExistsError as error:
                    self.stderr.write(f"Warning: {error} (Use -u to update)")
