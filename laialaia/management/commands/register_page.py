from pathlib import Path

from django.core.management.base import BaseCommand, CommandError

from laialaia import __file__ as laialaia_file
from laialaia.register import ObjectExistsError, register_from_md

MD_DIR = Path(laialaia_file).parent / "raw" / "md"


class Command(BaseCommand):
    help = "Register text file of a cifra into database"

    def add_arguments(self, parser):
        group = parser.add_mutually_exclusive_group(required=True)
        group.add_argument("files", nargs="*", default=[])
        group.add_argument(
            "-a",
            "--all",
            action="store_true",
            help="Register all existing cifras in txt folder",
        )

        parser.add_argument(
            "-u",
            "--update",
            action="store_true",
            help="Update existing versions",
        )

    def get_all_files(self):
        if not MD_DIR.is_dir():
            raise CommandError(
                "Could not find md dir to read all files from\n" f"(tried {MD_DIR})"
            )
            return []
        return MD_DIR.glob("*.md")

    def handle(self, *args, **options):
        if options["all"]:
            files = self.get_all_files()
        else:
            files = map(Path, options["files"])

        for file in files:
            with open(file, "r", encoding="utf-8") as f:
                txt = f.read()

            try:
                self.stdout.write(f"Adding {file.name}...")
                register_from_md(txt, file, update=options["update"])
            except ObjectExistsError as error:
                self.stderr.write(f"Warning: {error} (Use -u to update)")
