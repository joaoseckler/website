from difflib import SequenceMatcher
from itertools import combinations
import re

from django.core.management.base import BaseCommand

from laialaia.models.cifra import Album, Artist, Cifra, CifraVersion


class Command(BaseCommand):
    help = "Find similar titles, authors, albums etc, that probably should be equal"

    version_metadata_whitelist = re.compile("Sinuca de bico.*")

    def warning(self, msg):
        self.stdout.write(msg)
        self.stdout.write("------------------")

    def find_similar(self, collection, field):
        for a, b in combinations(collection, 2):
            value_a = getattr(a, field)
            value_b = getattr(b, field)

            if SequenceMatcher(None, value_a, value_b).ratio() > 0.75:
                self.warning(
                    f"These '{type(a).__name__}'s have similar '{field}'s:\n"
                    f"    {value_a}\n"
                    f"    {value_b}\n"
                    f"Verify if there are any errors"
                )

    def similarity_check(self):
        self.find_similar(Artist.objects.all(), "name")
        self.find_similar(Album.objects.all(), "title")
        self.find_similar(Cifra.objects.all(), "title")

    def check_missing_metadata(self):
        for cifra in CifraVersion.objects.all():
            if self.version_metadata_whitelist.match(cifra.display_version):
                continue

            missing = set()

            header = {o.key: o.value for o in cifra.info.all()}
            for field in "interpreters", "album", "year", "key":
                if not getattr(cifra, field, None):
                    missing.add(field)

            for field in "Forma", "Tessitura", "BPM", "Cifraclub", "Catálogo":
                if not header.get(field, None):
                    missing.add(field)

            if missing:
                missing = "  " + "\n  ".join(missing)
                self.warning(
                    f"'{cifra.cifra}' version '{cifra}' with missing keys:\n{missing}"
                )

    def handle(self, *args, **options):
        self.similarity_check()
        self.check_missing_metadata()
