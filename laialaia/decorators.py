from django.utils.cache import patch_cache_control
from django.utils.decorators import decorator_from_middleware_with_args


class CacheHeaderMiddleware:
    def __init__(self, func, **options):
        self.options = options

    def process_response(self, request, response):
        patch_cache_control(response, **self.options)
        return response


control_cache = decorator_from_middleware_with_args(CacheHeaderMiddleware)
