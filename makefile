MAKEFLAGS:= --no-print-directory
SHELL=bash
MD=$(shell find md/ -type f -name '*.md')
TEST_APPS=booklet autodelete

.PHONY: all dev cifras test createsuperuser

venv=. venv/bin/activate

all: $(MD)
	./makepages.py

dev:
	@$(MAKE) -f dev.makefile

build: all
	cd palco/assets && npm run build

test:
	@. venv/bin/activate && ./manage.py test $(TEST_APPS)


laialaia_txt:
	@. venv/bin/activate && \
	./manage.py register_cifra -u $(TARGET) $(DEV)


laialaia_md:
	@. venv/bin/activate && \
	./manage.py register_page -u $(TARGET)

tmp2 := $(shell mktemp)
tmp1 := $(shell mktemp)

compare_reqs:
	@cat *requirements.txt | sort > ${tmp1}
	@$(venv); pip freeze | sort > ${tmp2}
	@git diff --no-index ${tmp1} ${tmp2}

compare_reqs_comm:
	@cat *requirements.txt | sort > ${tmp1}
	@$(venv); pip freeze | sort > ${tmp2}
	@comm -13 ${tmp1} ${tmp2}

_createsuperuser:
	$(venv); DJANGO_SUPERUSER_PASSWORD=admin ./manage.py createsuperuser \
		--username "admin" --email "admin@example.com" --noinput

_restartdb:
	rm db.sqlite3
	$(venv); ./manage.py migrate

_loaddata:
	$(venv); ./manage.py loaddata db.json

restartdb: _restartdb _loaddata _createsuperuser
